import { createRequire } from 'node:module';

import { fileURLToPath } from 'node:url';
import { dirname } from 'node:path';

globalThis.require = createRequire(import.meta.url);
globalThis.__filename = fileURLToPath(import.meta.url);
globalThis.__dirname = dirname(__filename);

