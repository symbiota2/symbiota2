
# Symbiota2

This project was uses [Nx](https://nx.dev), [Angular](https://angular.io), and [Nest](https://nestjs.com).

## Documentation

[Nx Documentation](https://nx.dev/angular)

[Angular Documentation](https://angular.io/docs)

[Nest Documentation](https://docs.nestjs.com/)

[Tutorial on writing a Symbiota2 API endpoint](./docs/tutorials/simpleWebService.md)

[Tutorial on writing a Symbiota2 UI page](./docs/tutorials/simpleWebPage.md)

[Documentation on language translation in Symbiota2](./docs/translations.md)

[Getting started using Elasticsearch in Symbiota2](./docs/elasticsearch.md)

## Installation
There are two ways to install  Symbiota2.

1. [Install using Docker](./docs/dockerInstall.md)  recommended for most installations
2. [Install manually ](./docs/manualInstall.md)  recommended for development

## Configuration
Use [these instructions](./docs/configuration.md)   to configure Symbiota2, in particular, to set environment variables.

## Build
To build or rebuild Symbiota2 in response 
to code changes use the following depending on 
whether Symbiota2 is installed using Docker or 
manually.

1. [Build using Docker](./docs/dockerBuild.md)  recommended for most installations
2. [Build manually ](./docs/manualBuild.md)  recommended for development

## Overall architecture

![Architecture](./docs/app-architecture.png)

## Database

The Symbiota2 API uses [TypeORM](https://typeorm.io) to manage databases migrations and entities, so in theory any  
database that's compatible with TypeORM is compatible with Symbiota2. However, currently SQLite experiences issues due to  
a lack of support for spatial indexes. MariaDB is the only database that has been tested.

For development purposes, `docker-compose up -d` will start a mariadb server on port 3306 compatible with the  
DATABASE\_\* defaults. This database loads the initialization scripts in [docker-entrypoint-initdb.d](./docker-entrypoint-initdb.d/)  
to initialize a [Symbiota v1 database](https://github.com/Symbiota/Symbiota/blob/f158b1651632ecfe018d7c5d578e7fa8d904fb04/docs/INSTALL.txt#L26).  
This repo also provides a convenience script for running a sql file or directory of sql files. This script utilizes the environment  
variables above to connect to the database:

`npm run sql my-script.sql`

or

`npm run sql my/dir/with/sql/scripts`

Symbiota2 has been written under the assumption that most users will be upgrading from a Symbiota v1 database. Any new databases  
should first run the initialization scripts in docker-entrypoint-initdb.d. When ready to upgrade to the Symbiota2 schema, run  

`npm run typeorm migration:run`

This will load the schema updates from the [migrations directory](./libs/api-database/src/migrations).

During development, if any [entities](./libs/api-database/src/entities) are changed, a migration should be generated using  

`npm run typeorm migration:generate -- -n MyMigration`

A key requirement of Symbiota2 is backward-compatibility with Symbiota v1 databases. For this reason, care should be taken  
that any migrations do not result in data loss. However, **all users need to back up their data prior to upgrading to Symbiota2** as  
it's always possible that data loss could occur.

More information on the database schema can be found here:

- [Institutions/Collections/Occurrences/Taxa](./docs/occurrences.md)
- [Occurrences/Taxa/Images](./docs/images.md)

## Generate a plugin

Run `nx g @nrwl/angular:lib --buildable --publishable --importPath @<my-org>/<my-plugin> <my-plugin> ` to generate a UI plugin.

Run `nx workspace-generator api-plugin @<my-org>/<my-plugin>` to generate an API plugin.

Plugins are shareable across plugins and applications. They can be imported from `@<my-org>/<my-plugin>`.

All UI plugins should extend the [SymbiotaUiPlugin class](./libs/ui-common/src/lib/plugin/symbiota-ui-plugin.class.ts) and can be  
activated by adding them to the [UiPluginModule's](./libs/ui-common/src/lib/plugin/ui-plugin.module.ts) `configure()` method  
in the UI core [AppModule](./apps/ui/src/app/app.module.ts). The SymbiotaUiPlugin base class provides methods that are used  
to inject routes and UI components within the core app.

All API plugins should extend the [SymbiotaApiPlugin class](./libs/api-common/src/plugin/symbiota-api-plugin.ts) and can  
be activated by adding them to the [ApiPluginModule's](./libs/api-common/src/plugin/api-plugin.module.ts) `configure()` method  
in the API core [AppModule](./apps/api/src/app.module.ts). The SymbiotaApiPlugin base class provides a method that is used to  
inject TypeORM database entities into the [DatabaseModule](./libs/api-database/src/database.module.ts). All controllers  
exported from an API plugin module are automatically loaded into the core API app.

## Development server

Run `nx serve ui` for a UI dev server in 'watch' mode.

Run `nx serve api` for an API dev server in 'watch' mode.

Run `npm run start:dev` to start both servers in 'watch' mode.

## Code scaffolding

Run `nx g @nrwl/angular:<resource type> --project=<ui or my-plugin> <my-resource>` to generate a new UI resource.

Run `nx g @nrwl/nest:<resource type> --project=<api or my-plugin> <my-resource>` to generate a new API resource.

## Running unit tests

Run `ng test ui` to execute the UI unit tests via [Jest](https://jestjs.io).

Run `nx test api` to execute the API unit tests via [Jest](https://jestjs.io).

Run `nx affected:test` to execute the unit tests affected by a change.

## Understand your workspace

Run `nx dep-graph` to see a diagram of the dependencies of your projects.
