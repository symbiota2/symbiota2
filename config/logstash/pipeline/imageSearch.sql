SELECT url, thumbnailurl, taxa.sciname, photographer, caption, imagetype, locality,
       images.notes, images.dynamicProperties, taxa.kingdomName,
       UNIX_TIMESTAMP(images.InitialTimeStamp) AS unix_ts_in_secs
FROM images LEFT JOIN taxa ON (images.tid = taxa.tid)
WHERE UNIX_TIMESTAMP(images.InitialTimeStamp) > :sql_last_value AND images.InitialTimeStamp < NOW()
ORDER BY images.InitialTimeStamp ASC
