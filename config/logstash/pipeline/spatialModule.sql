SELECT category, CollType,
       omcollections.CollectionName,  taxa.SecurityStatus, taxa.author, taxa.sciname AS taxaSciname,
       taxa.dynamicProperties as dynamicPropertiesTaxa, taxa.status, taxa.kingdomName,
       omoccurrences.*, UNIX_TIMESTAMP(omoccurrences.dateLastModified) AS unix_ts_in_secs
FROM omoccurrences LEFT JOIN omcollections ON (omoccurrences.collid = omcollections.CollID)
  LEFT JOIN taxa ON (tid=tidinterpreted)
  LEFT JOIN (
    SELECT CollectionName, JSON_ARRAYAGG(category) AS category
    FROM omcollections LEFT JOIN omcollcatlink ON (omcollections.CollID = omcollcatlink.collid)
      LEFT JOIN omcollcategories ON (omcollcatlink.ccpk = omcollcategories.ccpk)
    GROUP BY CollectionName


    ) B ON (B.CollectionName = omcollections.collectionName)
WHERE (UNIX_TIMESTAMP(GREATEST(omoccurrences.dateLastModified, omcollections.InitialTimeStamp)) > :sql_last_value)
ORDER BY omoccurrences.dateLastModified ASC
