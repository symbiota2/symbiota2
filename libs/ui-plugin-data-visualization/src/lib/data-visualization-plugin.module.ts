import { NgModule } from "@angular/core";
import { TranslateModule } from "@ngx-translate/core";

import {
    ApiClientModule, AppConfigService,
    NavBarLink, SymbiotaComponentModule,
    SymbiotaUiPlugin
} from '@symbiota2/ui-common';

import { Route, RouterModule } from "@angular/router";
import { MatDialogModule } from "@angular/material/dialog";
import { MatCardModule } from "@angular/material/card";
import { CommonModule } from "@angular/common";
import { MatIconModule } from "@angular/material/icon";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatNativeDateModule, MatOptionModule } from "@angular/material/core";
import { MatButtonModule } from "@angular/material/button";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatInputModule } from "@angular/material/input";
import { MatSelectModule } from "@angular/material/select";
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDatepickerModule } from "@angular/material/datepicker";
import { FlexModule } from "@angular/flex-layout";
import { MatTreeModule } from '@angular/material/tree';

import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatListModule } from '@angular/material/list';
import { MatRadioModule } from '@angular/material/radio';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatTabsModule } from '@angular/material/tabs';
import { MatChipsModule } from '@angular/material/chips'
import { QuillModule } from 'ngx-quill';
import { BasicGraphPageComponent, GraphPageComponent } from './pages';
import { LocalMenu } from './local-menu';
import { DATA_VISUALIZATION_ROUTE, GRAPH_PAGE_ROUTE } from './routes';
import { ForceGraphComponent } from './components';


@NgModule({
    imports: [
        ApiClientModule,
        BrowserModule,
        BrowserAnimationsModule,
        CommonModule,
        FormsModule,
        MatAutocompleteModule,
        MatButtonModule,
        MatCardModule,
        MatDatepickerModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatNativeDateModule,
        MatOptionModule,
        MatSelectModule,
        ReactiveFormsModule,
        RouterModule,
        TranslateModule,
        FlexModule,
        MatTreeModule,
        MatCheckboxModule,
        MatListModule,
        MatRadioModule,
        SymbiotaComponentModule,
        MatPaginatorModule,
        MatGridListModule,
        MatTabsModule,
        MatChipsModule,
        QuillModule.forRoot()
    ],
    declarations: [
        BasicGraphPageComponent,
        GraphPageComponent,
        ForceGraphComponent
//        FilterPipe
    ],
    providers: [
    ],
    exports: [
        BasicGraphPageComponent,
        GraphPageComponent,
    ] /*,
    entryComponents: [
        BasicGraphPageComponent,
        GraphPageComponent,
    ] */
})
export class DataVisualizationPlugin extends SymbiotaUiPlugin {
    static readonly PLUGIN_NAME = 'plugins.dataviz.name';

    constructor(private readonly appConfig: AppConfigService) {
        super()
    }
    private static MY_DATA_VISUALIZATION_ROUTE = DATA_VISUALIZATION_ROUTE
    private static MY_GRAPH_PAGE_ROUTE = GRAPH_PAGE_ROUTE

    static routes(): Route[] {
        const arr2 = LocalMenu.routes()
        if (arr2.length > 0) {
            return arr2
        }
        return [
            {
                path: DataVisualizationPlugin.MY_DATA_VISUALIZATION_ROUTE,
                component: BasicGraphPageComponent
            },
            {
                path: DataVisualizationPlugin.MY_GRAPH_PAGE_ROUTE,
                component: GraphPageComponent
            },
        ]
    }

    static navBarLinks(): NavBarLink[] {
        return [
        ]
        }

    }
