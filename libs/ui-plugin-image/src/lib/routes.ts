export const IMAGE_API_BASE = "image"
export const IMAGE_DETAILS_ROUTE_PREFIX = "image/details"
export const IMAGE_DETAILS_ROUTE = IMAGE_DETAILS_ROUTE_PREFIX +"/:imageID"
export const IMAGE_LIBRARY_ROUTE = "image/library/:level"
export const IMAGE_SEARCH_ROUTE = "image/search"
export const IMAGE_SEARCH_DASHBOARD_ROUTE = "image/search/dashboard"
export const IMAGE_FOLDER_UPLOAD_ROUTE = "image/folderUpload"
export const IMAGE_DISPLAY_ROUTE = "image/display"
export const IMAGE_FOLDER_UPLOAD_COMPLETE_ROUTE = "image/folderUploadComplete"


