import { ApiUserRoleName } from "..";

export interface ApiCollectionInstitutionOutput {
    id: number;
    code: string;
    name: string;
}

export interface ApiInstitutionOutput extends ApiCollectionInstitutionOutput {
    address1: string;
    address2: string;
    city: string;
    stateProvince: string;
    postalCode: string;
    country: string;
    phone: string;
    contact: string;
    email: string;
    url: string;
    notes: string;
    initialTimestamp: string | Date;
}

export interface ApiInstitutionInput{
    code: string;
    name: string
    address1: string;
    address2: string;
    city: string;
    stateProvince: string;
    postalCode: string;
    country: string;
    phone: string;
    contact: string;
    email: string;
    url: string;
    notes: string;
}

export interface ApiCollectionRoleUser {
    uid: number;
    username: string;
    firstName: string;
    lastName: string;
}

export interface ApiCollectionRoleOutput {
    id: number;
    name: ApiUserRoleName;
    user: ApiCollectionRoleUser;
}

export interface ApiCollectionRoleInput {
    uid: number
    role: ApiUserRoleName
}

export interface ApiCollectionStatsOutput {
    familyCount: number;
    genusCount: number;
    speciesCount: number;
    recordCount: number;
    georeferencedCount: number;
    lastModifiedTimestamp: Date;
}

export interface ApiCollectionListItem {
    id: number;
    institutionCode: string;
    collectionCode: string;
    collectionName: string;
    icon: string;
    type: string;
}

export interface ApiImageListItem {
    id: number;
    taxonID: number | null;
    url: string;
    thumbnailUrl: string;
    originalUrl: string;
    archiveUrl: string;
    photographerName: string;
    photographerUID: number | null;
    type: string;
    format: string;
    caption: string;
    owner: string;
    sourceUrl: string;
    referenceUrl: string;
    copyright: string;
    rights: string;
    accessRights: string;
    locality: string;
    occurrenceID: number | null;
    notes: string;
    anatomy: string;
    username: string;
    sourceIdentifier: string;
    mediaMD5: string;
    dynamicProperties: string;
    sortSequence: number;
    initialTimestamp: Date;
}

export interface ApiCollectionOutput extends ApiCollectionListItem {
    collectionCode: string;
    institutionCode: string;
    institution: ApiCollectionInstitutionOutput;
    collectionStats: ApiCollectionStatsOutput;
    fullDescription: string;
    homePage: string;
    individualUrl: string;
    contact: string;
    email: string;
    latitude: number;
    longitude: number;
    managementType: string;
    rightsHolder: string;
    rights: string;
    usageTerm: string;
    accessRights: string;
    initialTimestamp: Date;
}

export interface ApiCollectionInput {
    collectionCode: string;
    collectionName: string;
    institutionID: number;
    fullDescription: string;
    homePage: string;
    individualUrl: string;
    contact: string;
    email: string;
    latitude: number;
    longitude: number;
    icon: string;
    type: string;
    managementType: string;
    rightsHolder: string;
    rights: string;
    usageTerm: string;
    accessRights: string;
    categoryID: number;
}
