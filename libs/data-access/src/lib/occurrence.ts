import { ApiListResults } from './common';
import { ApiCollectionListItem, ApiImageListItem } from './collection';

export enum ApiTaxonSearchCriterion {
    familyOrSciName = 'familyOrSciName',
    scientificName = 'scientificName',
    family = 'family',
    higherTaxonomy = 'higherTaxonomy',
    commonName = 'commonName',
    taxonID = 'taxonID'
}

export interface ApiOccurrenceFindAllParams {
    collectionID: number[];
    limit: number;
    offset: number;

    // Taxon criteria
    taxonSearchCriterion: ApiTaxonSearchCriterion;
    taxonSearchStr: string;
    includeSynonyms: boolean

    // Locality criteria
    country: string;
    stateProvince: string;
    county: string;
    locality: string;
    minimumElevationInMeters: number;
    maximumElevationInMeters: number;
    minLatitude: number;
    minLongitude: number;
    maxLatitude: number;
    maxLongitude: number;

    // Collector criteria
    collectorLastName: string;
    minEventDate: Date | string;
    maxEventDate: Date | string;

    // Specimen criteria
    catalogNumber: string;

    // Filters
    limitToSpecimens: boolean;
    limitToImages: boolean;
    limitToGenetic: boolean;

    geoJSON?: string;
}

export interface ApiOccurrenceListItem {
    id: number;
    collection: ApiCollectionListItem;
    // images: ApiImageListItem[];
    catalogNumber: string;
    taxonID: number | null;
    scientificNameComputed: string;
    scientificName: string;
    latitude: number | null;
    longitude: number | null;
    collectionID: number;
    dbpk: string;
    basisOfRecord: string;
    occurrenceGUID: string;
    otherCatalogNumbers: string;
    ownerInstitutionCode: string;
    datasetID: string;
    family: string;
    genus: string;
    specificEpithet: string;
    taxonRank: string;
    infraspecificEpithet: string;
    taxonRemarks: string;
    identifiedBy: string;
    dateIdentified: Date | null;
    identificationReferences: string;
    identificationRemarks: string;
    identificationQualifier: string;
    typeStatus: string;
    recordedByNames: string;
    recordNumber: string;
    recordedByID: string;
    associatedCollectors: string;
    latestDateCollected: string;
    startDayOfYear: number | null;
    endDayOfYear: number | null;
    verbatimEventDate: string;
    habitat: string;
    substrate: string;
    fieldNotes: string;
    fieldNumber: string;
    eventID: string;
    occurrenceRemarks: string;
    informationWithheld: string;
    dataGeneralizations: string;
    associatedOccurrences: string;
    associatedTaxa: string;
    dynamicProperties: string;
    verbatimAttributes: string;
    behavior: string;
    reproductiveCondition: string;
    cultivationStatus: number | null;
    establishmentMeans: string;
    lifeStage: string;
    sex: string;
    individualCount: string;
    samplingProtocol: string;
    samplingEffort: string;
    preparations: string;
    locationID: string;
    country: string;
    stateProvincecounty?: string;
    municipality: string;
    waterBody: string;
    locality: string;
    localitySecurity: number | null;
    localitySecurityReason: string;
    geodeticDatum: string;
    coordinateUncertaintyInMeters: number | null;
    footprintWKT: string;
    coordinatePrecision: number;
    locationRemarks: string;
    verbatimCoordinates: string;
    verbatimCoordinateSystem: string;
    georeferenceProtocol: string;
    degreeOfEstablishment: string;
    associatedSequences: string;
    earliestAgeOrLowestStage: string;
    earliestEonOrLowestEonothem: string;
    earliestEpochOrLowestSeries: string;
    earliestEraOrLowestErathem: string;
    earliestPeriodOrLowestSystem: string;
    eventRemarks: string;
    eventTime: string;
    measurementRemarks: string;
    parentEventID: string;
    associatedOrganisms: string;
    associatedReferences: string;
    countryCode: string;
    datasetName: string;
    subgenus: string;
    georeferenceSources: string;
    georeferenceVerificationStatus: string;
    georeferenceRemarks: string;
    minimumElevationInMeters: number | null;
    maximumElevationInMeters: number | null;
    verbatimElevation: string;
    minimumDepthInMeters: number | null;
    maximumDepthInMeters: number | null;
    verbatimDepth: string;
    previousIdentifications: string;
    disposition: string;
    storageLocation: string;
    genericColumn1: string;
    genericColumn2: string;
    modified: Date | null;
    language: string;
    observerUID: number | null;
    processingStatus: string;
    recordEnteredBy: string;
    duplicateQuantity: number | null;
    labelProject: string;
    dynamicFields: string;
    initialTimestamp: Date | null;
    lastModifiedTimestamp: Date
}

export interface ApiOccurrenceList extends ApiListResults<ApiOccurrenceListItem> { }

export interface ApiOccurrence extends ApiOccurrenceListItem {
    associatedCollectors: string;
    associatedOccurrences: string;
    associatedOrganisms: string;
    associatedReferences: string;
    associatedSequences: string;
    associatedTaxa: string;
    basisOfRecord: string;
    behavior: string;
    coordinatePrecision: number;
    coordinateUncertaintyInMeters: number | null;
    country: string;
    countryCode: string;
    county: string;
    cultivationStatus: number | null;
    dataGeneralizations: string;
    datasetID: string;
    dateIdentified: Date | null;
    datasetName: string;
    dbpk: string;
    disposition: string;
    duplicateQuantity: number | null;
    dynamicFields: string;
    dynamicProperties: string;
    endDayOfYear: number | null;
    establishmentMeans: string;
    earliestAgeOrLowestStage: string;
    earliestEonOrLowestEonothem: string;
    earliestEpochOrLowestSeries: string;
    earliestEraOrLowestErathem: string;
    earliestPeriodOrLowestSystem: string;
    eventDate: Date | null;
    eventID: string;
    eventRemarks: string;
    eventTime: string;
    family: string;
    fieldNotes: string;
    fieldNumber: string;
    footprintWKT: string;
    genericColumn1: string;
    genericColumn2: string;
    genus: string;
    geodeticDatum: string;
    georeferencedBy: string;
    georeferenceProtocol: string;
    georeferenceRemarks: string;
    georeferenceSources: string;
    georeferenceVerificationStatus: string;
    habitat: string;
    identificationQualifier: string;
    identificationReferences: string;
    identificationRemarks: string;
    identifiedBy: string;
    individualCount: string;
    informationWithheld: string;
    infraspecificEpithet: string;
    initialTimestamp: Date | null;
    labelProject: string;
    language: string;
    lastModifiedTimestamp: Date;
    latestDateCollected: string;
    lifeStage: string;
    locality: string;
    localitySecurity: number | null;
    localitySecurityReason: string;
    locationID: string;
    locationRemarks: string;
    maximumDepthInMeters: number | null;
    maximumElevationInMeters: number | null;
    measurementRemarks: string;
    minimumDepthInMeters: number | null;
    minimumElevationInMeters: number | null;
    modified: Date | null;
    municipality: string;
    observerUID: number | null;
    occurrenceGUID: string;
    occurrenceRemarks: string;
    otherCatalogNumbers: string;
    ownerInstitutionCode: string;
    parentEventID: string;
    preparations: string;
    previousIdentifications: string;
    processingStatus: string;
    recordedByID: string;
    recordedByNames: string;
    recordEnteredBy: string;
    recordNumber: string;
    reproductiveCondition: string;
    samplingEffort: string;
    samplingProtocol: string;
    scientificNameComputed: string;
    scientificName: string;
    sex: string;
    specificEpithet: string;
    startDayOfYear: number | null;
    stateProvince: string;
    storageLocation: string;
    subgenus: string;
    substrate: string;
    taxonRank: string;
    taxonRemarks: string;
    typeStatus: string;
    verbatimAttributes: string;
    verbatimCoordinates: string;
    verbatimCoordinateSystem: string;
    verbatimDepth: string;
    verbatimElevation: string;
    verbatimEventDate: string;
    waterBody: string;
}

export interface ApiOccurrenceUpload {
    id: number;
    fieldMap: Record<string, string>;
}

export interface ApiOccurrenceMapUploadFieldsBody {
    uniqueIDField: string;
    fieldMap: Record<string, string>;
}
