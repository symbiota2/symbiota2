/* eslint-disable no-extra-boolean-cast */
/* eslint-disable @typescript-eslint/member-ordering */
import { Injectable, EventEmitter } from '@angular/core';
import {
    BehaviorSubject,
    combineLatest,
    Observable,
    of,
    Subscription,
    timer,
} from 'rxjs';
import { User, UserProfileData } from './dto/user.class';
import { UserRoleInputDto } from './dto/role-input-dto.class'
import { ApiClientService } from '../api-client';
import { plainToClass } from 'class-transformer';
import {
    catchError,
    delay,
    filter,
    finalize,
    map,
    shareReplay,
    skip,
    startWith,
    switchMap,
    take,
    tap,
} from 'rxjs/operators';
import { ApiLoginResponse, ApiUserNotification } from '@symbiota2/data-access';
import { AlertService, LoadingService } from '../alert';
import { HttpErrorResponse } from '@angular/common/http';
import jwtDecode from 'jwt-decode';
import { ApiCreateUserData, ApiUser } from '@symbiota2/data-access';
import { CreateUserInputDto, PermissionOutputDto, RoleOutputDto, UserInputDto, UserOutputDto } from '@symbiota2/api-auth';
import { UserRole } from './dto/user-role.class';
import { PermissionRequestDto } from './dto/permission-request.dto';
import { AddRolePermissionsDto } from '@symbiota2/api-auth';

type AuthData = { username?: string; password?: string };
interface NotificationResults {
    count: number;
    notifications: ApiUserNotification[];
}

/**
 * Service for retrieving users from the API
 */
@Injectable()
export class UserService {
    private static readonly ONE_MINUTE = 60 * 1000;
    private static readonly TEN_SECONDS = 10 * 1000;

    private readonly _currentUser = new BehaviorSubject<User>(null);
    private refreshSubscription: Subscription = null;
    private notificationDeleted = new EventEmitter<void>();
    private roleDeleted = new EventEmitter<void>();

    public iAmEditing = new BehaviorSubject<boolean>(false);

    public setIAMEditing() {
        this.iAmEditing.next(true)
    }

    public setIAMViewing() {
        this.iAmEditing.next(false)
    }

    /**
     * The currently logged in user
     */
    public readonly currentUser: Observable<User> = this._currentUser
        .asObservable()
        .pipe(
            skip(1),
            // After that, replay the last submitted value to all subscribers
            shareReplay(1)
        );

    private readonly notificationResults: Observable<NotificationResults> = combineLatest(
        [this.currentUser, this.notificationDeleted.pipe(startWith({}))]
    ).pipe(
        switchMap(([user, _]) => {
            return timer(0, UserService.TEN_SECONDS).pipe(map(() => user));
        }),
        switchMap((user) => {
            if (!user) {
                return of([]);
            }

            const url = this.notificationUrl(user.uid);
            const query = this.api
                .queryBuilder(url)
                .get()
                .addJwtAuth(user.token)
                .build();

            return this.api.send(query, { skipLoading: true });
        }),
        map((response: NotificationResults) => {
            // API returns a timestamp string, so we need to convert to a date
            let notifications = [...response.notifications];
            notifications = notifications.map((n) => {
                return {
                    ...n,
                    createdAt: new Date(n.createdAt),
                };
            });
            return {
                count: response.count,
                notifications: notifications,
            };
        })
    );

    public readonly notifications: Observable<
        ApiUserNotification[]
    > = this.notificationResults.pipe(
        map((result) => result.notifications),
        shareReplay(1)
    );

    public readonly notificationCount: Observable<number> = this.notificationResults.pipe(
        map((result) => result.count),
        shareReplay(1)
    );

    /**
     * Profile data for the currently logged in user
     */
    public readonly profileData: Observable<UserProfileData> = this.currentUser.pipe(
        switchMap((userData) => {
            if (!userData) {
                return of(null);
            }

            const dataReq = this.api
                .queryBuilder(`${this.api.apiRoot()}/users/${userData.uid}`)
                .get()
                .addJwtAuth(userData.token)
                .build();

            return this.api.send(dataReq).pipe(
                catchError((e) => {
                    console.error(e);
                    return of(null);
                }),
                map((profileData) => plainToClass(UserProfileData, profileData))
            );
        }),
        shareReplay(1)
    );

    /**
     * Create a user object based on an ApiLoginResponse
     * @param jsonResponse The JSON returned from the API
     * @return User The parsed User
     * @private
     */
    private static userFromJwt(jsonResponse: ApiLoginResponse): User {
        if (jsonResponse === null) {
            return null;
        }
        const jwtData = jwtDecode(jsonResponse.accessToken) as Record<
            string,
            unknown
        >;
        return User.fromJSON({ ...jwtData, token: jsonResponse.accessToken });
    }

    constructor(
        private readonly alert: AlertService,
        private readonly api: ApiClientService,
        private readonly loading: LoadingService
    ) { }

    /**
     * Creates a new user
     * @param userData The fields for the user
     * @return Observable<User> The created user
     */
    create(userData: ApiCreateUserData): Observable<User> {
        const createReq = this.api
            .queryBuilder(this.usersUrl)
            .post()
            .body(userData)
            .build();

        return this.api.send(createReq).pipe(
            catchError((err: HttpErrorResponse) => {
                if (err.error && err.error.message) {
                    return of(`Account creation failed: ${err.error.message}`);
                }
                return of('Account creation failed');
            }),
            switchMap((userOrError: ApiUser | string) => {
                if (typeof userOrError !== 'string') {
                    this.alert.showMessage('Account created successfully');
                    return this.login(userData.username, userData.password);
                }
                return of(null);
            })
        );
    }

    addUser(
        userData: Partial<CreateUserInputDto>
    ): Observable<any> {
        return this.currentUser.pipe(
            take(1),
            map((user) => {
                if (!user) {
                    throw new Error('Please Login In.')
                }
                return user;
            }),
            switchMap((user) => {
                const createReq = this.api
                .queryBuilder(`${this.usersUrl}/admin`)
                .post()
                .body(userData)
                .addJwtAuth(user.token)
                .build();

                return this.api.send(createReq).pipe(
                    catchError((err: HttpErrorResponse) => {
                        if (err.error && err.error.message) {
                            return of(`User creation failed: ${err.error.message}`);
                        }
                        return of('User creation failed');
                    }),
                    switchMap((userOrError: ApiUser | string) => {
                        if (typeof userOrError !== 'string') {
                            this.alert.showMessage('User created successfully');
                            return of(null);
                        }
                        return of(null);
                    })
                );
            })
        )

    }

    updateUser(
        uid: number,
        userData: Partial<UserInputDto>
    ): Observable<any> {
        return this.currentUser.pipe(
            take(1),
            map((user) => {
                if (!user) {
                    throw new Error('Please Login In.')
                }
                return user;
            }),
            switchMap((user) => {
                const createReq = this.api
                .queryBuilder(`${this.usersUrl}/${uid}/admin`)
                .patch()
                .body(userData)
                .addJwtAuth(user.token)
                .build();

                return this.api.send(createReq).pipe(
                    catchError((err: HttpErrorResponse) => {
                        if (err.error && err.error.message) {
                            return of(`User update failed: ${err.error.message}`);
                        }
                        return of('User update failed');
                    }),
                    switchMap((userOrError: ApiUser | string) => {
                        if (typeof userOrError !== 'string') {
                            this.alert.showMessage('User updated successfully');
                            return of(null);
                        }
                        return of(null);
                    })
                );
            })
        )

    }

    /**
     * Log into the API
     * @return Observable<User> The logged in user
     */
    login(username: string, password: string): Observable<User> {
        this.loading.start();
        return this.authenticate(this.loginUrl, { username, password }).pipe(
            tap((userData) => {
                return this._currentUser.next(userData)}),
            finalize(() => this.loading.end())
        );
    }

    /**
     * Log out from the API
     */
    logout(): void {
        const currentUser = this._currentUser.getValue();
        const logoutReq = this.api
            .queryBuilder(this.logoutUrl)
            .post()
            .addJwtAuth(currentUser.token)
            .build();

        this.loading.start();
        this.api
            .send(logoutReq)
            .pipe(
                tap(() => {
                    this._currentUser.next(null);
                }),
                finalize(() => {
                    this.loading.end();
                })
            )
            .subscribe();
    }



    /**
     * Refresh the current access token using a refresh cookie
     */
    refresh(): Observable<User> {
        return this.authenticate(this.refreshUrl, {}).pipe(
            tap((userData) => this._currentUser.next(userData))
        );
    }

    /**
     * Update the current user's profile via the API
     * @param profileData The profile data
     * @return Observable<UserProfileData> The updated profile data
     */
    saveProfile(profileData: Partial<UserProfileData>): Observable<UserProfileData> {
        return this._currentUser.pipe(
            take(1),
            switchMap((userData) => {
                if (!userData) {
                    return null;
                }

                const dataReq = this.api
                    .queryBuilder(`${this.api.apiRoot()}/users/${userData.uid}`)
                    .patch()
                    .body(profileData)
                    .addJwtAuth(userData.token)
                    .build();

                return this.api.send(dataReq).pipe(
                    catchError((e: HttpErrorResponse) => {
                        this.alert.showError(
                            `An error occurred: ${e.error.message.join('\n')}`
                        );
                        return of(null);
                    }),
                    map((profileData) => {
                        if (profileData) {
                            this.alert.showMessage(
                                'Profile updated successfully'
                            );
                        }
                        return plainToClass(UserProfileData, profileData);
                    }),
                    tap(() => {
                        this.refresh().subscribe();
                    })
                );
            })
        );
    }

    /**
     * Change a password via the API
     * @param username The username whose password we're changing
     * @param newPassword The new password
     * @return Observable<string> An error, if any
     */
    changePassword(
        username: string,
        newPassword: string
    ): Observable<string | null> {
        return this.currentUser.pipe(
            take(1),
            filter((user) => user !== null),
            switchMap((user) => {
                const req = this.api
                    .queryBuilder(
                        `${this.api.apiRoot()}/users/${user.uid}/changePassword`
                    )
                    .patch()
                    .addJwtAuth(user.token)
                    .body({
                        username,
                        newPassword: newPassword,
                    })
                    .build();

                return this.api.send(req).pipe(
                    map(() => null),
                    catchError((err: HttpErrorResponse) => {
                        if (err.error && err.error.message) {
                            return of(err.error.message);
                        }
                        return of('Unknown error');
                    })
                );
            })
        );
    }

    /**
     * Authenticate with the API
     * @param url The url to authenticate against
     * @param authData A username and password
     * @private
     */
    private authenticate(url: string, authData: AuthData) {
        // Build a POST request to the given URL, with the given authData
        // as the body
        const authReq = this.api
            .queryBuilder<AuthData>(url)
            .post()
            .body(authData)
            .addCookieAuth()
            .build();

        return this.api
            .send<AuthData, ApiLoginResponse>(authReq, { skipLoading: true })
            .pipe(
                catchError((e) => {
                    console.error(e);
                    return of(null);
                }),
                map((jwtData) => {
                    // Parse the user from the returned JSON
                    const userData = UserService.userFromJwt(jwtData);
                    // Do refresh with cookie when five minutes remain
                    if (userData) {
                        const refreshIn =
                            userData.millisUntilExpires() -
                            UserService.ONE_MINUTE;
                        this.refreshSubscription = of(0)
                            .pipe(delay(refreshIn))
                            .subscribe(() => {
                                this.refresh().subscribe();
                            });
                    } else if (this.refreshSubscription !== null) {
                        this.refreshSubscription.unsubscribe();
                    }
                    return userData;
                })
            );
    }

    deleteNotification(id: number) {
        this.currentUser
            .pipe(
                take(1),
                map((user) => {
                    if (!user) {
                        throw new Error('Please log in');
                    }
                    return user;
                }),
                switchMap((user) => {
                    const url = `${this.notificationUrl(user.uid)}/${id}`;
                    const query = this.api
                        .queryBuilder(url)
                        .delete()
                        .addJwtAuth(user.token)
                        .build();

                    return this.api
                        .send(query, { skipLoading: true })
                        .pipe(map(() => null));
                }),
                catchError((e) => {
                    return of(e);
                })
            )
            .subscribe((err) => {
                if (err !== null) {
                    this.alert.showError(
                        `Error deleting notification: ${err.message}`
                    );
                } else {
                    this.notificationDeleted.emit();
                }
            });
    }

    deleteAllNotifications() {
        this.currentUser
            .pipe(
                take(1),
                map((user) => {
                    if (!user) {
                        throw new Error('Please log in');
                    }
                    return user;
                }),
                switchMap((user) => {
                    const url = this.notificationUrl(user.uid);
                    const query = this.api
                        .queryBuilder(url)
                        .delete()
                        .addJwtAuth(user.token)
                        .build();

                    return this.api
                        .send<unknown, { deleted: number }>(query, {
                            skipLoading: true,
                        })
                        .pipe(map((result) => result.deleted));
                }),
                catchError((e) => {
                    return of(e);
                })
            )
            .subscribe((errOrDeletedCount) => {
                if (errOrDeletedCount instanceof Error) {
                    this.alert.showError(
                        `Error deleting notification: ${errOrDeletedCount.message}`
                    );
                } else {
                    this.notificationDeleted.emit();
                    this.alert.showMessage(
                        `Cleared ${errOrDeletedCount} notifications`
                    );
                }
            });
    }

    getUsers(searchTerm?: string): Observable<UserOutputDto[]> {
        return this.currentUser.pipe(
            switchMap((currentUser) => {
                const url = `${this.usersUrl}`;
                const req = this.api
                    .queryBuilder(url)
                    .get()
                    .addJwtAuth(currentUser?.token)
                    .build();

                return this.api.send(req).pipe(
                    map((users: UserOutputDto[]) => {
                        return users;
                    })
                );
            }),
            map((users) => { //TODO: move search param to api
                if (!!searchTerm) {
                    return users.filter(
                        (user) =>
                            user.firstName.toLowerCase().includes(searchTerm.toLowerCase()) ||
                            user.lastName.toLowerCase().includes(searchTerm.toLowerCase()) ||
                            user.username.toLowerCase().includes(searchTerm.toLowerCase())
                    );
                }

                return users;
            })
        );
    }

    getUserById(uid: number): Observable<UserOutputDto> {
        return this.currentUser.pipe(
            map((currentUser) => {
                return currentUser.token;
            }),
            switchMap((token) => {
                const url = `${this.usersUrl}/${uid}`;
                const req = this.api
                    .queryBuilder(url)
                    .get()
                    .addJwtAuth(token)
                    .build();

                return this.api.send(req).pipe(
                    map((user: UserOutputDto) => {
                        return user;
                    })
                );
            })
        );
    }

    getAllRoles(): Observable<RoleOutputDto[]> {
        return this.currentUser.pipe(
            map((currentUser) => {
                return currentUser.token;
            }),
            switchMap((token) => {
                const url = `${this.rolesUrl}/all`;
                const req = this.api
                    .queryBuilder(url)
                    .get()
                    .addJwtAuth(token)
                    .build();

                return this.api.send(req).pipe(
                    map((roles: RoleOutputDto[]) => {
                        console.log("roles " + roles.length)
                        return roles;
                    })
                );
            })
        );
    }

    getUserRoleById(uid: number): Observable<RoleOutputDto[]> {
        return this.currentUser.pipe(
            map((currentUser) => {
                return currentUser.token;
            }),
            switchMap((token) => {
                const url = `${this.rolesUrl}/${uid}/user`;
                const req = this.api
                    .queryBuilder(url)
                    .get()
                    .addJwtAuth(token)
                    .build();

                return this.api.send(req).pipe(
                    map((roles: RoleOutputDto[]) => {
                        return roles;
                    })
                );
            }),
            catchError((e) => {
                this.alert.showError(`${this.displayError(e)}`);
                return of(null)
            })
        );
    }


    getRoleById(rid: number): Observable<RoleOutputDto[]> {
        return this.currentUser.pipe(
            map((currentUser) => {
                return currentUser.token;
            }),
            switchMap((token) => {
                const url = `${this.rolesUrl}/${rid}/role`;
                const req = this.api
                    .queryBuilder(url)
                    .get()
                    .addJwtAuth(token)
                    .build();

                return this.api.send(req).pipe(
                    map((roles: RoleOutputDto[]) => {
                        return roles;
                    })
                );
            }),
            catchError((e) => {
                this.alert.showError(`${this.displayError(e)}`);
                return of(null)
            })
        );
    }

    assignRole(
        uid: number,
        rids: number[],
    ): Observable<any> {
        const url = `${this.rolesUrl}/${uid}/user`;

        return this.currentUser
            .pipe(
                take(1),
                map((user) => {
                    if (!user) {
                        throw new Error('Please log in');
                    }
                    return user;
                }),
                switchMap((user) => {
                    const query = this.api
                        .queryBuilder(url)
                        .post()
                        .body({rids})
                        .addJwtAuth(user.token)
                        .build();

                    return this.api
                        .send(query, { skipLoading: true })
                        .pipe(map(() => of(null)));
                }),
                catchError((e) => {
                    this.alert.showError(`${this.displayError(e)}`);
                    return of(e);
                })
            )
    }

    addNewRole(
        roleData: Partial<AddRolePermissionsDto>
    ): Observable<any> {
        const url = `${this.rolesUrl}`;

        return this.currentUser
            .pipe(
                take(1),
                map((user) => {
                    if (!user) {
                        throw new Error('Please log in');
                    }
                    return user;
                }),
                switchMap((user) => {
                    const query = this.api
                        .queryBuilder(url)
                        .post()
                        .body(roleData)
                        .addJwtAuth(user.token)
                        .build();

                    return this.api
                        .send(query, { skipLoading: true })
                        .pipe(map(() => {
                            this.alert.showMessage('Role created successfully.');
                            return of(null)
                        }));
                }),
                catchError((e) => {
                    this.alert.showError(`${this.displayError(e)}`);
                    return of(e);
                })
            )
    }

    updateRole(
        rid: number,
        roleData: Partial<AddRolePermissionsDto>,
    ): Observable<any> {
        const url = `${this.rolesUrl}/${rid}/role`

        return this.currentUser
            .pipe(
                take(1),
                map((user) => {
                    if (!user) {
                        throw new Error('Please log in');
                    }
                    return user;
                }),
                switchMap((user) => {
                    const query = this.api
                        .queryBuilder(url)
                        .patch()
                        .body(roleData)
                        .addJwtAuth(user.token)
                        .build();

                    return this.api
                        .send(query, { skipLoading: true })
                        .pipe(map(() => {
                            this.alert.showMessage('Role and permissions updated successfully.');
                            return of(null)
                        }));
                }),
                catchError((e) => {
                    this.alert.showError(`${this.displayError(e)}`);
                    return of(e);
                })
            )
    }

    updateUserRoles(
        uid: number,
        rids: number[]
    ): Observable<any> {
        const url = `${this.rolesUrl}/${uid}/user`;

        return this.currentUser
            .pipe(
                take(1),
                map((user) => {
                    if (!user) {
                        throw new Error('Please log in');
                    }
                    return user;
                }),
                switchMap((user) => {
                    const query = this.api
                        .queryBuilder(url)
                        .patch()
                        .body({rids})
                        .addJwtAuth(user.token)
                        .build();

                    return this.api
                        .send(query, { skipLoading: true })
                        .pipe(map(() => of(null)));
                }),
                catchError((e) => {
                    this.alert.showError(`${this.displayError(e)}`);
                    return of(e);
                })
            )
    }

    deleteUser(uid: number): Observable<any> {
        return this.currentUser
            .pipe(
                take(1),
                map((user) => {
                    if (!user) {
                        throw new Error('Please log in');
                    }
                    return user;
                }),
                switchMap((user) => {
                    const url = `${this.usersUrl}/${uid}`;
                    const query = this.api
                        .queryBuilder(url)
                        .delete()
                        .addJwtAuth(user.token)
                        .build();

                    return this.api
                        .send(query, { skipLoading: true })
                        .pipe(map(() => {
                            this.alert.showMessage('User deleted successfully.');
                            return of(null);
                        }));
                }),
                catchError((e) => {
                    this.alert.showError(`${this.displayError(e)}`);
                    return of(e);
                })
            );
    }

    unassignUserRoles(uid: number): Observable<any> {
        return this.currentUser
            .pipe(
                take(1),
                map((user) => {
                    if (!user) {
                        throw new Error('Please log in');
                    }
                    return user;
                }),
                switchMap((user) => {
                    const url = `${this.rolesUrl}/${uid}/user`;
                    const query = this.api
                        .queryBuilder(url)
                        .delete()
                        .addJwtAuth(user.token)
                        .build();

                    return this.api
                        .send(query, { skipLoading: true })
                        .pipe(map(() => {
                            this.alert.showMessage('roles unassigned successfully.');
                            return of(null);
                        }));
                }),
                catchError((e) => {
                    this.alert.showError(`${this.displayError(e)}`);
                    return of(e);
                })
            );
    }


    deleteRole(rid: number): Observable<any> {
        return this.currentUser
            .pipe(
                take(1),
                map((user) => {
                    if (!user) {
                        throw new Error('Please log in');
                    }
                    return user;
                }),
                switchMap((user) => {
                    const url = `${this.rolesUrl}/${rid}/role`;
                    const query = this.api
                        .queryBuilder(url)
                        .delete()
                        .addJwtAuth(user.token)
                        .build();

                    return this.api
                        .send(query, { skipLoading: true })
                        .pipe(map(() => {
                            this.alert.showMessage('Role deleted successfully.');
                            return of(null);
                        }));
                }),
                catchError((e) => {
                    this.alert.showError(`${this.displayError(e)}`);
                    return of(e);
                })
            );
    }


    getAllPermissionRequests(): Observable<any[]> {
        return this.currentUser.pipe(
            map((currentUser) => {
                return currentUser.token;
            }),
            switchMap((token) => {
                const url = `${this.permissionsUrl}/requests`;
                const req = this.api
                    .queryBuilder(url)
                    .get()
                    .addJwtAuth(token)
                    .build();

                return this.api.send(req).pipe(
                    map((permissions: PermissionOutputDto[]) => {
                        return permissions;
                    })
                );
            })
        );
    }


    getAllPermissions(): Observable<PermissionOutputDto[]> {
        return this.currentUser.pipe(
            map((currentUser) => {
                return currentUser.token;
            }),
            switchMap((token) => {
                const url = `${this.permissionsUrl}`;
                const req = this.api
                    .queryBuilder(url)
                    .get()
                    .addJwtAuth(token)
                    .build();

                return this.api.send(req).pipe(
                    map((permissions: PermissionOutputDto[]) => {
                        return permissions;
                    })
                );
            })
        );
    }


    getOnePermission(id:number): Observable<PermissionOutputDto> {
        return this.currentUser.pipe(
            map((currentUser) => {
                return currentUser.token;
            }),
            switchMap((token) => {
                const url = `${this.permissionsUrl}/${id}`;
                const req = this.api
                    .queryBuilder(url)
                    .get()
                    .addJwtAuth(token)
                    .build();

                return this.api.send(req).pipe(
                    map((permission: PermissionOutputDto) => {
                        return permission;
                    })
                );
            })
        );
    }


    createNewPermission(permission: {name: string}): Observable<any> {
        return this.currentUser
            .pipe(
                take(1),
                map((user) => {
                    if (!user) {
                        throw new Error('Please log in');
                    }
                    return user;
                }),
                switchMap((user) => {
                    const url = `${this.permissionsUrl}`;
                    const query = this.api
                        .queryBuilder(url)
                        .post()
                        .body(permission)
                        .addJwtAuth(user.token)
                        .build();

                    return this.api
                        .send(query, { skipLoading: true })
                        .pipe(map(() => {
                            this.alert.showMessage(`Permission created successfully.`)
                        }));
                }),
                catchError((e) => {
                    this.alert.showError(`${this.displayError(e)}`);
                    return of(e);
                })
            );
    }


    updatePermission(id: number, data: {name: string}): Observable<any> {
        return this.currentUser
            .pipe(
                take(1),
                map((user) => {
                    if (!user) {
                        throw new Error('Please log in');
                    }
                    return user;
                }),
                switchMap((user) => {
                    const url = `${this.permissionsUrl}/${id}`;
                    const query = this.api
                        .queryBuilder(url)
                        .patch()
                        .body(data)
                        .addJwtAuth(user.token)
                        .build();

                    return this.api
                        .send(query, { skipLoading: true })
                        .pipe(map(() => {
                            this.alert.showMessage(`Permission updated successfully.`)
                        }));
                }),
                catchError((e) => {
                    this.alert.showError(`${this.displayError(e)}`);
                    return of(e);
                })
            );
    }


    deletePermission(id: number): Observable<any> {
        return this.currentUser
            .pipe(
                take(1),
                map((user) => {
                    if (!user) {
                        throw new Error('Please log in');
                    }
                    return user;
                }),
                switchMap((user) => {
                    const url = `${this.permissionsUrl}/${id}`;
                    const query = this.api
                        .queryBuilder(url)
                        .delete()
                        .addJwtAuth(user.token)
                        .build();

                    return this.api
                        .send(query, { skipLoading: true })
                        .pipe(map(() => {
                            this.alert.showMessage(`Permission deleted successfully.`)
                        }));
                }),
                catchError((e) => {
                    this.alert.showError(`${this.displayError(e)}`);
                    return of(e);
                })
            );
    }


    requestPermission(permissionRequest: PermissionRequestDto): Observable<any> {
        return this.currentUser
            .pipe(
                take(1),
                map((user) => {
                    if (!user) {
                        throw new Error('Please log in');
                    }
                    return user;
                }),
                switchMap((user) => {
                    const url = `${this.permissionsUrl}/request`;
                    const query = this.api
                        .queryBuilder(url)
                        .post()
                        .body(permissionRequest)
                        .addJwtAuth(user.token)
                        .build();

                    return this.api
                        .send(query, { skipLoading: true })
                        .pipe(map(() => null));
                }),
                catchError((e) => {
                    this.alert.showError(`${this.displayError(e)}`);
                    return of(e);
                })
            );
    }


    private displayError(e) {
        return e?.error?.message || 'An Error occurred processing your request.';
    }


    private get loginUrl() {
        return `${this.api.apiRoot()}/auth/login`;
    }

    private get logoutUrl() {
        return `${this.api.apiRoot()}/auth/logout`;
    }

    private get refreshUrl() {
        return `${this.api.apiRoot()}/auth/refresh`;
    }

    private get usersUrl() {
        return `${this.api.apiRoot()}/users`;
    }

    private get rolesUrl() {
        return `${this.api.apiRoot()}/userroles`;
    }

    private get permissionsUrl() {
        return `${this.api.apiRoot()}/permissions`;
    }

    private notificationUrl(uid: number) {
        return `${this.api.apiRoot()}/users/${uid}/notifications`;
    }

}
