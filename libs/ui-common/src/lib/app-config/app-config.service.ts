import { Injectable } from '@angular/core';
import { AppEnv } from './AppEnv.class';

@Injectable({
    providedIn: 'root'
})
export class AppConfigService {

    constructor(private readonly environment: AppEnv) { }

    defaultLanguage(): string {
        return this.environment.defaultLanguage;
    }

    appTitle(): string {
        return this.environment.appTitle;
    }

    apiUri(): string {
        return this.environment.apiUrl;
    }

    tilesUrl(): string {
        let url = this.environment.tilesUrl;
        if (this.environment.tilesToken) {
            url += `?access_token=${this.environment.tilesToken}`;
        }
        return url;
    }

    tilesAttribution(): string {
        return this.environment.tilesAttribution;
    }

    enableFooter(): boolean {
        return this.environment.enableFooter;
    }

    kibanaServer(): string {
        return this.environment.kibanaServer;
    }

    kibanaPort(): string {
        return this.environment.kibanaPort;
    }

    kibanaDashboardSpatialModule(): string {
        return this.environment.kibanaDashboardSpatialModule;
    }

    kibanaDashboardImageSearch(): string {
        return this.environment.kibanaDashboardImageSearch;
    }

    kibanaDashboardCollectionStats(): string {
        return this.environment.kibanaDashboardCollectionStats;
    }

}
