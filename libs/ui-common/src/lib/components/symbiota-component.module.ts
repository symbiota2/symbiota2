import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetectLinksPipe } from './detect-links.pipe';
import { Symbiota2ExpansionPanelComponent } from './expansion-panel/expansion-panel.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { FileUploadFieldComponent } from './file-upload-field/file-upload-field.component';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { FlexModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { KibanaDashboardLinkComponent } from './kibana-dashboard-link/kibana-dashboard-link.component';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
    declarations: [
        DetectLinksPipe,
        Symbiota2ExpansionPanelComponent,
        FileUploadFieldComponent,
        KibanaDashboardLinkComponent
    ],
    imports: [
        CommonModule,
        MatExpansionModule,
        MatButtonModule,
        MatInputModule,
        FlexModule,
        FormsModule,
        MatIconModule,
        MatMenuModule,
        MatTooltipModule,
        TranslateModule
    ],
    exports: [
        DetectLinksPipe,
        Symbiota2ExpansionPanelComponent,
        FileUploadFieldComponent,
        KibanaDashboardLinkComponent
    ]
})
export class SymbiotaComponentModule {
}
