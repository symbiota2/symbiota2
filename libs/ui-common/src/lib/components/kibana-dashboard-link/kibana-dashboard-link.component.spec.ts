import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KibanaDashboardLinkComponent } from './kibana-dashboard-link.component';

describe('ExpansionPanelComponent', () => {
  let component: KibanaDashboardLinkComponent;
  let fixture: ComponentFixture<KibanaDashboardLinkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KibanaDashboardLinkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KibanaDashboardLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
