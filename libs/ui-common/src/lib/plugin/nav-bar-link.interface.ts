export interface NavBarLink {
    name: string;
    url?: string;
    subMenu?: NavBarLink[]
    openInNewTab?: boolean
}
