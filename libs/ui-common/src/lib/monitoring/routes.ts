export const MONITORING_API_BASE = "health"
export const MONITORING_UI_BASE = "health"
export const MONITORING_API_UP_CHECK_ROUTE = MONITORING_UI_BASE + "/isAPIUp"
