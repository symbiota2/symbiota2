export const CHECKLIST_LIST_ROUTE = 'checklists';
export const CHECKLIST_PROJECT_LIST_ROUTE = 'projects';
export const CHECKLIST_PROJECT_ROUTE = 'project';
export const CHECKLIST_ROUTE = 'checklist';
export const CHECKLIST_REGIONAL_ROUTE = 'checklist/regional';
export const CHECKLIST_PROJECT_ID_ROUTE = CHECKLIST_PROJECT_LIST_ROUTE + '/:projectId';
export const CHECKLIST_PROJECT_ID_CHECKLIST_LIST_ROUTE = CHECKLIST_PROJECT_ID_ROUTE + '/' + CHECKLIST_LIST_ROUTE;
export const CHECKLIST_ID_ROUTE = CHECKLIST_LIST_ROUTE + '/:checklistId';
export const CHECKLIST_TAXA_DISPLAY = CHECKLIST_PROJECT_ID_ROUTE + '/' +  CHECKLIST_ID_ROUTE;

export const CHECKLIST_TEACHING_ROUTE = 'checklist/teaching';
export const CHECKLIST_ADMIN_ROUTE = CHECKLIST_ROUTE + 'checklistadmin/:checklistId';
export const PROFILE_VIEWER_ROUTE = 'profile/viewprofile';
export const CHECKLIST_CREATE_ROUTE = CHECKLIST_ROUTE + '/create';
export const CHECKLIST_CREATE_INVENTORY_ROUTE = CHECKLIST_CREATE_ROUTE + '/inventory';
export const CHECKLIST_CREATE_TEACHING_ROUTE = CHECKLIST_CREATE_ROUTE + '/teaching';
export const CHECKLIST_CREATE_RARE_ROUTE = CHECKLIST_CREATE_ROUTE + '/rare';
export const CHECKLIST_CREATE_TERMINOLOGY_ROUTE = CHECKLIST_CREATE_ROUTE + '/terminology';
export const CHECKLIST_TOOLS_CHECKLIST_UPLOAD_BATCH = CHECKLIST_TAXA_DISPLAY + '/tools/batchupload';

export const GAMES_ROUTE = 'games';
export const FlashCARD_QUIZ_ROUTE = GAMES_ROUTE + '/flashcards'