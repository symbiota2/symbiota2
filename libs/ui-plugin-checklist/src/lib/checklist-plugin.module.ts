import { NgModule } from '@angular/core';
import { TranslateModule } from "@ngx-translate/core";
import { CommonModule } from '@angular/common';
import { NavBarLink, SymbiotaUiPlugin } from '@symbiota2/ui-common';

import {
  CHECKLIST_LIST_ROUTE,
  CHECKLIST_PROJECT_LIST_ROUTE,
  CHECKLIST_PROJECT_ROUTE,
  CHECKLIST_PROJECT_ID_ROUTE,
  CHECKLIST_REGIONAL_ROUTE,
  CHECKLIST_CREATE_INVENTORY_ROUTE,
  CHECKLIST_CREATE_TEACHING_ROUTE,
  CHECKLIST_CREATE_RARE_ROUTE,
  CHECKLIST_CREATE_TERMINOLOGY_ROUTE,
  CHECKLIST_ID_ROUTE,
  CHECKLIST_ADMIN_ROUTE,
  CHECKLIST_CREATE_ROUTE,
  CHECKLIST_TEACHING_ROUTE,
  CHECKLIST_PROJECT_ID_CHECKLIST_LIST_ROUTE,
  CHECKLIST_TAXA_DISPLAY,
  CHECKLIST_TOOLS_CHECKLIST_UPLOAD_BATCH,
  FlashCARD_QUIZ_ROUTE
 } from './routes';
import { Route, RouterModule } from '@angular/router';
import { ChecklistCreatePageComponent } from './pages/checklist-create/checklist-create-page.component';
import { ChecklistRegionalPageComponent } from './pages/checklist-regional/checklist-regional-page.component';
import { ChecklistTeachingPageComponent } from './pages/checklist-teaching/checklist-teaching-page.component';
import { ChecklistService } from './services/checklist/checklist.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule} from '@angular/material/card';
import { ChecklistSinglePageComponent } from './pages/checklist-single/checklist-single-page.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import {MatRadioModule} from '@angular/material/radio';

import { ChecklistDisplayTaxaComponent } from './components/checklist-display-taxa/checklist-taxa.component';
import { ChecklistListTaxaComponent } from './components/checklist-taxon-list/checklist-taxon-list.component';
import { ChecklistUploadTaxonComponent } from './components/checklist-upload-taxon/checklist-upload-taxon.component';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ChecklistBatchUploadTaxaComponent } from './components/checklist-batch-upload-taxa/checklist-batch-upload-taxa.component';
import { ChecklistTaxonSearchOptionsComponent } from './components/checklist-search-filter/checklist-search-filter.component';
import { BrowserModule } from '@angular/platform-browser';
import { ChecklistTaxaFilterService } from './components/filter-form-service';
import { ChecklistDto } from './dto/checklist';
import { FlashCardGameComponent } from './components/flash-card-game/flash-card-game.component';
import { FlashCardGameService } from './components/flash-card.service';
import { LocalMenu } from './local-menu';
// import { AuthGuard } from './guard/auth.guard';

@NgModule({
  imports: [
    FormsModule,
    BrowserModule,
    CommonModule,
    TranslateModule,
    ReactiveFormsModule,
    RouterModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule,
    MatButtonModule,
    MatDividerModule,
    MatListModule,
    MatIconModule,
    MatTooltipModule,
    MatRadioModule,
  ],
  providers: [ChecklistService, ChecklistTaxaFilterService, FlashCardGameService],
    declarations: [
    ChecklistCreatePageComponent,
    ChecklistTeachingPageComponent,
    ChecklistRegionalPageComponent,
    ChecklistSinglePageComponent,
    ChecklistDisplayTaxaComponent,
    ChecklistListTaxaComponent,
    ChecklistUploadTaxonComponent,
    ChecklistBatchUploadTaxaComponent,
    ChecklistTaxonSearchOptionsComponent,
    FlashCardGameComponent,
  ],
    /*
  entryComponents: [
    ChecklistCreatePageComponent,
    ChecklistTeachingPageComponent,
    ChecklistRegionalPageComponent,
    ChecklistSinglePageComponent,
    ChecklistDisplayTaxaComponent,
    ChecklistListTaxaComponent,
    ChecklistUploadTaxonComponent,
    ChecklistBatchUploadTaxaComponent,
    ChecklistTaxonSearchOptionsComponent,
    FlashCardGameComponent,
  ],

     */
})
export class ChecklistPlugin extends SymbiotaUiPlugin {
  static readonly PLUGIN_NAME = 'plugins.checklist.name';

  constructor() {
    super();
  }

  static routes(): Route[] {
    const arr2 = LocalMenu.routes()
    if (arr2.length > 0) {
        return arr2
    }
    return [
      {
        path: CHECKLIST_PROJECT_LIST_ROUTE,
        component: ChecklistRegionalPageComponent,
      },
      {
        path: CHECKLIST_TEACHING_ROUTE,
        component: ChecklistTeachingPageComponent,
      },
      {
        path: CHECKLIST_CREATE_INVENTORY_ROUTE,
        component: ChecklistCreatePageComponent,
      },
      {
        path: CHECKLIST_CREATE_TEACHING_ROUTE,
        component: ChecklistCreatePageComponent,
      },
      {
        path: CHECKLIST_CREATE_RARE_ROUTE,
        component: ChecklistCreatePageComponent,
      },
      {
        path: CHECKLIST_CREATE_TERMINOLOGY_ROUTE,
        component: ChecklistCreatePageComponent,
      },
      {
        path: CHECKLIST_PROJECT_ID_ROUTE,
        component: ChecklistSinglePageComponent
      },
      {
        path: CHECKLIST_TAXA_DISPLAY,
        component: ChecklistDisplayTaxaComponent
      },
      {
        path: CHECKLIST_TOOLS_CHECKLIST_UPLOAD_BATCH,
        component: ChecklistBatchUploadTaxaComponent
      },
      {
        path: FlashCARD_QUIZ_ROUTE,
        component: FlashCardGameComponent
      }
    ];
  }

  static navBarLinks(): NavBarLink[] {
    const arr2 = LocalMenu.navBarLinks()
    if (arr2.length > 0) {
        return arr2
    }
      return [
          {
            url: `/${CHECKLIST_PROJECT_LIST_ROUTE}`,
            name: 'core.layout.header.topnav.checklist.regional.link',
          },
          {
            url: `/${CHECKLIST_TEACHING_ROUTE}`,
            name: 'core.layout.header.topnav.checklist.teaching.link',
          },
          {
            name: 'core.layout.header.topnav.checklist.create.link',
            subMenu: [
              {
                  url: `/${CHECKLIST_CREATE_INVENTORY_ROUTE}`,
                  name: "core.layout.header.topnav.checklist.create.inventory.checklist"
              },
              {
                  url: `/${CHECKLIST_CREATE_TEACHING_ROUTE}`,
                  name: "core.layout.header.topnav.checklist.create.teaching.checklist"
              },
              {
                  url: `/${CHECKLIST_CREATE_RARE_ROUTE}`,
                  name: "core.layout.header.topnav.checklist.create.rare.checklist"
              },
              {
                url: `/${CHECKLIST_CREATE_TERMINOLOGY_ROUTE}`,
                name: "core.layout.header.topnav.checklist.create.terminology.checklist"
            }
          ]
          },
      ];
  }
}
