import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsOptional } from 'class-validator';

export class UpdateArchiveQuery {
    @ApiProperty({ type: 'boolean', required: false })
    @IsBoolean()
    @IsOptional()
    publish: boolean;

    @ApiProperty({ type: 'boolean', required: false })
    @IsBoolean()
    @IsOptional()
    refresh: boolean;
}
