import { Module } from '@nestjs/common';
import { UserModule } from '@symbiota2/api-auth';
import { SymbiotaApiPlugin } from '@symbiota2/api-common';
import { AppConfigModule } from '@symbiota2/api-config';
import { DatabaseModule } from '@symbiota2/api-database';
// import { ChecklistController, ChecklistService } from '..';
import { ChecklistController } from './checklist/checklist.controller';
import { ChecklistService } from './checklist/project.service';

@Module({
  imports: [
    AppConfigModule, 
    DatabaseModule,
    UserModule,
  ],
  controllers: [ChecklistController],
  providers: [ChecklistService],
  exports: [],
})
export class ChecklistModule extends SymbiotaApiPlugin {}
