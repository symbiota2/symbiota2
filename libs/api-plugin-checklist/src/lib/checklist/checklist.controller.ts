import {
    Controller,
    Get,
    HttpStatus,
    NotFoundException,
    Query,
    Param,
    Post,
    UseGuards,
    HttpCode,
    Req,
    Body,
    Delete, Patch,
    ClassSerializerInterceptor,
    UseInterceptors,
    ForbiddenException, Inject
} from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiOperation, ApiResponse, ApiTags } from "@nestjs/swagger";
import { AuthenticatedRequest, JwtAuthGuard, TokenService } from "@symbiota2/api-auth";

import { Checklist } from "@symbiota2/api-database";
import { ChecklistDto } from "./dto/checklist-dto";
import { ChecklistInputDto } from "./dto/checklist-input.dto";
import { ChecklistTaxonLinkDto } from "./dto/checklist-taxon-link.dto";
import { ProjectDto } from "./dto/project-dto";
import { ProjectLinkDto } from "./dto/project-link-dto";
import { ChecklistService } from "./project.service";
import { TaxonDto } from "./dto/TaxonDto";
import { TaxonomicStatusDto } from "./dto/TaxonomicStatusDto";
import { ChecklistTaxaImages } from "./dto/taxon-images.dto";
import { TaxonVernacularOutputDto } from "./dto/checklist-taxon-vernacular-names.dto";
import { ProjectFindAllParams, TaxonFindAllParams } from "./dto/project-find-params";

@ApiTags('Checklist')
@Controller()
export class ChecklistController {
   checklistsIds: number[] = [];
   checklistImages
   checklistChildren
   checklistParent
   checklistTree

   constructor(
       @Inject(ChecklistService)
       private projectService: ChecklistService
   ) {}



   //The default controller fetches all of the records
   @Get('projects')
   @ApiResponse({ status: HttpStatus.OK, type: ProjectDto, isArray: true })
   @ApiOperation({
       summary: "Retrieve a list of projects.  The list can be narrowed by project ID"
   })
   async findAllProject(@Query() findAllParams: ProjectFindAllParams): Promise<ProjectDto[]> {
       const projects = await this.projectService.findAllProjects(findAllParams)
       if (!projects) {
           return []
       }
       const projectsDto = projects.map(async (p) => {
           const project = new ProjectDto(p)
           return project
       });
       return Promise.all(projectsDto)
   }





   @Get('checklists')
   @ApiResponse({ status: HttpStatus.OK, type: ChecklistDto, isArray: true})
   @ApiOperation({ summary: "Retrieve all checklists." })
   async findAllChecklists(): Promise<ChecklistDto[] | []> {

       const checklists = await this.projectService.findAllChecklists();

       if (!checklists) return [];

       const checklistsDto = checklists.map(async (c) => {
           const checklist = new ChecklistDto(c)
           return checklist
       })
       return Promise.all(checklistsDto)
   }





   @Get('projects/:pid')
   @ApiResponse({ status: HttpStatus.OK, type: ProjectLinkDto })
   @ApiOperation({
       summary: "find a project by id"
   })
   async findProjectById(@Param('pid') pid: number): Promise<ProjectDto> {

       const project = await this.projectService.findProjectById(pid)
       if (!project) {
           throw new NotFoundException('Project not found!')
       }

       const projectDto = new ProjectDto(project);

       return projectDto;
   }





   @Get('projects/:pid/checklists')
   @ApiResponse({ status: HttpStatus.OK, type: ProjectLinkDto })
   @ApiOperation({
       summary: "find a project checklists by pid"
   })
   async findChecklistsByProjectId(@Param('pid') pid: number): Promise<ChecklistDto[]> {

       const checklistProjectLink = await this.projectService.findProjectchecklists(pid)
       if (!checklistProjectLink) {
           throw new NotFoundException()
       }

       const checklistProjectLinkDto = checklistProjectLink.map(async (c) => {
           const checklistDto = new ChecklistDto(await c.checklist);
           return checklistDto;
       })

       return Promise.all(checklistProjectLinkDto)
   }






  @Get('projects/:pid/checklists/:clid')
  @UseInterceptors(ClassSerializerInterceptor)
   @ApiResponse({ status: HttpStatus.OK, type: ChecklistDto })
   @ApiOperation({
       summary: "find single checklist, checklist taxa and parents by checklist id"
   })
   async findChecklistById(@Param('pid') pid: number, @Param('clid') clid: number) {
       const projectChecklists = await this.projectService.findProjectchecklists(pid)

       const projectChecklistLink = projectChecklists.find(c => c.checklistID === clid)
       if (!projectChecklistLink) throw new NotFoundException('Checklist not found!')
       const checklistDto = new ChecklistDto(await projectChecklistLink.checklist)

       const builder = await this.projectService.findTaxaByChecklist(clid)

       const taxonDto = builder.map(async (c, i) => {
           const checklistTaxon = await c.taxon;

           const taxon = new TaxonDto(checklistTaxon)

           taxon.family = await checklistTaxon.family()

           // const taxonParentDto = (await checklistTaxon.taxaEnumEntries).map(async (t, i) => {
           //     const p = await t.parentTaxon;

           //     const parentTaxon = new TaxonDto(p)
           //     return parentTaxon
           // });
           // taxon.parentTaxon = await Promise.all(taxonParentDto)

           const taxonImagesDto = (await checklistTaxon.images).map(async (images, i) => {
               const taxonImages = new ChecklistTaxaImages(images)
               return taxonImages
           });
           taxon.images = await Promise.all(taxonImagesDto)

           const acceptedTaxonStatusesDto = (await checklistTaxon.acceptedTaxonStatuses).map(async (acceptedTaxonStatuses, i) => {
               const taxonStatuse = new TaxonomicStatusDto(acceptedTaxonStatuses)
               return taxonStatuse
           })
           taxon.acceptedTaxonStatuses = await Promise.all(acceptedTaxonStatusesDto)

           const taxonVernacularNamesDto = (await checklistTaxon.vernacularNames).map(async (name, i) => {
               const vernacularName = new TaxonVernacularOutputDto(name)
               return vernacularName
           });
           taxon.vernacularNames = await Promise.all(taxonVernacularNamesDto)

           return taxon
       });

       // async findAncestorTaxons(@Param('taxonID') taxonid: string, @Query() findAllParams: TaxonomicEnumTreeFindAllParams): Promise<TaxonDto[]> {
       // const taxons = await this.projectService.findAncestors(taxonid, findAllParams)
       // const taxonDtos = taxons.map(async (c) => {
       //     const parentTaxon = await c.parentTaxon
       //     const acceptedStatuses = await parentTaxon.acceptedTaxonStatuses
       //     const synonyms = []
       //     for (const s of acceptedStatuses) {
       //         const synstatus = new TaxonomicStatusDto(s)
       //         const t = await s.taxon
       //         synstatus.taxon = new TaxonDto(t)
       //         synonyms.push(synstatus)
       //     }
       //     const taxon = new TaxonAndAcceptedStatusesDto(parentTaxon)
       //     taxon.acceptedTaxonStatuses = synonyms
       //     return taxon
       // })
       // return Promise.all(taxonDtos)

       return  [
           checklistDto,
           await Promise.all(taxonDto)
       ];
   }



   private canEdit(request, checklistID) {
       // SuperAdmins and ChecklsitProfileEditors have editing privileges
       const isSuperAdmin = TokenService.isSuperAdmin(request.user)
       const isEditor = TokenService.isChecklistAdmin(request.user, checklistID)
       return isSuperAdmin || isEditor
   }


   // The default controller fetches all of the records
   @Get('checklist/taxa')
   @ApiResponse({ status: HttpStatus.OK, type: TaxonDto, isArray: true })
   @ApiOperation({
       summary: "Retrieve a list of taxa.  The list can be narrowed by taxon IDs and/or a taxon authority."
   })
   async findAll(@Query() findAllParams: TaxonFindAllParams): Promise<TaxonDto[]> {
       const taxons = await this.projectService.findAllTaxa(findAllParams)
       if (!taxons) {
           return []
       }

       const taxonDtos = taxons.map(async (c) => {
           const taxonData = await c;
           const taxon = new TaxonDto(taxonData)

           // get common names
           const taxonVernacularNamesDto = (await taxonData.vernacularNames).map(async (name, i) => {
               const vernacularName = new TaxonVernacularOutputDto(name)
               return vernacularName
           });
           taxon.vernacularNames = await Promise.all(taxonVernacularNamesDto)

           return taxon
       });
       return Promise.all(taxonDtos)
   }






   @Post('checklists')
   @ApiOperation({
       summary: "Create a new checklist"
   })
   @ApiBearerAuth()
   @UseGuards(JwtAuthGuard)
   @HttpCode(HttpStatus.CREATED)
   @ApiResponse({ status: HttpStatus.CREATED, type: ChecklistDto })
   //@SerializeOptions({ groups: ['single'] })
   @ApiBody({ type: ChecklistInputDto, isArray: false })
   /**
    @see - @link ChecklistInputDto
    **/
   async createChecklist(
       @Req() request: AuthenticatedRequest,
       @Body() data: Partial<Checklist>
   ): Promise<ChecklistDto> {

       const checklist = await this.projectService.createChecklist(data)
       const dto = new ChecklistDto(checklist)
       // if (!this.canEdit(request, dto.creatorUID)) {
       //     throw new ForbiddenException()
       // }
       return dto
   }






   @Patch('checklists/:id')
   @ApiOperation({
       summary: "Update a checklist"
   })
   @ApiBearerAuth()
   @UseGuards(JwtAuthGuard)
   @HttpCode(HttpStatus.OK)
   @ApiResponse({ status: HttpStatus.OK, type: ChecklistDto })
   @ApiBody({ type: ChecklistInputDto, isArray: false })
   /**
    @see - @link ChecklistInputDto
    **/
   async updateChecklist(
       @Req() request: AuthenticatedRequest,
       @Param('id') id: number,
       @Body() data: Partial<Checklist>
   ): Promise<ChecklistDto> {

       const checklist = await this.projectService.updateChecklist(id, data)
       const dto = new ChecklistDto(checklist)
       return dto
   }





   @Delete('checklists/:id')
   @ApiOperation({
       summary: "Delete a checklist"
   })
   @ApiBearerAuth()
   @UseGuards(JwtAuthGuard)
   @HttpCode(HttpStatus.OK)
   async deleteChecklist(
       @Req() request: AuthenticatedRequest,
       @Param('id') id: number
   ): Promise<void> {
       this.projectService.deleteChecklist(id)
   }





   @Post('projects/:pid/checklists/:clid/taxon')
   @ApiOperation({
       summary: "Add a taxon to checklist"
   })
   @ApiBearerAuth()
   @UseGuards(JwtAuthGuard)
   @HttpCode(HttpStatus.OK)
   async uploadChecklistTaxon(
       @Req() request: AuthenticatedRequest,
       @Param('clid') id: string,
       @Body() data: Partial<ChecklistTaxonLinkDto>
   ): Promise<ChecklistTaxonLinkDto> {

       const checklistTaxonLink = await this.projectService.uploadChecklistTaxon(parseInt(id), data );

       const checklistTaxonLinkDto = new ChecklistTaxonLinkDto(checklistTaxonLink);
       return checklistTaxonLinkDto;
   }








   // @Get('ancestorTaxons/:taxonID')
   // @ApiResponse({ status: HttpStatus.OK, type: TaxonDto })
   // @ApiOperation({
   //     summary: "Retrieve all of the ancestor taxons for a given taxon ID."
   // })
   // async findAncestorTaxons(@Param('taxonID') taxonid: string, @Query() findAllParams: TaxonomicEnumTreeFindAllParams): Promise<TaxonDto[]> {
   //     const taxons = await this.projectService.findAncestors(taxonid, findAllParams)
   //     const taxonDtos = taxons.map(async (c) => {
   //         const parentTaxon = await c.parentTaxon
   //         const acceptedStatuses = await parentTaxon.acceptedTaxonStatuses
   //         const synonyms = []
   //         for (const s of acceptedStatuses) {
   //             const synstatus = new TaxonomicStatusDto(s)
   //             const t = await s.taxon
   //             synstatus.taxon = new TaxonDto(t)
   //             synonyms.push(synstatus)
   //         }
   //         const taxon = new TaxonAndAcceptedStatusesDto(parentTaxon)
   //         taxon.acceptedTaxonStatuses = synonyms
   //         return taxon
   //     })
   //     return Promise.all(taxonDtos)
   // }





}

