import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsArray, IsInt, Max, Min } from 'class-validator';
import { Type } from 'class-transformer';
import { BaseFindAllParams } from '@symbiota2/api-common';

export class TaxonomicEnumTreeFindAllParams extends BaseFindAllParams {
    static readonly TAXON_ENUM_DEFAULT_LIMIT = 10
    static readonly TAXON_ENUM_DEFAULT_OFFSET = 1
    static readonly TAXON_ENUM_MAX_LIMIT = 15
    static readonly DEFAULT_AUTHORITY = 1

    @ApiProperty({ name: 'taxonAuthorityID', type: Number, required: false, default: TaxonomicEnumTreeFindAllParams.DEFAULT_AUTHORITY })
    @Type(() => Number)
    @IsInt({ each: true })
    @IsOptional()
    taxonAuthorityID: number

    @ApiProperty({ name: 'taxonID[]', type: [Number], required: false })
    @Type(() => Number)
    @IsArray()
    @IsInt({ each: true })
    @IsOptional()
    taxonID: number[]

    @Min(0)
    @Max(TaxonomicEnumTreeFindAllParams.TAXON_ENUM_MAX_LIMIT)
    limit: number = TaxonomicEnumTreeFindAllParams.TAXON_ENUM_DEFAULT_LIMIT;

    @ApiProperty({ required: false, default: TaxonomicEnumTreeFindAllParams.TAXON_ENUM_DEFAULT_OFFSET })
    @Min(0)
    offset: number // = TaxonomicEnumTreeFindAllParams.TAXON_ENUM_DEFAULT_OFFSET;

    @IsOptional()
    s: string;
}
