import { Exclude, Expose } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { Medium, OccurrenceDetermination, ReferenceTaxonLink, Taxon, TaxonMap, Unknown, Image, OccurrenceType, TaxaNestedTreeEntry, TaxonVernacular, DynamicChecklistTaxonLink, CharacteristicDescription, GlossarySource, UserTaxonomy, OccurrenceGeoIndex, TaxonResourceLink, ChotomousKey, TaxonomicStatus, GlossaryTaxonLink, TraitTaxonLink, TaxaEnumTreeEntry, ChecklistTaxonLink, OccurrenceAssociation, ImageAnnotation, TaxonLink, Occurrence, TaxonDescriptionBlock, User, CharacteristicTaxonLink } from '@symbiota2/api-database';

@Exclude()
export class ChecklistTaxonDto {
    constructor(taxon: Taxon) {
        Object.assign(this, taxon);
    }

    //@ApiProperty()
    @Expose()
    id: number;

    //@ApiProperty()
    @Expose()
    kingdomName: string;

    //@ApiProperty()
    @Expose()
    rankID: number | null;

    //@ApiProperty()
    @Expose()
    scientificName: string;

    //@ApiProperty()
    @Expose()
    unitName1: string;

    //@ApiProperty()
    @Expose()
    unitName2: string;

    //@ApiProperty()
    @Expose()
    unitName3: string;

    //@ApiProperty()
    @Expose()
    unitInd1: string;

    //@ApiProperty()
    @Expose()
    unitInd2: string;

    //@ApiProperty()
    @Expose()
    unitInd3: string;

    //@ApiProperty()
    @Expose()
    author: string;

    //@ApiProperty()
    @Expose()
    phyloSortSequence: number | null;

    //@ApiProperty()
    @Expose()
    status: string;

    //@ApiProperty()
    @Expose()
    source: string;

    //@ApiProperty()
    @Expose()
    notes: string;

    //@ApiProperty()
    @Expose()
    hybrid: string;

    //@ApiProperty()
    @Expose()
    securityStatus: number;

    //@ApiProperty()
    @Expose()
    lastModifiedUID: number | null;

    //@ApiProperty()
    @Expose()
    lastModifiedTimestamp: Date | null;

    //@ApiProperty()
    @Expose()
    initialTimestamp: Date;

    //@ApiProperty()
    @Expose()
    dynamicProperties: Record<string, unknown>;

    //@ApiProperty()
    @Expose()
    __unknowns__: Promise<Unknown[]>;

    //@ApiProperty()
    @Expose()
    __taxonMaps__: Promise<TaxonMap[]>;

    //@ApiProperty()
    @Expose()
    __media__: Promise<Medium[]>;

    //@ApiProperty()
    @Expose()
    __occurrenceDeterminations__: Promise<OccurrenceDetermination[]>;

    //@ApiProperty()
    @Expose()
    __referenceTaxonLinks__: Promise<ReferenceTaxonLink[]>;

    //@ApiProperty()
    @Expose()
    __images__: Promise<Image[]>;

    //@ApiProperty()
    @Expose()
    __occurrenceTypes__: Promise<OccurrenceType[]>;

    //@ApiProperty()
    @Expose()
    __nestedTaxonTrees__: Promise<TaxaNestedTreeEntry[]>;

    //@ApiProperty()
    @Expose()
    __vernacularNames__: Promise<TaxonVernacular[]>;

    //@ApiProperty()
    @Expose()
    __dynamicChecklistLinks__: Promise<DynamicChecklistTaxonLink[]>;

    //@ApiProperty()
    @Expose()
    __characteristicDescriptions__: Promise<CharacteristicDescription[]>;

    //@ApiProperty()
    @Expose()
    __glossarySource__: Promise<GlossarySource>;

    //@ApiProperty()
    @Expose()
    __userTaxonomies__: Promise<UserTaxonomy[]>;

    //@ApiProperty()
    @Expose()
    __occurrenceGeoIndices__: Promise<OccurrenceGeoIndex[]>;

    //@ApiProperty()
    @Expose()
    __resourceLinks__: Promise<TaxonResourceLink[]>;

    //@ApiProperty()
    @Expose()
    __chotomousKeys__: Promise<ChotomousKey[]>;

    //@ApiProperty()
    @Expose()
    __childTaxonStatuses__: Promise<TaxonomicStatus[]>;

    //@ApiProperty()
    @Expose()
    __taxonStatuses__: Promise<TaxonomicStatus[]>;

    //@ApiProperty()
    @Expose()
    __acceptedTaxonStatuses__: Promise<TaxonomicStatus[]>;

    //@ApiProperty()
    @Expose()
    __glossaryTaxonLinks__: Promise<GlossaryTaxonLink[]>;

    //@ApiProperty()
    @Expose()
    __traitTaxonLinks__: Promise<TraitTaxonLink[]>;

    //@ApiProperty()
    @Expose()
    taxaEnumEntries: Promise<TaxaEnumTreeEntry[]>;

    //@ApiProperty()
    @Expose()
    __childTaxaEnumEntries__: Promise<TaxaEnumTreeEntry[]>;

    //@ApiProperty()
    @Expose()
    __checklistLinks__: Promise<ChecklistTaxonLink[]>;

    //@ApiProperty()
    @Expose()
    occurrenceAssociations: Promise<OccurrenceAssociation[]>;

    //@ApiProperty()
    @Expose()
    imageAnnotations: Promise<ImageAnnotation[]>;

    //@ApiProperty()
    @Expose()
    taxonLinks: Promise<TaxonLink[]>;

    //@ApiProperty()
    @Expose()
    occurrences: Promise<Occurrence[]>;

    //@ApiProperty()
    @Expose()
    taxonDescriptionBlocks: Promise<TaxonDescriptionBlock[]>;

    //@ApiProperty()
    @Expose()
    lastModifiedUser: Promise<User>;

    //@ApiProperty()
    @Expose()
    characteristicLinks: Promise<CharacteristicTaxonLink[]>;
}
