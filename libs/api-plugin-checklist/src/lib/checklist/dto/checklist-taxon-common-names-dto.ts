
import { Exclude, Expose } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { TaxonVernacular } from '@symbiota2/api-database';

@Exclude()
export class ChecklistTaxonCommonNames {
    constructor(taxon: TaxonVernacular) {
        Object.assign(this, taxon);
    }

    //@ApiProperty()
    @Expose()
    taxonID: number;

    //@ApiProperty()
    @Expose()
    vernacularName: string;

    //@ApiProperty()
    @Expose()
    language: string;

    //@ApiProperty()
    @Expose()
    adminLanguageID: number | null;

    //@ApiProperty()
    @Expose()
    source: string;

    //@ApiProperty()
    @Expose()
    notes: string;

    //@ApiProperty()
    @Expose()
    username: string;

    //@ApiProperty()
    @Expose()
    isUpperTerm: number | null;

    //@ApiProperty()
    @Expose()
    sortSequence: number | null;

    //@ApiProperty()
    @Expose()
    initialTimestamp: Date;
}
