import { ConflictException, Inject, Injectable, InternalServerErrorException, Logger, NotFoundException } from '@nestjs/common';
import {
  Checklist,
  ChecklistChild,
  ChecklistCoordinatePair,
  ChecklistProjectLink,
  ChecklistTaxonComment,
  ChecklistTaxonLink,
  ChecklistTaxonStatus,
  DynamicChecklist,
  DynamicChecklistTaxonLink,
  Project,
  TaxaEnumTreeEntry,
  Taxon,
} from '@symbiota2/api-database';

import { In, Like, Repository, Raw } from 'typeorm';
import { BaseService, csvIterator } from '@symbiota2/api-common';
import { ProjectDto } from './dto/project-dto';
import { ChecklistDto } from './dto/checklist-dto';
import e from 'express';
import { ChecklistTaxonLinkDto } from './dto/checklist-taxon-link.dto';
import { isObject } from 'node:util';
import { TaxonomicEnumTreeFindAllParams } from './dto/taxonomicEnumTree-find-all.input.dto';
import { ProjectFindAllParams, TaxonFindAllParams } from './dto/project-find-params';
// import { PageOptionsDto } from './dto/page-option.dto';
// import { PageDto } from './dto/page-dto';
// import { PageMetaDto } from './dto/page-meta.dto';

@Injectable()
export class ChecklistService extends BaseService<Project> {
  private static readonly S3_PREFIX = 'checklist';
  private static readonly LOGGER = new Logger(ChecklistService.name);

  constructor(
    @Inject(Project.PROVIDER_ID)
    private readonly projectRepo: Repository<Project>,
    @Inject(Checklist.PROVIDER_ID)
    private readonly checklistRepo: Repository<Checklist>,
    @Inject(ChecklistProjectLink.PROVIDER_ID)
    private readonly projectLinkRepo: Repository<ChecklistProjectLink>,
    @Inject(Taxon.PROVIDER_ID)
    private readonly taxonRepo: Repository<Taxon>,
    @Inject(ChecklistTaxonLink.PROVIDER_ID)
    private readonly checklistTaxonLinkRepo: Repository<ChecklistTaxonLink>,
    @Inject(TaxaEnumTreeEntry.PROVIDER_ID)
        private readonly enumTreeRepository: Repository<TaxaEnumTreeEntry>,
  ) {
      super(projectRepo)
  }

  public static s3Key(objectName: string): string {
    return [ChecklistService.S3_PREFIX, objectName].join('/');
  }

    async findAllProjects(params?: ProjectFindAllParams): Promise<Project[]> {

        const qb = this.projectRepo.createQueryBuilder('p')

        return qb.getMany()
    }

    async findAllChecklists(): Promise<Checklist[]> {
        return this.checklistRepo.find();
    }

    async findProjectchecklists(pid: number): Promise<ChecklistProjectLink[]> {
        const qb = this.projectLinkRepo.createQueryBuilder('link')
            .leftJoinAndSelect('link.checklist', 'checklist')
            .where('link.projectID = :projectID', { projectID: pid })
        return qb.getMany()
    }



    /**
     * Service to find all of the taxa possibly using an array of ids, a scientific name,
     * and/or a taxonomic authority ID
     * Can also limit the number fetched and use an offset.
     * Set find params using the 'TaxonFindAllParams'
     * @param params - the 'TaxonFindAllParams'
     * @returns Observable of response from api casted as `Taxon[]`
     * will be the found statements
     * @returns `of(null)` if api errors
     * @see Taxon
     * @see TaxonFindAllParams
     */
     async findAllTaxa(params?: TaxonFindAllParams): Promise<Taxon[]> {
        const { limit, offset, ...qParams } = params

        const qb = this.taxonRepo.createQueryBuilder('o')
            .leftJoinAndSelect('o.vernacularNames', 'vernacularNames')
            .where("true")

        /*  Since we are looking for names, we don't care about the authority
        if (qParams.taxonAuthorityID) {
            // Have to use query builder since filter on nested relations does not work
            qb.innerJoin('o.taxonStatuses', 'c')
                .where('c.taxonAuthorityID = :authorityID', { authorityID: params.taxonAuthorityID })

        }
         */

        if (qParams.id) {
            qb.andWhere('o.id IN (:...taxonIDs)', {taxonIDs: params.id})
        }

        // Don't limit if it has filters
        if (limit && !(qParams.id || qParams.scientificName)) {
            qb.take(limit)
        }

        if (offset && !(qParams.id || qParams.scientificName)) {
            qb.skip(offset)
        }

        if (qParams.scientificName) {
            qb.andWhere('o.scientificName = :sciname', {sciname: params.scientificName})
        }

        return qb.getMany()
    }





    async findProjectById(id: number): Promise<Project> {
        if (!id) {
            return null;
        }
        return this.projectRepo.findOne({where: {id: id}})
    }

    async findChecklistById(id: number): Promise<Checklist> {
        if (!id) {
            return null;
        }
        //return this.checklistRepo.findOne({where: {id: id}})
        //const qb = this.checklistRepo.createQueryBuilder('c')
        // .leftJoinAndSelect('c.taxaLinks', 'taxa')
        // .where('c.id = :id', {id: id})
        return this.checklistRepo.findOne({where: {id: id}})
        //return qb;
    }

    // async findTaxaByChecklist(cid: number, pageOptionsDto: PageOptionsDto) {
    async findTaxaByChecklist(cid: number) {
        if (!cid) {
            return null;
        }
        const queryBuilder = this.checklistTaxonLinkRepo.createQueryBuilder('link')
            .leftJoinAndSelect('link.taxon', 'taxon')
            // .leftJoinAndSelect('taxon.taxaEnumEntries', 'taxaEnumEntries')
            .leftJoinAndSelect('taxon.acceptedTaxonStatuses', 'acceptedTaxonStatuses')
            .leftJoinAndSelect('taxon.images', 'images')
            .leftJoinAndSelect('taxon.vernacularNames', 'vernacularNames')
            .where({checklistID: cid})

            // if (pageOptionsDto.order) {
            //     queryBuilder.orderBy("taxon.scientificName", pageOptionsDto.order)
            // }

        const itemCount = await queryBuilder.getCount();
        const entities = await queryBuilder.getMany();
        // const pageMetaDto = new PageMetaDto({ itemCount, pageOptionsDto });

        return entities;

    }














    // /**
    //  * Retrieve the ancestor records for a given taxon and taxon authority id
    //  * (which is optional, in which case for any taxon authority are returned)
    //  * Fetches the entire record
    //  * Set authority id as a param using the 'TaxonomicEnumTreeFindAllParams'
    //  * @param taxonID - the id of the taxon
    //  * @param params - the 'TaxonomicEnumTreeFindAllParams'
    //  * @returns Observable of response from api casted as `TaxaEnumTreeEntry[]`
    //  * will be the found enum tree entries
    //  * @returns `of(null)` if api errors or not found
    //  * @see TaxaEnumTreeEntry
    //  * @see TaxonomicEnumTreeFindAllParams
    //  */
    //  async findAncestors(taxonID: string, params?: TaxonomicEnumTreeFindAllParams): Promise<TaxaEnumTreeEntry[]> {
    //     const { ...qParams } = params

    //     // Fetch me, the underlying table stores a row for each tid/self->parenttid/ancestor relationship
    //     return (qParams.taxonAuthorityID) ?
    //         await this.enumTreeRepository.find({
    //             relations: [
    //                 "parentTaxon",
    //                 "parentTaxon.acceptedTaxonStatuses",
    //                 "parentTaxon.acceptedTaxonStatuses.taxon"],
    //             where: {taxonAuthorityID: params.taxonAuthorityID, taxonID: taxonID}})
    //         : this.enumTreeRepository.find({
    //             relations: [
    //                 "parentTaxon",
    //                 "parentTaxon.acceptedTaxonStatuses",
    //                 "parentTaxon.acceptedTaxonStatuses.taxon"],
    //             where: {taxonID: taxonID}})
    // }







    /**
     * Create a checklist record using a Partial Checklist record
     * @param data The data for the record to create
     * @return number The created data or null (not found)
     */
    async createChecklist(data: Partial<Checklist>): Promise<Checklist> {
        const checklist = this.checklistRepo.create(data);
        return this.checklistRepo.save(checklist)
    }

    /**
     * update a checklist record using a Partial Checklist record
     * @param id The id for the record to update
     * @return The created data or null (id not found)
     */
     async updateChecklist(id: number, data: Partial<Checklist>): Promise<Checklist> {
        const updateResult = await this.checklistRepo.update({ id }, data);
        if (updateResult.affected > 0) {
            return this.findChecklistById(id)
        }
        return null
    }

    /**
     * delete a checklist record using a id record
     * @param id The id for the record to delete
     * @return null (id not found)
     */
    async deleteChecklist(id: number): Promise<void> {
        this.checklistRepo.delete({id: id})
    }

    async uploadChecklistTaxon(clid: number, data: Partial<ChecklistTaxonLinkDto>): Promise<ChecklistTaxonLink> {
        const checklist = await this.checklistRepo.findOne({where: {id: clid}})
        if (!checklist) throw new NotFoundException('Invalid checklist')

        // if (typeof data === 'object' &&
        // !Array.isArray(data) &&
        // data !== null) {
            const taxon = await this.taxonRepo.findOne({where: {scientificName: data.scientificName}})

            if (!taxon) throw new NotFoundException('Name not found. It needs to be added to the database. Contact the portal manager [embed email link]')


                try {
                     return await this.checklistTaxonLinkRepo.save({
                        taxonID: taxon.id,
                        checklistID: clid,
                        familyOverride: data.familyOverride,
                        habitat: data.habitat,
                        abundance: data.abundance,
                        notes: data.notes,
                        internalNotes: data.internalNotes,
                        source: data.source
                    })
                } catch(error) {
                    if (error.code === 'ER_DUP_ENTRY') {
                        throw new ConflictException('Name already in the list.');
                    } else {
                        throw new InternalServerErrorException('Something went wrong! Please try again later.')
                    }
                }
            }
        }
