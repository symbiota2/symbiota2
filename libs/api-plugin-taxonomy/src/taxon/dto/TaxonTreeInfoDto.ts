import { Exclude, Expose } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

@Exclude()
export class TaxonTreeInfoDto {
    constructor(id: number, name: string, author: string, rankID: number, isSynonym: boolean, parentID: number, acceptedID: number) {
        this.id = id
        this.name = name
        this.author = author
        this.isSynonym = isSynonym
        this.parentID = parentID
        this.acceptedID = acceptedID
        this.rankID = rankID
    }

    //@ApiProperty()
    @Expose()
    id: number

    //@ApiProperty()
    @Expose()
    name: string

    //@ApiProperty()
    @Expose()
    author: string | null

    //@ApiProperty()
    @Expose()
    isSynonym: boolean

    //@ApiProperty()
    @Expose()
    rankID: number

    //@ApiProperty()
    @Expose()
    parentID: number | null

    //@ApiProperty()
    @Expose()
    acceptedID: number | null
}
