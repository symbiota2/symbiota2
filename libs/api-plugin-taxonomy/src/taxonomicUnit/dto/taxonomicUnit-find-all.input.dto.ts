import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsArray, IsInt, Max, Min } from 'class-validator';
import { Type } from 'class-transformer';
import { BaseFindAllParams } from '@symbiota2/api-common';

export class TaxonomicUnitFindAllParams extends BaseFindAllParams {
    static readonly TAXON_FIND_ALL_DEFAULT_LIMIT = 1000;
    static readonly TAXON_FIND_ALL_DEFAULT_OFFSET = 0;
    static readonly TAXON_FIND_ALL_MAX_LIMIT = 1000;

    @ApiProperty({ name: 'id[]', type: [Number], required: false })
    @Type(() => Number)
    @IsArray()
    @IsInt({ each: true })
    @IsOptional()
    id: number[];

    @Min(0)
    @Max(TaxonomicUnitFindAllParams.TAXON_FIND_ALL_MAX_LIMIT)
    limit: number = TaxonomicUnitFindAllParams.TAXON_FIND_ALL_DEFAULT_LIMIT;

    @ApiProperty({ type: 'number', required: false, default: TaxonomicUnitFindAllParams.TAXON_FIND_ALL_DEFAULT_OFFSET })
    @Min(0)
    offset: number //= TaxonomicUnitFindAllParams.TAXON_FIND_ALL_DEFAULT_OFFSET;
}
