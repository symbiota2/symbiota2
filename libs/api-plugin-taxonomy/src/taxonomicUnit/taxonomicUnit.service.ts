import { Inject, Injectable } from '@nestjs/common';
import { In, InsertResult, LessThan, Repository } from 'typeorm';
import { TaxonomicUnitFindAllParams } from './dto/taxonomicUnit-find-all.input.dto';
import { BaseService } from '@symbiota2/api-common';
import { TaxaEnumTreeEntry, TaxonomicUnit } from '@symbiota2/api-database';

@Injectable()
export class TaxonomicUnitService extends BaseService<TaxonomicUnit>{
    constructor(
        @Inject(TaxonomicUnit.PROVIDER_ID)
        private readonly taxonRanks: Repository<TaxonomicUnit>) {

        super(taxonRanks);
    }

    /*
    Fetch all of the taxonomic units.
    Can limit the list by a list of Unit IDs.
    Can also limit the number fetched and use an offset.
     */
    async findAll(params?: TaxonomicUnitFindAllParams): Promise<TaxonomicUnit[]> {
        const { limit, offset, ...qParams } = params;

        //console.log(" id is " + qParams.id + " " + limit + " " + offset)
        return await (qParams.id)?
            this.taxonRanks.find({
                take: limit,  // Set default limit high since table is small
                skip: offset,
                where: { id: In(params.id) }
            })
            : this.taxonRanks.find({take: limit, skip: offset})
    }

    /*
    Fetch all of the taxonomic units.
    Can limit the list by a list of Unit IDs.
    Can also limit the number fetched and use an offset.
    */
    async findKingdomNames(): Promise<TaxonomicUnit[]> {

        const names = await this.taxonRanks.createQueryBuilder()
            .select('kingdomName')
            .distinct(true)
            .getRawMany()

        return names
    }

    /**
     * Insert taxonomic unit records
     * @param data The data for the record to create, a list of taxonomic unit records
     * @return number The created data or null (not found)
     */
    async saveCheck(data: Partial<TaxonomicUnit>[]): Promise<InsertResult> {
        return await this.taxonRanks.upsert(data,[])
    }


    async create(data: Partial<TaxonomicUnit>): Promise<TaxonomicUnit> {
        const taxon = this.taxonRanks.create(data);
        return this.taxonRanks.save(taxon);
    }

    async updateByID(id: number, data: Partial<TaxonomicUnit>): Promise<TaxonomicUnit> {
        const updateResult = await this.taxonRanks.update({ id }, data);
        if (updateResult.affected > 0) {
            return this.findByID({where: {id: id}});
        }
        return null;
    }

    async getAncestorRanks(rank: TaxonomicUnit): Promise<TaxonomicUnit[]> {
        return this.taxonRanks.find({where: {
            kingdomName: rank.kingdomName,
            rankID: LessThan(rank.rankID)
        }});
    }

    async getMostSpecificRank(ranks: TaxonomicUnit[]): Promise<TaxonomicUnit> {
        return ranks.reduce((previous, current) => {
            return previous.rankID > current.rankID ? previous : current;
        }, ranks[0]);
    }

    async getDirectParentRank(rank: TaxonomicUnit): Promise<TaxonomicUnit> {
        const allAncestors = await this.getAncestorRanks(rank);
        return await this.getMostSpecificRank(allAncestors);
    }

    /* Insert the initial taxonomic unit records
     */

    initialLoad() {

        const ranks : Partial<TaxonomicUnit>[] =
                    [
                        {
                            "kingdomName": "Animalia",
                            "rankID": 1,
                            "rankName": "Organism",
                            "suffix": null,
                            "directParentRankID": 1,
                            "reqParentRankID": 1
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 10,
                            "rankName": "Kingdom",
                            "suffix": null,
                            "directParentRankID": 1,
                            "reqParentRankID": 1
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 20,
                            "rankName": "Subkingdom",
                            "suffix": null,
                            "directParentRankID": 10,
                            "reqParentRankID": 10
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 22,
                            "rankName": "Infrakingdom",
                            "suffix": null,
                            "directParentRankID": 10,
                            "reqParentRankID": 10
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 25,
                            "rankName": "Superphylum",
                            "suffix": null,
                            "directParentRankID": 10,
                            "reqParentRankID": 10
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 30,
                            "rankName": "Phylum",
                            "suffix": null,
                            "directParentRankID": 20,
                            "reqParentRankID": 10
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 32,
                            "rankName": "Infraphylum",
                            "suffix": null,
                            "directParentRankID": 20,
                            "reqParentRankID": 20
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 40,
                            "rankName": "Subphylum",
                            "suffix": null,
                            "directParentRankID": 30,
                            "reqParentRankID": 30
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 50,
                            "rankName": "Superclass",
                            "suffix": null,
                            "directParentRankID": 40,
                            "reqParentRankID": 30
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 60,
                            "rankName": "Class",
                            "suffix": null,
                            "directParentRankID": 50,
                            "reqParentRankID": 30
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 70,
                            "rankName": "Subclass",
                            "suffix": null,
                            "directParentRankID": 60,
                            "reqParentRankID": 60
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 75,
                            "rankName": "Infraclass",
                            "suffix": null,
                            "directParentRankID": 70,
                            "reqParentRankID": 70
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 80,
                            "rankName": "Superorder",
                            "suffix": null,
                            "directParentRankID": 70,
                            "reqParentRankID": 60
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 100,
                            "rankName": "Order",
                            "suffix": null,
                            "directParentRankID": 70,
                            "reqParentRankID": 60
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 110,
                            "rankName": "Suborder",
                            "suffix": null,
                            "directParentRankID": 100,
                            "reqParentRankID": 100
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 120,
                            "rankName": "Infraorder",
                            "suffix": null,
                            "directParentRankID": 110,
                            "reqParentRankID": 100
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 123,
                            "rankName": "Section",
                            "suffix": null,
                            "directParentRankID": 120,
                            "reqParentRankID": 100
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 125,
                            "rankName": "Parvorder",
                            "suffix": null,
                            "directParentRankID": 120,
                            "reqParentRankID": 100
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 130,
                            "rankName": "Nanorder",
                            "suffix": null,
                            "directParentRankID": 125,
                            "reqParentRankID": 100
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 135,
                            "rankName": "Superfamily",
                            "suffix": null,
                            "directParentRankID": 125,
                            "reqParentRankID": 100
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 140,
                            "rankName": "Family",
                            "suffix": null,
                            "directParentRankID": 110,
                            "reqParentRankID": 100
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 150,
                            "rankName": "Subfamily",
                            "suffix": null,
                            "directParentRankID": 140,
                            "reqParentRankID": 140
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 160,
                            "rankName": "Tribe",
                            "suffix": null,
                            "directParentRankID": 150,
                            "reqParentRankID": 140
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 170,
                            "rankName": "Subtribe",
                            "suffix": null,
                            "directParentRankID": 160,
                            "reqParentRankID": 140
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 180,
                            "rankName": "Genus",
                            "suffix": null,
                            "directParentRankID": 170,
                            "reqParentRankID": 140
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 190,
                            "rankName": "Subgenus",
                            "suffix": null,
                            "directParentRankID": 180,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 220,
                            "rankName": "Species",
                            "suffix": null,
                            "directParentRankID": 210,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 222,
                            "rankName": "Form",
                            "suffix": null,
                            "directParentRankID": 210,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 230,
                            "rankName": "Subspecies",
                            "suffix": null,
                            "directParentRankID": 220,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 240,
                            "rankName": "Morph",
                            "suffix": null,
                            "directParentRankID": 220,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Animalia",
                            "rankID": 245,
                            "rankName": "Variety",
                            "suffix": null,
                            "directParentRankID": 230,
                            "reqParentRankID": 220
                        },
                        {
                            "kingdomName": "Fungi",
                            "rankID": 1,
                            "rankName": "Organism",
                            "suffix": null,
                            "directParentRankID": 1,
                            "reqParentRankID": 1
                        },
                        {
                            "kingdomName": "Fungi",
                            "rankID": 10,
                            "rankName": "Kingdom",
                            "suffix": null,
                            "directParentRankID": 1,
                            "reqParentRankID": 1
                        },
                        {
                            "kingdomName": "Fungi",
                            "rankID": 20,
                            "rankName": "Subkingdom",
                            "suffix": null,
                            "directParentRankID": 10,
                            "reqParentRankID": 10
                        },
                        {
                            "kingdomName": "Fungi",
                            "rankID": 30,
                            "rankName": "Division",
                            "suffix": null,
                            "directParentRankID": 20,
                            "reqParentRankID": 10
                        },
                        {
                            "kingdomName": "Fungi",
                            "rankID": 40,
                            "rankName": "Subdivision",
                            "suffix": null,
                            "directParentRankID": 30,
                            "reqParentRankID": 30
                        },
                        {
                            "kingdomName": "Fungi",
                            "rankID": 50,
                            "rankName": "Superclass",
                            "suffix": null,
                            "directParentRankID": 40,
                            "reqParentRankID": 30
                        },
                        {
                            "kingdomName": "Fungi",
                            "rankID": 60,
                            "rankName": "Class",
                            "suffix": null,
                            "directParentRankID": 50,
                            "reqParentRankID": 30
                        },
                        {
                            "kingdomName": "Fungi",
                            "rankID": 70,
                            "rankName": "Subclass",
                            "suffix": null,
                            "directParentRankID": 60,
                            "reqParentRankID": 60
                        },
                        {
                            "kingdomName": "Fungi",
                            "rankID": 100,
                            "rankName": "Order",
                            "suffix": null,
                            "directParentRankID": 70,
                            "reqParentRankID": 60
                        },
                        {
                            "kingdomName": "Fungi",
                            "rankID": 110,
                            "rankName": "Suborder",
                            "suffix": null,
                            "directParentRankID": 100,
                            "reqParentRankID": 100
                        },
                        {
                            "kingdomName": "Fungi",
                            "rankID": 140,
                            "rankName": "Family",
                            "suffix": null,
                            "directParentRankID": 110,
                            "reqParentRankID": 100
                        },
                        {
                            "kingdomName": "Fungi",
                            "rankID": 150,
                            "rankName": "Subfamily",
                            "suffix": null,
                            "directParentRankID": 140,
                            "reqParentRankID": 140
                        },
                        {
                            "kingdomName": "Fungi",
                            "rankID": 160,
                            "rankName": "Tribe",
                            "suffix": null,
                            "directParentRankID": 150,
                            "reqParentRankID": 140
                        },
                        {
                            "kingdomName": "Fungi",
                            "rankID": 170,
                            "rankName": "Subtribe",
                            "suffix": null,
                            "directParentRankID": 160,
                            "reqParentRankID": 140
                        },
                        {
                            "kingdomName": "Fungi",
                            "rankID": 180,
                            "rankName": "Genus",
                            "suffix": null,
                            "directParentRankID": 170,
                            "reqParentRankID": 140
                        },
                        {
                            "kingdomName": "Fungi",
                            "rankID": 190,
                            "rankName": "Subgenus",
                            "suffix": null,
                            "directParentRankID": 180,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Fungi",
                            "rankID": 200,
                            "rankName": "Section",
                            "suffix": null,
                            "directParentRankID": 190,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Fungi",
                            "rankID": 210,
                            "rankName": "Subsection",
                            "suffix": null,
                            "directParentRankID": 200,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Fungi",
                            "rankID": 220,
                            "rankName": "Species",
                            "suffix": null,
                            "directParentRankID": 210,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Fungi",
                            "rankID": 230,
                            "rankName": "Subspecies",
                            "suffix": null,
                            "directParentRankID": 220,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Fungi",
                            "rankID": 240,
                            "rankName": "Variety",
                            "suffix": null,
                            "directParentRankID": 220,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Fungi",
                            "rankID": 250,
                            "rankName": "Subvariety",
                            "suffix": null,
                            "directParentRankID": 240,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Fungi",
                            "rankID": 260,
                            "rankName": "Form",
                            "suffix": null,
                            "directParentRankID": 220,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Fungi",
                            "rankID": 270,
                            "rankName": "Subform",
                            "suffix": null,
                            "directParentRankID": 260,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Fungi",
                            "rankID": 300,
                            "rankName": "Cultivated",
                            "suffix": null,
                            "directParentRankID": 220,
                            "reqParentRankID": 220
                        },
                        {
                            "kingdomName": "Monera",
                            "rankID": 1,
                            "rankName": "Organism",
                            "suffix": null,
                            "directParentRankID": 1,
                            "reqParentRankID": 1
                        },
                        {
                            "kingdomName": "Monera",
                            "rankID": 10,
                            "rankName": "Kingdom",
                            "suffix": null,
                            "directParentRankID": 1,
                            "reqParentRankID": 1
                        },
                        {
                            "kingdomName": "Monera",
                            "rankID": 20,
                            "rankName": "Subkingdom",
                            "suffix": null,
                            "directParentRankID": 10,
                            "reqParentRankID": 10
                        },
                        {
                            "kingdomName": "Monera",
                            "rankID": 30,
                            "rankName": "Phylum",
                            "suffix": null,
                            "directParentRankID": 20,
                            "reqParentRankID": 10
                        },
                        {
                            "kingdomName": "Monera",
                            "rankID": 40,
                            "rankName": "Subphylum",
                            "suffix": null,
                            "directParentRankID": 30,
                            "reqParentRankID": 30
                        },
                        {
                            "kingdomName": "Monera",
                            "rankID": 60,
                            "rankName": "Class",
                            "suffix": null,
                            "directParentRankID": 50,
                            "reqParentRankID": 30
                        },
                        {
                            "kingdomName": "Monera",
                            "rankID": 70,
                            "rankName": "Subclass",
                            "suffix": null,
                            "directParentRankID": 60,
                            "reqParentRankID": 60
                        },
                        {
                            "kingdomName": "Monera",
                            "rankID": 100,
                            "rankName": "Order",
                            "suffix": null,
                            "directParentRankID": 70,
                            "reqParentRankID": 60
                        },
                        {
                            "kingdomName": "Monera",
                            "rankID": 110,
                            "rankName": "Suborder",
                            "suffix": null,
                            "directParentRankID": 100,
                            "reqParentRankID": 100
                        },
                        {
                            "kingdomName": "Monera",
                            "rankID": 140,
                            "rankName": "Family",
                            "suffix": null,
                            "directParentRankID": 110,
                            "reqParentRankID": 100
                        },
                        {
                            "kingdomName": "Monera",
                            "rankID": 150,
                            "rankName": "Subfamily",
                            "suffix": null,
                            "directParentRankID": 140,
                            "reqParentRankID": 140
                        },
                        {
                            "kingdomName": "Monera",
                            "rankID": 160,
                            "rankName": "Tribe",
                            "suffix": null,
                            "directParentRankID": 150,
                            "reqParentRankID": 140
                        },
                        {
                            "kingdomName": "Monera",
                            "rankID": 170,
                            "rankName": "Subtribe",
                            "suffix": null,
                            "directParentRankID": 160,
                            "reqParentRankID": 140
                        },
                        {
                            "kingdomName": "Monera",
                            "rankID": 180,
                            "rankName": "Genus",
                            "suffix": null,
                            "directParentRankID": 170,
                            "reqParentRankID": 140
                        },
                        {
                            "kingdomName": "Monera",
                            "rankID": 190,
                            "rankName": "Subgenus",
                            "suffix": null,
                            "directParentRankID": 180,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Monera",
                            "rankID": 220,
                            "rankName": "Species",
                            "suffix": null,
                            "directParentRankID": 210,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Monera",
                            "rankID": 230,
                            "rankName": "Subspecies",
                            "suffix": null,
                            "directParentRankID": 220,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Monera",
                            "rankID": 240,
                            "rankName": "Morph",
                            "suffix": null,
                            "directParentRankID": 220,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Organism",
                            "rankID": 1,
                            "rankName": "Organism",
                            "suffix": null,
                            "directParentRankID": 1,
                            "reqParentRankID": 1
                        },
                        {
                            "kingdomName": "Organism",
                            "rankID": 10,
                            "rankName": "Kingdom",
                            "suffix": null,
                            "directParentRankID": 1,
                            "reqParentRankID": 1
                        },
                        {
                            "kingdomName": "Organism",
                            "rankID": 20,
                            "rankName": "Subkingdom",
                            "suffix": null,
                            "directParentRankID": 10,
                            "reqParentRankID": 10
                        },
                        {
                            "kingdomName": "Organism",
                            "rankID": 30,
                            "rankName": "Division",
                            "suffix": null,
                            "directParentRankID": 20,
                            "reqParentRankID": 10
                        },
                        {
                            "kingdomName": "Organism",
                            "rankID": 40,
                            "rankName": "Subdivision",
                            "suffix": null,
                            "directParentRankID": 30,
                            "reqParentRankID": 30
                        },
                        {
                            "kingdomName": "Organism",
                            "rankID": 50,
                            "rankName": "Superclass",
                            "suffix": null,
                            "directParentRankID": 40,
                            "reqParentRankID": 30
                        },
                        {
                            "kingdomName": "Organism",
                            "rankID": 60,
                            "rankName": "Class",
                            "suffix": null,
                            "directParentRankID": 50,
                            "reqParentRankID": 30
                        },
                        {
                            "kingdomName": "Organism",
                            "rankID": 70,
                            "rankName": "Subclass",
                            "suffix": null,
                            "directParentRankID": 60,
                            "reqParentRankID": 60
                        },
                        {
                            "kingdomName": "Organism",
                            "rankID": 100,
                            "rankName": "Order",
                            "suffix": null,
                            "directParentRankID": 70,
                            "reqParentRankID": 60
                        },
                        {
                            "kingdomName": "Organism",
                            "rankID": 110,
                            "rankName": "Suborder",
                            "suffix": null,
                            "directParentRankID": 100,
                            "reqParentRankID": 100
                        },
                        {
                            "kingdomName": "Organism",
                            "rankID": 140,
                            "rankName": "Family",
                            "suffix": null,
                            "directParentRankID": 110,
                            "reqParentRankID": 100
                        },
                        {
                            "kingdomName": "Organism",
                            "rankID": 150,
                            "rankName": "Subfamily",
                            "suffix": null,
                            "directParentRankID": 140,
                            "reqParentRankID": 140
                        },
                        {
                            "kingdomName": "Organism",
                            "rankID": 160,
                            "rankName": "Tribe",
                            "suffix": null,
                            "directParentRankID": 150,
                            "reqParentRankID": 140
                        },
                        {
                            "kingdomName": "Organism",
                            "rankID": 170,
                            "rankName": "Subtribe",
                            "suffix": null,
                            "directParentRankID": 160,
                            "reqParentRankID": 140
                        },
                        {
                            "kingdomName": "Organism",
                            "rankID": 180,
                            "rankName": "Genus",
                            "suffix": null,
                            "directParentRankID": 170,
                            "reqParentRankID": 140
                        },
                        {
                            "kingdomName": "Organism",
                            "rankID": 190,
                            "rankName": "Subgenus",
                            "suffix": null,
                            "directParentRankID": 180,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Organism",
                            "rankID": 200,
                            "rankName": "Section",
                            "suffix": null,
                            "directParentRankID": 190,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Organism",
                            "rankID": 210,
                            "rankName": "Subsection",
                            "suffix": null,
                            "directParentRankID": 200,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Organism",
                            "rankID": 220,
                            "rankName": "Species",
                            "suffix": null,
                            "directParentRankID": 210,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Organism",
                            "rankID": 230,
                            "rankName": "Subspecies",
                            "suffix": null,
                            "directParentRankID": 220,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Organism",
                            "rankID": 240,
                            "rankName": "Variety",
                            "suffix": null,
                            "directParentRankID": 220,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Organism",
                            "rankID": 250,
                            "rankName": "Subvariety",
                            "suffix": null,
                            "directParentRankID": 240,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Organism",
                            "rankID": 260,
                            "rankName": "Form",
                            "suffix": null,
                            "directParentRankID": 220,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Organism",
                            "rankID": 270,
                            "rankName": "Subform",
                            "suffix": null,
                            "directParentRankID": 260,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Organism",
                            "rankID": 300,
                            "rankName": "Cultivated",
                            "suffix": null,
                            "directParentRankID": 220,
                            "reqParentRankID": 220
                        },
                        {
                            "kingdomName": "Plantae",
                            "rankID": 1,
                            "rankName": "Organism",
                            "suffix": null,
                            "directParentRankID": 1,
                            "reqParentRankID": 1
                        },
                        {
                            "kingdomName": "Plantae",
                            "rankID": 10,
                            "rankName": "Kingdom",
                            "suffix": null,
                            "directParentRankID": 1,
                            "reqParentRankID": 1
                        },
                        {
                            "kingdomName": "Plantae",
                            "rankID": 20,
                            "rankName": "Subkingdom",
                            "suffix": null,
                            "directParentRankID": 10,
                            "reqParentRankID": 10
                        },
                        {
                            "kingdomName": "Plantae",
                            "rankID": 30,
                            "rankName": "Division",
                            "suffix": null,
                            "directParentRankID": 20,
                            "reqParentRankID": 10
                        },
                        {
                            "kingdomName": "Plantae",
                            "rankID": 40,
                            "rankName": "Subdivision",
                            "suffix": null,
                            "directParentRankID": 30,
                            "reqParentRankID": 30
                        },
                        {
                            "kingdomName": "Plantae",
                            "rankID": 50,
                            "rankName": "Superclass",
                            "suffix": null,
                            "directParentRankID": 40,
                            "reqParentRankID": 30
                        },
                        {
                            "kingdomName": "Plantae",
                            "rankID": 60,
                            "rankName": "Class",
                            "suffix": null,
                            "directParentRankID": 50,
                            "reqParentRankID": 30
                        },
                        {
                            "kingdomName": "Plantae",
                            "rankID": 70,
                            "rankName": "Subclass",
                            "suffix": null,
                            "directParentRankID": 60,
                            "reqParentRankID": 60
                        },
                        {
                            "kingdomName": "Plantae",
                            "rankID": 100,
                            "rankName": "Order",
                            "suffix": null,
                            "directParentRankID": 70,
                            "reqParentRankID": 60
                        },
                        {
                            "kingdomName": "Plantae",
                            "rankID": 110,
                            "rankName": "Suborder",
                            "suffix": null,
                            "directParentRankID": 100,
                            "reqParentRankID": 100
                        },
                        {
                            "kingdomName": "Plantae",
                            "rankID": 140,
                            "rankName": "Family",
                            "suffix": null,
                            "directParentRankID": 110,
                            "reqParentRankID": 100
                        },
                        {
                            "kingdomName": "Plantae",
                            "rankID": 150,
                            "rankName": "Subfamily",
                            "suffix": null,
                            "directParentRankID": 140,
                            "reqParentRankID": 140
                        },
                        {
                            "kingdomName": "Plantae",
                            "rankID": 160,
                            "rankName": "Tribe",
                            "suffix": null,
                            "directParentRankID": 150,
                            "reqParentRankID": 140
                        },
                        {
                            "kingdomName": "Plantae",
                            "rankID": 170,
                            "rankName": "Subtribe",
                            "suffix": null,
                            "directParentRankID": 160,
                            "reqParentRankID": 140
                        },
                        {
                            "kingdomName": "Plantae",
                            "rankID": 180,
                            "rankName": "Genus",
                            "suffix": null,
                            "directParentRankID": 170,
                            "reqParentRankID": 140
                        },
                        {
                            "kingdomName": "Plantae",
                            "rankID": 190,
                            "rankName": "Subgenus",
                            "suffix": null,
                            "directParentRankID": 180,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Plantae",
                            "rankID": 200,
                            "rankName": "Section",
                            "suffix": null,
                            "directParentRankID": 190,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Plantae",
                            "rankID": 210,
                            "rankName": "Subsection",
                            "suffix": null,
                            "directParentRankID": 200,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Plantae",
                            "rankID": 220,
                            "rankName": "Species",
                            "suffix": null,
                            "directParentRankID": 210,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Plantae",
                            "rankID": 230,
                            "rankName": "Subspecies",
                            "suffix": null,
                            "directParentRankID": 220,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Plantae",
                            "rankID": 240,
                            "rankName": "Variety",
                            "suffix": null,
                            "directParentRankID": 220,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Plantae",
                            "rankID": 250,
                            "rankName": "Subvariety",
                            "suffix": null,
                            "directParentRankID": 240,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Plantae",
                            "rankID": 260,
                            "rankName": "Form",
                            "suffix": null,
                            "directParentRankID": 220,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Plantae",
                            "rankID": 270,
                            "rankName": "Subform",
                            "suffix": null,
                            "directParentRankID": 260,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Plantae",
                            "rankID": 300,
                            "rankName": "Cultivated",
                            "suffix": null,
                            "directParentRankID": 220,
                            "reqParentRankID": 220
                        },
                        {
                            "kingdomName": "Protista",
                            "rankID": 1,
                            "rankName": "Organism",
                            "suffix": null,
                            "directParentRankID": 1,
                            "reqParentRankID": 1
                        },
                        {
                            "kingdomName": "Protista",
                            "rankID": 10,
                            "rankName": "Kingdom",
                            "suffix": null,
                            "directParentRankID": 1,
                            "reqParentRankID": 1
                        },
                        {
                            "kingdomName": "Protista",
                            "rankID": 20,
                            "rankName": "Subkingdom",
                            "suffix": null,
                            "directParentRankID": 10,
                            "reqParentRankID": 10
                        },
                        {
                            "kingdomName": "Protista",
                            "rankID": 30,
                            "rankName": "Phylum",
                            "suffix": null,
                            "directParentRankID": 20,
                            "reqParentRankID": 10
                        },
                        {
                            "kingdomName": "Protista",
                            "rankID": 40,
                            "rankName": "Subphylum",
                            "suffix": null,
                            "directParentRankID": 30,
                            "reqParentRankID": 30
                        },
                        {
                            "kingdomName": "Protista",
                            "rankID": 60,
                            "rankName": "Class",
                            "suffix": null,
                            "directParentRankID": 50,
                            "reqParentRankID": 30
                        },
                        {
                            "kingdomName": "Protista",
                            "rankID": 70,
                            "rankName": "Subclass",
                            "suffix": null,
                            "directParentRankID": 60,
                            "reqParentRankID": 60
                        },
                        {
                            "kingdomName": "Protista",
                            "rankID": 100,
                            "rankName": "Order",
                            "suffix": null,
                            "directParentRankID": 70,
                            "reqParentRankID": 60
                        },
                        {
                            "kingdomName": "Protista",
                            "rankID": 110,
                            "rankName": "Suborder",
                            "suffix": null,
                            "directParentRankID": 100,
                            "reqParentRankID": 100
                        },
                        {
                            "kingdomName": "Protista",
                            "rankID": 140,
                            "rankName": "Family",
                            "suffix": null,
                            "directParentRankID": 110,
                            "reqParentRankID": 100
                        },
                        {
                            "kingdomName": "Protista",
                            "rankID": 150,
                            "rankName": "Subfamily",
                            "suffix": null,
                            "directParentRankID": 140,
                            "reqParentRankID": 140
                        },
                        {
                            "kingdomName": "Protista",
                            "rankID": 160,
                            "rankName": "Tribe",
                            "suffix": null,
                            "directParentRankID": 150,
                            "reqParentRankID": 140
                        },
                        {
                            "kingdomName": "Protista",
                            "rankID": 170,
                            "rankName": "Subtribe",
                            "suffix": null,
                            "directParentRankID": 160,
                            "reqParentRankID": 140
                        },
                        {
                            "kingdomName": "Protista",
                            "rankID": 180,
                            "rankName": "Genus",
                            "suffix": null,
                            "directParentRankID": 170,
                            "reqParentRankID": 140
                        },
                        {
                            "kingdomName": "Protista",
                            "rankID": 190,
                            "rankName": "Subgenus",
                            "suffix": null,
                            "directParentRankID": 180,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Protista",
                            "rankID": 220,
                            "rankName": "Species",
                            "suffix": null,
                            "directParentRankID": 210,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Protista",
                            "rankID": 230,
                            "rankName": "Subspecies",
                            "suffix": null,
                            "directParentRankID": 220,
                            "reqParentRankID": 180
                        },
                        {
                            "kingdomName": "Protista",
                            "rankID": 240,
                            "rankName": "Morph",
                            "suffix": null,
                            "directParentRankID": 220,
                            "reqParentRankID": 180
                        }
                    ]

    }

}
