import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsArray, IsInt, Max, Min } from 'class-validator';
import { Type } from 'class-transformer';
import { BaseFindAllParams } from '@symbiota2/api-common';

export class TaxonDescriptionBlockFindAllParams extends BaseFindAllParams {
    static readonly TAXON_DESCR_DEFAULT_LIMIT = 5;
    static readonly TAXON_DESCR_DEFAULT_OFFSET = 0;
    static readonly TAXON_DESCR_MAX_LIMIT = 15;

    @ApiProperty({ name: 'id[]', type: [Number], required: false })
    @Type(() => Number)
    @IsArray()
    @IsInt({ each: true })
    @IsOptional()
    id: number[];

    @Min(0)
    @Max(TaxonDescriptionBlockFindAllParams.TAXON_DESCR_MAX_LIMIT)
    limit: number = TaxonDescriptionBlockFindAllParams.TAXON_DESCR_DEFAULT_LIMIT;

    @ApiProperty({ type: 'number', required: false, default: TaxonDescriptionBlockFindAllParams.TAXON_DESCR_DEFAULT_OFFSET })
    @Min(0)
    offset: number //= TaxonDescriptionBlockFindAllParams.TAXON_DESCR_DEFAULT_OFFSET;
}
