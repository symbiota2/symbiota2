import { Exclude, Expose, Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { Column, PrimaryGeneratedColumn } from 'typeorm';
import { TaxonDescriptionStatementDto } from '../../taxonDescriptionStatement/dto/TaxonDescriptionStatementDto';
import { Taxon, TaxonDescriptionBlock } from '@symbiota2/api-database';
import { TaxonDto } from '../../taxon/dto/TaxonDto';
import { ImageDto } from '../../../../api-plugin-image/src/image/dto/ImageDto';
import { ApiCollectionInput } from '@symbiota2/data-access';

@Exclude()
export class TaxonDescriptionBlockInputDto {
    /*
    constructor(taxonDescriptionBlock: TaxonDescriptionBlock) {
        Object.assign(this, taxonDescriptionBlock)
    }
     */

    @ApiProperty({ type: 'number', required: false })
    @Expose()
    id: number

    //@ApiProperty()
    @Expose()
    taxonID: number

    @ApiProperty({ type: 'string', required: false })
    @Expose()
    caption: string

    @ApiProperty({ type: 'string', required: false })
    @Expose()
    source: string

    @ApiProperty({ type: 'string', required: false })
    @Expose()
    sourceUrl: string

    @ApiProperty({ type: 'string', required: false })
    @Expose()
    language: string

    @ApiProperty({ type: 'number', required: false })
    @Expose()
    adminLanguageID: number | null

    @ApiProperty({ type: 'number',required: false })
    @Expose()
    displayLevel: number

    @ApiProperty({ type: 'number',required: false })
    @Expose()
    creatorUID: number

    @ApiProperty({ type: 'string',required: false })
    @Expose()
    notes: string

    @ApiProperty({ type: Date,required: false })
    @Expose()
    initialTimestamp: Date

}
