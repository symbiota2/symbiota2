import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsArray, IsInt, Max, Min } from 'class-validator'
import { Type } from 'class-transformer'
import { BaseFindAllParams } from '@symbiota2/api-common'

export class ImageFindAllParams extends BaseFindAllParams {
    static readonly IMAGE_FIND_DEFAULT_LIMIT = 5
    static readonly IMAGE_FIND_DEFAULT_OFFSET = 0
    static readonly IMAGE_FIND_MAX_LIMIT = 15

    @ApiProperty({ name: 'id[]', type: [Number], required: false })
    @Type(() => Number)
    @IsArray()
    @IsInt({ each: true })
    @IsOptional()
    id: number[]

    @Min(0)
    @Max(ImageFindAllParams.IMAGE_FIND_MAX_LIMIT)
    limit: number = ImageFindAllParams.IMAGE_FIND_DEFAULT_LIMIT

    @ApiProperty({ type: 'number', required: false, default: ImageFindAllParams.IMAGE_FIND_DEFAULT_OFFSET })
    @Min(0)
    offset: number //= ImageFindAllParams.IMAGE_FIND_DEFAULT_OFFSET
}
