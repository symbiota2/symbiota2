import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsArray, IsInt, Max, Min } from 'class-validator'
import { Type } from 'class-transformer'
import { BaseFindAllParams } from '@symbiota2/api-common'

export class ImageTagFindAllParams extends BaseFindAllParams {
    static readonly IMAGE_DEFAULT_LIMIT = 1000
    static readonly IMAGE_DEFAULT_OFFSET = 0
    static readonly IMAGE_MAX_LIMIT = 5000

    @ApiProperty({ name: 'id[]', type: [Number], required: false })
    @IsOptional()
    @Type(() => Number)
    @IsArray()
    @IsInt({ each: true })
    id: number[]

    @ApiProperty({ name: 'imageId[]', type: [Number], required: false })
    @Type(() => Number)
    @IsArray()
    @IsInt({ each: true })
    @IsOptional()
    imageId: number[]

    @Min(0)
    @Max(ImageTagFindAllParams.IMAGE_MAX_LIMIT)
    @IsOptional()
    limit: number = ImageTagFindAllParams.IMAGE_DEFAULT_LIMIT

    @ApiProperty({ type: 'number', required: false, default: ImageTagFindAllParams.IMAGE_DEFAULT_OFFSET })
    @Min(0)
    @IsOptional()
    offset: number //= ImageTagFindAllParams.IMAGE_DEFAULT_OFFSET
}
