import "reflect-metadata";
const ES_META_PREFIX = 'kg';
const ES_FIELD_PREFIX = `${ES_META_PREFIX}:field`;
const ES_FIELD_LIST_PREFIX = `${ES_META_PREFIX}:fieldList`;
const ESNodeKey = Symbol(`${ES_META_PREFIX}:type`);
const ESRecordIDKey = Symbol(`${ES_META_PREFIX}:recordID`);

export interface ESPropertyType {
    index: string
    type: string
}

/**
 * Class decorator that describes a field in an Elasticsearch index
 * @param indexes - a list of indexes
 */
export function ESNode(indexes: ESPropertyType[]) {
    return Reflect.metadata(ESNodeKey, indexes)
}

/**
 * Property decorator that describes which ES property the property corresponds to
 * @param url The ES URI identifier
 */
export function ESProperty(indexes : ESPropertyType[]) {
    return function(instance, propertyName) {
        const metaKey = `${ES_FIELD_PREFIX}:${propertyName}`
        return Reflect.defineMetadata(metaKey, indexes, instance.constructor)
    }
}

/**
 * Property decorator that describes which ES property the property corresponds to for a list
 * of things
 * @param url The ES URI identifier
 */
export function ESEdge(indexes : ESPropertyType[]) {
    return function(instance, propertyName) {
        const metaKey = `${ES_FIELD_LIST_PREFIX}:${propertyName}`;
        return Reflect.defineMetadata(metaKey, indexes, instance.constructor);
    }
}

/**
 * Property decorator that marks the property as part of an identifier
 */
export function ESID() {
    return function(instance, propertyName) {
        return Reflect.defineMetadata(
            ESRecordIDKey,
            propertyName,
            instance.constructor
        );
    }
}

/**
 * Retrieves the JSON for the given class set by the ESNode decorator
 */
export function getESNode(cls: any) : ESPropertyType[] {
    return Reflect.getMetadata(ESNodeKey, cls);
}

/**
 * Retrieves the URI for the given propertyName on the given class,
 * set by the ESProperty decorator
 */
export function getESProperty(cls: any, propertyName: string) : ESPropertyType[] {
    const metaKey = `${ES_FIELD_PREFIX}:${propertyName}`;
    return Reflect.getMetadata(metaKey, cls);
}

/**
 * Retrieves the URI for the given propertyName on the given class,
 * set by the ESPropertyList decorator
 */
export function getESEdge(cls: any, propertyName: string) : ESPropertyType[] {
    const metaKey = `${ES_FIELD_LIST_PREFIX}:${propertyName}`;
    return Reflect.getMetadata(metaKey, cls);
}

/**
 * Returns the DwC unique identifier for the given class, set by the
 * DwCID decorator
 */
export function ESCoreID(cls: any) {
    return Reflect.getMetadata(ESRecordIDKey, cls);
}

/**
 * Returns whether the given propertyName on the given class is the unique
 * identifier for the class
 */
export function isESID(cls: any, propertyName: string) {
    return propertyName === ESCoreID(cls);
}
