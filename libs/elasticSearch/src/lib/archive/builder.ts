import { Logger } from '@nestjs/common';
import { ESEdgeType } from '@symbiota2/api-elasticsearch'
import { Client } from '@elastic/elasticsearch'

export class ElasticSearchIndexBuilder {

    private static readonly ES_KEY_SEPARATOR = '.'
    private readonly indexName: string;
    private static readonly ES_HOST = 'http://localhost/es' // 'http://localhost:9200',

    private readonly logger = new Logger(ElasticSearchIndexBuilder.name);

    constructor(indexName: string, tmpDir?: string) {
        this.indexName = indexName
    }

    async build() {
        const client = new Client({
            node: ElasticSearchIndexBuilder.ES_HOST,
//            cloud: { id: '<cloud-id>' },
//            auth: { apiKey: 'base64EncodedKey' }
        })

        // Let's start by indexing some data
        await client.index({
            index: 'game-of-thrones',
            document: {
                character: 'Ned Stark',
                quote: 'Winter is coming.'
            }
        })


        // here we are forcing an index refresh, otherwise we will not
        // get any result in the consequent search
        await client.indices.refresh({ index: 'game-of-thrones' })


    }


    async bulkInsert (client: Client) {
        await client.indices.create({
            index: 'tweets',
            mappings: {
                properties: {
                    id: { type: 'integer' },
                    text: {
                        type: 'text',
                        fields: {
                            raw: {
                                type:  "keyword"
                            }
                        }
                        },
                    user: { type: 'keyword' },
                    time: { type: 'date' }
                }
            }
            /*
            operations: {
                mappings: {
                    properties: {
                        id: { type: 'integer' },
                        text: { type: 'text' },
                        user: { type: 'keyword' },
                        time: { type: 'date' }
                    }
                }
            }

             */
        }, { ignore: [400] })

        const dataset = [{
            id: 1,
            text: 'If I fall, don\'t bring me back.',
            user: 'jon',
            date: new Date()
        }, {
            id: 2,
            text: 'Winter is coming',
            user: 'ned',
            date: new Date()
        }, {
            id: 3,
            text: 'A Lannister always pays his debts.',
            user: 'tyrion',
            date: new Date()
        }, {
            id: 4,
            text: 'I am the blood of the dragon.',
            user: 'daenerys',
            date: new Date()
        }, {
            id: 5, // change this value to a string to see the bulk response with errors
            text: 'A girl is Arya Stark of Winterfell. And I\'m going home.',
            user: 'arya',
            date: new Date()
        }]

        const operations = dataset.map(doc => [{ index: { _index: 'tweets' } }, doc])

        const bulkResponse = await client.bulk({ refresh: true, operations })

        if (bulkResponse.errors) {
            const erroredDocuments = []
            // The items array has the same order of the dataset we just indexed.
            // The presence of the `error` key indicates that the operation
            // that we did for the document has failed.
            bulkResponse.items.forEach((action, i) => {
                const operation = Object.keys(action)[0]
                if (action[operation].error) {
                    erroredDocuments.push({
                        // If the status is 429 it means that you can retry the document,
                        // otherwise it's very likely a mapping error, and you should
                        // fix the document before to try it again.
                        status: action[operation].status,
                        error: action[operation].error,
                        operation: dataset[i * 2],
                        document: dataset[i * 2 + 1]
                    })
                }
            })
            console.log(erroredDocuments)
        }

        const count = await client.count({ index: 'tweets' })
        console.log(count)
    }

    // bulkInsert().catch(console.log)

    async addEntity(input,
                    factory,
                    name: string,
                    url: string,
                    keys: string[],
                    propertiesMap: Map<string,string>,
                    edgesMap: Map<string, ESEdgeType>,
                    record: any): Promise<void> {

        // First form the key
        const keyValues = []
        for (const keyField of keys) {
            keyValues.push(record[keyField])
        }

        // Generate the ID string
        const myID = keyValues.join(ElasticSearchIndexBuilder.ES_KEY_SEPARATOR)
        const myUrl = ElasticSearchIndexBuilder.ES_HOST + '/' + name + '#' + myID

        // Add quads for the properties
        for (const [property, url] of propertiesMap) {
            input.push(factory.quad(
                factory.namedNode( myUrl ),
                factory.namedNode(url),
                factory.literal(record[property])))
        }

        // Add quads for the edges
        for (const [edge, edgeType] of edgesMap) {
            // First form the key
            const edgeKeyValues = []
            const name = "__" + edge + "__"
            const records = record[name]
            for (let i = 0; i < records.length; i++) {
                for (let k = 0; k < edgeType.keys.length; k++) {
                    edgeKeyValues.push(records[i][edgeType.keys[k]["propertyName"]])
                }

                // Generate the ID string
                const edgeID = edgeKeyValues.join(ElasticSearchIndexBuilder.ES_KEY_SEPARATOR)
                const edgeUrl = ElasticSearchIndexBuilder.ES_HOST + '/' + edgeType.name + '#' + edgeID

                // Add a quad for the node
                input.push(factory.quad(
                    factory.namedNode( myUrl ),
                    factory.namedNode(edgeType.url),
                    factory.namedNode( edgeUrl)))
            }

        }

        return;
    }

    /*
    async build(archivePath: string) {

    }
     */


}
