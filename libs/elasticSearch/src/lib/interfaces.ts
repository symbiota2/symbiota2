export const DWC_SCHEMA_LOCATION = 'http://rs.tdwg.org/dwc/text/ http://dwc.tdwg.org/text/tdwg_dwc_text.xsd';
export const DWC_XML_NS = 'http://rs.tdwg.org/dwc/text/';
export type DwCAParseCallback = (tmpdir: string, archive: IESAMeta) => any;

/*
The full meta.xml for a dwc archive
 */
export interface IESAMeta {
    archive: {
        $: {
            xmlns: string;
            metadata?: string;
            "xmlns:xsi": string;
            "xsi:schemaLocation": string;
            "xmlns:xs"?: string;
        }
        core: IESAMetaCoreFileType;
        extension?: IESAMetaExtensionFileType[];
    }
}

/*
A core file within the archive
 */
export interface IESAMetaCoreFileType extends IESAMetaFileType {
    id?: IESAMetaIDFieldType;
}

/*
An extension file within the archive
 */
export interface IESAMetaExtensionFileType extends IESAMetaFileType {
    coreid: IESAMetaIDFieldType;
}

/*
List of data files within the archive
 */
export interface IESAMetaFileLocationType {
    location: string[];
}

/*
Attributes shared across all file types, core or extensions
 */
export interface IESAMetaFileType {
    $: {
        rowType: 'http://rs.tdwg.org/dwc/terms/Occurrence' | 'http://rs.tdwg.org/dwc/terms/Taxon';
        fieldsTerminatedBy?: string;
        linesTerminatedBy?: string;
        fieldsEnclosedBy?: string;
        encoding?: string;
        ignoreHeaderLines?: number;
        dateFormat?: string;
    },
    files: IESAMetaFileLocationType[];
    field: IESAMetaFieldType[];
}

/*
A field within one of the archive data files
 */
export interface IESAMetaFieldType {
    $: {
        index?: number;
        term: string;
        default?: string;
        vocabulary?: string;
    }
}

/*
Defines the column that's the primary key for the given file
 */
export interface IESAMetaIDFieldType {
    $: {
        index?: number;
    }
}

// https://dwc.tdwg.org/terms



