import {
    BadRequestException,
    Controller, Delete, ForbiddenException,
    Get, HttpStatus, Inject,
    NotFoundException,
    Param,
    Patch, Post,
    Put,
    Query, Req, Res, UseGuards
} from '@nestjs/common';
import { Elasticsearch } from './dto/elasticsearch';
import { ElasticsearchIdParam } from './dto/elasticsearch-id-param';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { ElasticsearchQuery } from './dto/elasticsearch-query';
import { basename } from 'path';
import { Response } from 'express';
import {
    AuthenticatedRequest,
    JwtAuthGuard,
    TokenService
} from '@symbiota2/api-auth';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';
import { ElasticsearchService } from './elasticsearch.service';
import { QUEUE_ID_GENERATE_ELASTIC_SEARCH } from './queues/elasticsearch-generate.queue';
import { ElasticSearchGenerateJob } from './queues/elasticsearch-generate.processor';

@ApiTags('Elasticsearch')
@Controller('elasticsearch')
export class ElasticsearchController {

    constructor(
        @InjectQueue(QUEUE_ID_GENERATE_ELASTIC_SEARCH)
        private readonly esQueue: Queue<ElasticSearchGenerateJob>,
        @Inject(ElasticsearchService)
        private readonly esService: ElasticsearchService) { }

    @Get('')
    @ApiOperation({ summary: 'Retrieve the list of Elasticsearch indexes for this portal' })
    @ApiResponse({ status: HttpStatus.OK, type: Elasticsearch, isArray: true })
    async getIndexes(): Promise<Elasticsearch[]> {
        const graphs = await this.esService.listIndexes();

        return graphs.map((a) => {
            //const { name, ...graph } = a;
            return new Elasticsearch({
                name: a.name,
                updatedAt: a.updatedAt,
                size: a.size
            })
        })
    }

    /*&
    @Get(':indexName')
    @ApiOperation({ summary: 'Download the Elasticsearch index for a given name' })
    @ApiResponse({
        status: HttpStatus.OK,
        content: {
            'application/json': {
                schema: {
                    type: 'string',
                    format: 'binary'
                }
            }
        }
    })

    async getGraphByName(@Param('graphName') name: string, @Res() res: Response): Promise<void> {
        const graphStream: NodeJS.ReadableStream = await this.esService.getElasticSearchIndex(name);
        if (!graphStream) {
            throw new NotFoundException();
        }

        graphStream.pipe(res);

        return new Promise((resolve, reject) => {
            let error = null;
            graphStream.on('error', (e) => {
                error = e;
                reject(e)
            });
            graphStream.on('end', () => {
                if (!error) {
                    resolve();
                }
            });
        });
    }

     */

    @Post(':indexName')
    @ApiOperation({ summary: 'Build or rebuild an Elasticsearch index ' })
    @ApiBearerAuth()
    @UseGuards(JwtAuthGuard)
    async createElasticSearchIndex (
        @Req() req: AuthenticatedRequest,
        @Param('indexName') name: string): Promise<void> {

        const [isSuperAdmin] = await Promise.all([
            TokenService.isSuperAdmin(req.user)
        ]);

        if (!(isSuperAdmin)) {
            throw new ForbiddenException()
        }

        if (await this.jobIsRunning()) {
            throw new BadRequestException(
                `An Elasticsearch index job is already running`
            )
        }

        await this.esQueue.add({
            publish: true,
            indexName: name,
            userID: req.user?.uid
        })

    }

    private async jobIsRunning(): Promise<boolean> {
        const jobs = await this.esQueue.getJobs(['active', 'waiting']);
        //const matchingJobs = jobs.filter((j) => j.data.collectionID === collectionID);
        return jobs.length > 0;
    }

    @Delete(':indexName')
    @ApiOperation({ summary: 'Delete an elastic search index' })
    @ApiBearerAuth()
    @UseGuards(JwtAuthGuard)
    async deleteElasticSearchIndex (
        @Req() req: AuthenticatedRequest,
        @Param('indexName') name: string): Promise<void> {

        const [isSuperAdmin] = await Promise.all([
            TokenService.isSuperAdmin(req.user)
        ]);

        if (!(isSuperAdmin)) {
            throw new ForbiddenException()
        }

        await this.esService.deleteElasticSearchIndex(name);

    }
}
