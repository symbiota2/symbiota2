import { Inject, Injectable, Logger } from '@nestjs/common';
import { S3Object, StorageService } from '@symbiota2/api-storage';
import {
    EntityMetadata,
    EntityTarget,
    FindManyOptions,
    getConnection, IsNull,
    Repository
} from 'typeorm';
import { withTempDir } from '@symbiota2/api-common';
import { AppConfigService } from '@symbiota2/api-config';
import { join as pathJoin } from 'path';
import { createReadStream, createWriteStream } from 'fs';
import ReadableStream = NodeJS.ReadableStream;
import { ColumnMetadata } from 'typeorm/metadata/ColumnMetadata';
import { RelationMetadata } from 'typeorm/metadata/RelationMetadata';
import { DataFactory } from 'rdf-data-factory';
import { Readable } from 'stream';
import { Elasticsearch } from './dto/elasticsearch';
import { getESEdge, getESNode, getESProperty } from '../../../../elasticSearch/src/lib/decorators';
import { ElasticSearchIndexBuilder } from '@symbiota2/elasticSearch';


interface CreateESOpts {
    publish?: boolean;
}

/**
 * ES edge type
 */
export interface ESEdgeType {
    url: string
    name: string
    keys: string[]
}

/**
 * ES node structure
 */
interface ESGraphNode {
    url: string
    meta: EntityMetadata
    edges: Map<string,ESEdgeType>
    properties: Map<string,string>
    keys: string[]
}

@Injectable()
export class ElasticsearchService {
    private static readonly S3_PREFIX = 'es'
    private static readonly DB_LIMIT = 1024
    private static readonly DEFAULT_CREATE_ARCHIVE_OPTS: CreateESOpts = {
        publish: false
    };
    private readonly logger = new Logger(ElasticsearchService.name)

    constructor(
        private readonly appConfig: AppConfigService,
        private readonly storage: StorageService) { }

    private static s3Key(objectName: string): string {
        return [ElasticsearchService.S3_PREFIX, objectName].join('/');
    }

    // TODO: Redact sensitive localities
    private async createIndex (
        indexName: string,
        nodeMap: Map<string,ESGraphNode>,
        objectTags = {}
    ) {
        const db = getConnection()
        const elasticSearchIndexBuilder = new ElasticSearchIndexBuilder(indexName)
        /*

        const indexPath = pathJoin(tmpDir, indexName)

        // Let's process each kind of node
        for (let [key, value] of nodeMap) {

            // get the repository
            const repo = db.getRepository(value.meta.target)
            let offset = 0
            const edges = []

            for (let edge of value.edges.keys()) {
                edges.push(edge)
            }

            let entities = await repo.find({
                //...findOpts,
                relations: edges,
                take: ElasticsearchService.DB_LIMIT,
                skip: offset
            });

            while (entities.length > 0) {
                await Promise.all(entities.map((o) => elasticSearchIndexBuilder
                    .addEntity(
                        input,
                        factory,
                        value.meta.targetName,
                        value.url,
                        value.keys,
                        value.properties,
                        value.edges,
                        o)))
                offset += entities.length;

                entities = await repo.find({
                    // ...findOpts,
                    relations: edges,
                    take: ElasticsearchService.DB_LIMIT,
                    skip: offset
                })
            }
        }

         */
    }

    async createElasticSearchIndex(indexName: string, opts = ElasticsearchService.DEFAULT_CREATE_ARCHIVE_OPTS): Promise<string> {
        // Map of graph nodes
        const nodeMap : Map<string,ESGraphNode> = new Map()

        // Get its tags
        const tags = {
            indexName: indexName,
            public: opts.publish.toString()
        }

        // Look at the metadata to build up a list of nodes, edges, and properties to process
        const db = getConnection()
        db.entityMetadatas.forEach((entityMeta) => {
            const nodeType = getESNode(entityMeta.target)

            if (nodeType) {
                // This type has been decorated as a KG node
                // Get a list of its columns
                const columns : ColumnMetadata[] = entityMeta.columns

                // Process the object associated with the node type
                // Is the current graph in this type?
                let nodeValue = null
                for (let i = 0; i < nodeType.length; i++) {
                    if (nodeType[i].index == indexName) {
                        // This node is in the graph being built
                        nodeValue = nodeType[i].type
                    }
                }

                // We can skip the rest if the node isn't in the graph
                if (nodeValue == null) {
                    return
                }

                // Set up the mapping
                const propertyMapValues = new Map<string, string>()
                const edgeMapValues = new Map<string, ESEdgeType>()
                const mapValue = {
                    url: nodeValue,
                    meta: entityMeta,
                    edges: edgeMapValues,
                    properties: propertyMapValues,
                    keys: []
                }
                nodeMap.set(entityMeta.targetName, mapValue)

                // Now let's deal with the properties
                const propertyMap = entityMeta.propertiesMap
                const ids = entityMeta.primaryColumns
                // console.log(" keys key is " + entityMeta.name)
                const resolvedKeys = []
                // Get the names of the columns for the primary key
                // eslint-disable-next-line prefer-const
                for (let key in ids) {
                    // console.log(" key " + propertyMap)
                    resolvedKeys.push(columns[key].propertyName)
                }
                mapValue.keys = resolvedKeys

                // Run through the properties
                // eslint-disable-next-line prefer-const
                for (let key in propertyMap) {
                    const propertyType = getESProperty(entityMeta.target, key)
                    if (propertyType) {

                        // Let's check to see if property is in graph
                        let propertyUrl = null
                        for (let i = 0; i < propertyType.length; i++) {
                            if (propertyType[i].index == indexName) {
                                propertyUrl = propertyType[i].type
                            }
                        }
                        if (propertyUrl != null) {
                            propertyMapValues.set(key,propertyUrl)
                        }
                    }
                }

                const linkMap = new Map();
                // eslint-disable-next-line prefer-const
                for (let index in entityMeta.relations) {
                    const relationMeta : RelationMetadata = entityMeta.relations[index]
                    linkMap.set(relationMeta.propertyName, relationMeta)
                }

                // process the edges
                // eslint-disable-next-line prefer-const
                for (let key in propertyMap) {
                    const edgeType = getESEdge(entityMeta.target, key)
                    if (edgeType) {
                        // console.log(" Edge " + key)
                        let edgeUrl = null
                        for (const index in edgeType) {
                            // console.log("KG Edge " + index + " " + edgeType[index] + " ")
                            // Let's check to see if edge is in graph
                            if (edgeType[index].index == indexName) {
                                edgeUrl = edgeType[index].type
                            }
                        }
                        if (edgeUrl != null) {
                            console.log("ES Edge setting " + key + " " + edgeUrl)
                            const relationMeta = linkMap.get(key)
                            edgeMapValues.set(key,
                                {
                                    url: edgeUrl,
                                    name: relationMeta.inverseEntityMetadata.target.name,
                                    keys: relationMeta.inverseEntityMetadata.primaryColumns
                                })
                        }
                    }
                }

            }
        })

        this.logger.log("Creating elasticsearch index ")

        await this.createIndex(
            indexName,
            nodeMap,
            tags
        )

        return indexName
    }

    async listIndexes(): Promise<Elasticsearch[]> {
        // Look at the metadata to build up a list of graph names
        const db = getConnection()
        const indexNames = []
        for (const entityMeta of db.entityMetadatas) {
            const nodeType = getESNode(entityMeta.target)

            if (nodeType) {
                // This type has been decorated as a KG node

                // Process the object associated with the node type
                // Is the current graph in this type?
                // eslint-disable-next-line @typescript-eslint/no-unused-vars
                //let nodeValue = null
                const objs = await this.elasticSearchListObjects()
                const objMap = new Map<string, S3Object>()
                for (const obj of objs) {
                    objMap.set(obj.key, obj)
                }
                for (let i = 0; i < nodeType.length; i++) {
                    const name = nodeType[i].index
                    const mapKey = ElasticsearchService.s3Key(name)
                    if (objMap.has(mapKey)) {
                        const obj = objMap.get(mapKey)
                        indexNames.push(new Elasticsearch({
                            name: name,
                            updatedAt: obj.updatedAt,
                            size: obj.size
                        }))
                    } else {
                        indexNames.push(new Elasticsearch({
                            name: name,
                            updatedAt: null,
                            size: null
                        }))
                    }
                }
            }
        }

        return indexNames
    }

    async indexExists(indexName: string): Promise<boolean> {
        return await this.storage.hasObject(ElasticsearchService.s3Key(indexName))
    }

    async elasticSearchListObjects(): Promise<S3Object[]> {
        return await this.storage.listObjects(ElasticsearchService.S3_PREFIX)
    }

    async publishElasticSearchIndex(indexName: string): Promise<void> {
        await this.updateElasticSearchTags(indexName, { public: 'true' });
    }

    async unpublishElasticSearchIndex(indexName: string): Promise<void> {
        await this.updateElasticSearchTags(indexName, { public: 'false' });
    }

    async getElasticSearchIndex(indexName: string): Promise<ReadableStream> {
        const objectKey = ElasticsearchService.s3Key(indexName)

        if (await this.storage.hasObject(objectKey)) {
            return this.storage.getObject(objectKey)
        }
        return null;
    }

    public deleteElasticSearchIndex(indexName: string) {
        const objectKey = ElasticsearchService.s3Key(indexName)

        return this.storage.deleteObject(objectKey)
    }

    private async updateElasticSearchTags(indexName: string, tags: Record<string, string>): Promise<void> {
        const objectKey = ElasticsearchService.s3Key(indexName);
        await this.storage.patchTags(objectKey, tags);
    }

}
