import { ApiProperty } from '@nestjs/swagger';
import { IsInt } from 'class-validator';

export class ElasticsearchIdParam {
    //@ApiProperty()
    @IsInt()
    graphID: number;
}
