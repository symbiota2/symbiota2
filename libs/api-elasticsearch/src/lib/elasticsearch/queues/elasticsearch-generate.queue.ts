import { BullModule } from '@nestjs/bull';

export const QUEUE_ID_GENERATE_ELASTIC_SEARCH = 'generate-elasticsearch';
export const ElasticsearchGenerateQueue = BullModule.registerQueue({ name: QUEUE_ID_GENERATE_ELASTIC_SEARCH });
