import {
    OnQueueCompleted,
    OnQueueFailed,
    Process,
    Processor
} from '@nestjs/bull';
import { Logger } from '@nestjs/common';
import { Job } from 'bull';
import { NotificationService } from '@symbiota2/api-auth';
import { QUEUE_ID_GENERATE_ELASTIC_SEARCH } from './elasticsearch-generate.queue';
import { ElasticsearchService } from '../elasticsearch.service';

export interface ElasticSearchGenerateJob {
    userID: number;
    indexName: string;
    publish: boolean;
}

@Processor(QUEUE_ID_GENERATE_ELASTIC_SEARCH)
export class ElasticsearchGenerateProcessor {
    private readonly logger = new Logger(ElasticsearchGenerateProcessor.name);

    constructor(
        private readonly elasticSearchService: ElasticsearchService,
        private readonly notifications: NotificationService) { }

    @Process()
    async generateElasticSearchIndex(job: Job<ElasticSearchGenerateJob>) {
        this.logger.debug(
            `Generating Elasticsearch index ...`
        );
        await this.elasticSearchService.createElasticSearchIndex(
            job.data.indexName,
            { publish: job.data.publish }
        );
    }

    @OnQueueCompleted()
    async onESGenerated(job: Job<ElasticSearchGenerateJob>) {
        const message = `Elasticsearch index generation complete `;
        this.logger.debug(message);
        await this.notifications.add(job.data.userID, message);
    }

    @OnQueueFailed()
    async onESGenerateFailed(job: Job<ElasticSearchGenerateJob>, err: Error) {
        const message = `Failed to generate elasticsearch index `;
        this.logger.error(message);
        this.logger.error(JSON.stringify(err));

        await this.notifications.add(
            job.data.userID,
            `${message}: ${JSON.stringify(err)}`
        );
    }
}
