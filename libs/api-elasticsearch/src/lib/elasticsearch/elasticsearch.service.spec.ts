import { Test, TestingModule } from '@nestjs/testing';
import { ElasticSearchService } from './knowledge-graph.service';

describe('ApiKnowledgeGraphService', () => {
  let service: ElasticSearchService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ElasticSearchService],
    }).compile();

    service = module.get<ElasticSearchService>(ElasticSearchService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
