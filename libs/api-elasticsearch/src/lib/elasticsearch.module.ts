import { Module } from '@nestjs/common';
import { StorageModule } from '@symbiota2/api-storage';
import { SymbiotaApiPlugin } from '@symbiota2/api-common';
import { AppConfigModule } from '@symbiota2/api-config';
import { DatabaseModule } from '@symbiota2/api-database';
import { UserModule } from '@symbiota2/api-auth';
import { ElasticsearchService } from './elasticsearch/elasticsearch.service';
import { ElasticsearchGenerateQueue } from './elasticsearch/queues/elasticsearch-generate.queue';
import { ElasticsearchController } from './elasticsearch/elasticsearch.controller';
import { ElasticsearchGenerateProcessor } from './elasticsearch/queues/elasticsearch-generate.processor';

@Module({
    imports: [AppConfigModule, DatabaseModule, StorageModule, ElasticsearchGenerateQueue, UserModule],
    controllers: [ElasticsearchController],
    providers: [ElasticsearchService, ElasticsearchGenerateProcessor],
    exports: [ElasticsearchService]
})
export class ElasticsearchModule extends SymbiotaApiPlugin { }
