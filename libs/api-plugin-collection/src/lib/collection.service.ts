/* eslint-disable no-constant-condition */
/* eslint-disable no-prototype-builtins */
import { BadRequestException, ConflictException, Inject, Injectable, Logger, NotFoundException } from '@nestjs/common';
import {
    Collection,
    CollectionCategoryLink,
    CollectionStat,
    Occurrence,
    TaxaEnumTreeEntry,
    Taxon,
    TaxonomicUnit,
    User,
    UserRole
} from '@symbiota2/api-database';
import { DeepPartial, In, Repository, Any, Raw, SimpleConsoleLogger } from 'typeorm';
import { CollectionFindAllParams } from './dto/coll-find-all.input.dto';
import { BaseService } from '@symbiota2/api-common';
import { ApiUserRoleName } from '@symbiota2/data-access';
import { CollectionFindStatsParam } from './dto/collection-stats-params.dto';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AssignedRoleOutputDto } from './dto/assigned-role-output.dto';
import { AssignedRole } from '@symbiota2/ui-common';

/**
 * Service for manipulating specimen collections
 */
@Injectable()
export class CollectionService extends BaseService<Collection> {
    private stats = [];
    private static readonly OCCURRENCE_BATCH_SIZE = 1024;
    private readonly logger = new Logger('fetch-collection-stats');

    constructor(
        @Inject(Collection.PROVIDER_ID)
        private readonly collections: Repository<Collection>,
        @Inject(CollectionCategoryLink.PROVIDER_ID)
        private readonly categoryLinks: Repository<CollectionCategoryLink>,
        @Inject(CollectionStat.PROVIDER_ID)
        private readonly collectionStats: Repository<CollectionStat>,
        @Inject(Occurrence.PROVIDER_ID)
        private readonly occurrenceRepo: Repository<Occurrence>,
        @Inject(User.PROVIDER_ID)
        private readonly userRepo: Repository<User>,
        @Inject(UserRole.PROVIDER_ID)
        private readonly roleRepo: Repository<UserRole>,
        @Inject(TaxonomicUnit.PROVIDER_ID) private readonly taxonRanks: Repository<TaxonomicUnit>,
        @Inject(Taxon.PROVIDER_ID) private readonly taxa: Repository<Taxon>,
        @Inject(TaxaEnumTreeEntry.PROVIDER_ID) private readonly taxaEnumTree: Repository<TaxaEnumTreeEntry>,
        // @InjectQueue(QUEUE_All_COLLECTION_STATS)
        // private queue: Queue,
        ) {

        super(collections);
    }

    async findByID(id: number): Promise<Collection> {
        const collection = await this.collections.findOne(
            {
                where: {id: id},
                relations: [
                    'institution',
                    'collectionStats'
                ]
            });
        if (!collection) {
            return null;
        }
        return collection;
    }

    /**
     * Queries for a list of collections
     * @param params Filter parameters for the query
     * @return Collection[] The list of collections;
     */
    async findAll(params?: CollectionFindAllParams): Promise<Collection[]> {
        const { orderBy, ...qParams } = params;

        // return await this.collections.find({
        //     order: { [orderBy]: 'ASC' },
        //     where: qParams.id ? { id: qParams.id } : {}
        // });

        const where: any = {};
        if (qParams.id) {
            if (Array.isArray(qParams.id)) {
            where.id = In(qParams.id);
            } else {
            where.id = qParams.id;
            }
        }

        return await this.collections.find({
            order: { [orderBy]: 'ASC' },
            where,
          });
    }

    /**
     * Queries for a list of collections
     * @return collectionStats The list of statistics for all collections;
     */
     async processCollectionStats(findStatsParams: CollectionFindStatsParam): Promise<any[]> {
        this.stats = [];

        if (findStatsParams.catId) {
            const collIds = (await this.findByCategory(findStatsParams.catId)).map(cat => cat.id);

            this.stats[0] = await this.collectionStatsQuery(
                `Collection stats for category ${findStatsParams.catId} processed successfully`,
                undefined,
                collIds
            )
            return this.stats;
        }

        if (findStatsParams.uncat) {
            const subquery = await this.categoryLinks.createQueryBuilder('l')
            .select(['l.collectionID'])
            .distinct(true);

            this.stats[0] = await this.collectionStatsQuery(
                `Collection stats for uncategorized collections processed successfully`,
                subquery
            )
            return this.stats;
        }

        this.stats[0] = await this.collectionStatsQuery(
            `Collection stats for all collections processed successfully`,
            undefined
        )
        return this.stats;
    }

    // getAllStats() {
    //     return this.stats;
    // }


    async collectionStatsQuery(messageLog: string, subQ: any = null, collectionIds: number[] = null) {
        const statsQuery = this.occurrenceRepo.createQueryBuilder('o')
            .addSelect("COUNT(*)", "occurrences")
            .addSelect("COUNT(DISTINCT o.scientificName)", 'sciName')
            .addSelect("COUNT(DISTINCT o.family)", "family")
            .addSelect("COUNT(DISTINCT o.genus)", "genus")
            .addSelect("COUNT(CASE WHEN o.latitude IS NOT NULL AND o.longitude IS NOT NULL THEN 1 END) as hasCoordinates")

            if (subQ) {
                statsQuery.where(`o.collectionID not in (${subQ.getQuery()})`)
            }

            if (collectionIds) {
                statsQuery.where('o.collectionID IN (:...collectionIds)', { collectionIds });
            }

            const stats = await statsQuery.getRawOne();

            const data = {
                total: stats['occurrences'],
                sciName: stats['sciName'],
                family: stats['family'],
                genera: stats['genus'],
                hasCoordinates: stats['hasCoordinates'],
            }
            this.logger.log(messageLog)
            return data;
    }


    /**
     * Retrieves the list of collections with a given collection categoryID
     * @param categoryID The categoryID for the list of collections
     * @return CollectionListItem[] The list of collections for the given
     * categoryID
     */
    async findByCategory(categoryID: number): Promise<Collection[]> {
        // This is too slow without using query builder to filter out fields
        const links = await this.categoryLinks.createQueryBuilder('l')
            .select()
            .where({ categoryID })
            .leftJoin('l.collection', 'collection')
            .orderBy('collection.collectionName', 'ASC')
            .getMany();
        return Promise.all(links.map(async (l) => await l.collection));
    }

    /**
     * Retrieves the list of collections that are not associated with any
     * category
     * @return CollectionListItem[] The list of uncategoried collections
     */
    async findUncategorized(): Promise<Collection[]> {
        const subquery = await this.categoryLinks.createQueryBuilder('l')
            .select(['l.collectionID'])
            .distinct(true);

        const collections = await this.collections.createQueryBuilder('c')
            .select()
            .where(`c.id not in (${subquery.getQuery()})`)
            .orderBy('c.collectionName', 'ASC')
            .getMany();

        return collections;
    }

    /**
     * Creates a new collection
     * @param data Field values for the new collection
     * @return Collection The new collection entity
     */
    async create(data: DeepPartial<Collection>): Promise<Collection> {
        const collection = this.collections.create(data);
        return this.collections.save(collection);
    }

    /**
     * Updates the collection with the given ID
     * @param id The collection ID
     * @param data The collection field values to update
     * @return Collection The updated collection
     */
    async updateByID(id: number, data: DeepPartial<Collection>): Promise<Collection> {
        const updateResult = await this.collections.update({ id }, data);
        if (updateResult.affected > 0) {
            await this.updateLastModified(id);
            return this.findByID(id);
        }
        return null;
    }

    /**
     * Updates the
     */
    async updateLastModified(id: number): Promise<void> {
        await this.collectionStats.update(id, {
            lastModifiedTimestamp: new Date()
        });
    }

    // async getRolesForCollection(collectionID: number): Promise<UserRole[]> {
    //     const reqRoles = [
    //         ApiUserRoleName.COLLECTION_ADMIN,
    //         ApiUserRoleName.COLLECTION_EDITOR
    //     ];
    //     return await this.roleRepo.find({
    //         select: ['id', 'name', 'user'],
    //         where: { name: In(reqRoles), details: collectionID },
    //         relations: ['user']
    //     });
    // }

    // async getRolesForCollection(collectionID: number): Promise<UserRole[]> {
    //     const reqRoles = [
    //         ApiUserRoleName.COLLECTION_ADMIN,
    //         ApiUserRoleName.COLLECTION_EDITOR
    //     ];
    //     const roles = await this.roleRepo.find({
    //         select: ['id', 'name', 'user', 'details'],
    //         where: { name: In(reqRoles), details: Any([{ tablePrimaryKey: collectionID }]) },
    //         relations: ['user']
    //     });

    //     const matchingRole = roles.find(role => {
    //         const details = role.details as { tablePrimaryKey: number }[];
    //         return details.some(detail => detail.tablePrimaryKey === collectionID);
    //     });

    //     return matchingRole ? [matchingRole] : [];
    // }

    async getRolesForCollection(collID: number): Promise<AssignedRoleOutputDto> {

        if (!collID) throw new BadRequestException('Invalid user id');
        const users = await this.userRepo.find();

        const roles = await this.roleRepo.find();
        const details = roles.filter(role => role.details.length > 0)
        .map(role => role.details)
        .reduce((acc, val) => acc.concat(val), [])
        .filter(detail => detail.tablePrimaryKey === collID);

        const assignedRoles = Array.isArray(details) ? details : [details];
        for (const detail of assignedRoles) {
            const user = users.find(user => user.uid === detail.assignedForUID);
            Object.assign(detail, {username: user.username})
        }

        return Promise.resolve({details: assignedRoles });
    }

    async assignRoleForCollection(
        uid: number,
        role: ApiUserRoleName,
        tableName: string,
        tablePrimaryKey: number,
        assignedByUID: number,
        secondVar = null
    ): Promise<void> {

        if (!uid) throw new BadRequestException('Invalid user ID');
        const user = await this.userRepo.findOne({
            where: { uid },
            relations: ['roles']
        });

        if (!user) {
          throw new NotFoundException('User not found');
        }

        const existingRoles = await this.roleRepo.find();
        const collectionRole = (askedRole: ApiUserRoleName) => existingRoles.find(r => r.name === askedRole);
        if (!collectionRole) throw new BadRequestException('Probably the role you are trying to assign does not exist in the table. Try to create it first.')
        // check if user is the original collectionAdmin of the collection
        let originalAdmin = false;

        if (role === ApiUserRoleName.COLLECTION_ADMIN) {
            if (!(collectionRole(ApiUserRoleName.COLLECTION_ADMIN).details.some(detail => detail.access === ApiUserRoleName.COLLECTION_ADMIN && detail.tablePrimaryKey === tablePrimaryKey))) {
                originalAdmin = true;
            }
        }

        if (role === ApiUserRoleName.COLLECTION_EDITOR) {
            if (collectionRole(ApiUserRoleName.COLLECTION_ADMIN).details.some(detail =>
                detail.access === ApiUserRoleName.COLLECTION_ADMIN && detail.assignedForUID === uid && detail.default === true)) {
                originalAdmin = true;
            }
        }

        if (role === ApiUserRoleName.RARE_SPECIES_READER) {
            if (collectionRole(ApiUserRoleName.COLLECTION_ADMIN).details.some(detail =>
                detail.access === ApiUserRoleName.COLLECTION_ADMIN && detail.assignedForUID === uid && detail.default === true)) {
                originalAdmin = true;
            }
        }

        // check if user already has the collection role
        const hasRole = collectionRole(role).details.find(detail =>
                detail.access === role && detail.assignedForUID === uid && detail.tablePrimaryKey === tablePrimaryKey)

        if (hasRole) {
          throw new ConflictException(`User is already the ${role} of this ${hasRole.tableName}.`);
        }

        const existingRole = existingRoles.find(r => r.name === role)

            existingRole.details.push({
                default: originalAdmin,
                tableName: tableName,
                access: role,
                tablePrimaryKey: tablePrimaryKey,
                assignedByUID: assignedByUID,
                assignedForUID: uid,
                secondaryVariable: secondVar
              });

              await this.roleRepo.save(existingRole);
    }

    async unassignCollectionRole(collID: number, uid: number, role: ApiUserRoleName): Promise<AssignedRoleOutputDto> {

        const roles = await this.roleRepo.find();
        const roleToUpdate = roles.find(r => r.name === role);
        if (!roleToUpdate) {
            throw new BadRequestException('No such a role in the roleTable.');
        }
        const details = roleToUpdate.details;
        const index = details.findIndex(detail => detail.assignedForUID === uid && detail.tablePrimaryKey === collID && detail.access === role);
        if (index === -1) {
            throw new BadRequestException('Either invalid userID or user has no such a role assigned.')
        }
        details.splice(index, 1);
        await this.roleRepo.save(roleToUpdate);
        return {details};

    }

    // deleteCollectionByID(id: number) {

    // }

    // async removeAllJobs() {
    //     await this.queue.clean(0, 'completed');
    //     await this.queue.clean(0, 'failed');
    //     await this.queue.clean(0, 'active');
    //   }
}
