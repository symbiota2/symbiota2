import { ApiProperty } from '@nestjs/swagger';
import {
    IsOptional,
    IsArray,
    IsInt,
    IsString,
    IsBoolean
} from 'class-validator';
import { Transform, Type } from 'class-transformer';

export class CollectionFindStatsParam {
    // static readonly DEFAULT_ORDER_BY = 'collectionName';

    // @ApiProperty({ name: 'id[]', type: [Number], required: false })
    // @Type(() => Number)
    // @Transform((collectionIDs) => {
    //     return Number.isInteger(collectionIDs) ? [collectionIDs] : collectionIDs;
    // })
    // @IsArray()
    // @IsInt({ each: true })
    // @IsOptional()
    // id: number[];

    // @ApiProperty({ required: false, default: CollectionFindStatsParam.DEFAULT_ORDER_BY })
    // @IsString()
    // orderBy: string = CollectionFindStatsParam.DEFAULT_ORDER_BY;

    @ApiProperty({name: 'catId', type: Number, required: false})
    @Type(() => Number)
    @IsInt()
    @IsOptional()
    catId: number;

    @ApiProperty({name: 'uncat', type: Boolean, required: false})
    @Type(() => Boolean)
    @IsBoolean()
    @IsOptional()
    uncat: boolean;
}
