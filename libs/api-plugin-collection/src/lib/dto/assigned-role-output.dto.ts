import { Exclude, Expose } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { UserRole } from '@symbiota2/api-database';
import { ApiUserRoleName, ApiUserRole } from '@symbiota2/data-access';

/**
 * Object representing the body of a response that contains a user role
 */
@Exclude()
export class AssignedRoleOutputDto {
    constructor(role: UserRole) {
        Object.assign(this, role);
    }

    //@ApiProperty()
    @Expose()
    details: {
        tableName: string | null,
        tablePrimaryKey: number | null,
        secondaryVariable: string | null,
        access: ApiUserRoleName | null,
        assignedByUID: number | null,
        assignedForUID: number | null,
        default: boolean | false,
    }[];
}
