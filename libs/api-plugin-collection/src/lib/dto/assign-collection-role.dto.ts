import { Exclude, Expose } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { UserRole, User } from '@symbiota2/api-database';
import { ApiUserRoleName, ApiUserRole } from '@symbiota2/data-access';
import { IsInt, IsNotEmpty, IsOptional } from 'class-validator';

/**
 * Object representing the body of a response that contains a user role
 */
export class AssignCollectionRoleDto {
    @IsNotEmpty()
    collectionID: number;

    @IsNotEmpty()
    assignedByUID: number;

    @IsNotEmpty()
    role: ApiUserRoleName;

    @IsNotEmpty()
    @IsOptional()
    roleId: number;

    @IsOptional()
    tableName: string | null;

    @IsOptional()
    secVar: string | null;

}
