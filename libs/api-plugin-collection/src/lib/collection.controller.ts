/* eslint-disable @typescript-eslint/no-unused-vars */
import {
    Controller,
    Get,
    Param,
    Query,
    Post,
    Body,
    HttpStatus,
    HttpCode,
    Delete,
    NotFoundException,
    Patch,
    UseGuards, SerializeOptions, Sse, Res,
    HttpException, Inject
} from '@nestjs/common';
import {
    ApiTags,
    ApiResponse,
    ApiBearerAuth,
    ApiOperation
} from '@nestjs/swagger';
import { CollectionService } from './collection.service';
import { CollectionFindAllParams } from './dto/coll-find-all.input.dto';
import { InstitutionService } from './institution/institution.service';
import {
    CollectionInputDto,
    UpdateCollectionInputDto
} from './dto/Collection.input.dto';
import { HasPermission, JwtAuthGuard } from '@symbiota2/api-auth';
import {
    ProtectCollection
} from './collection-edit-guard/protect-collection.decorator';
import { Collection, CollectionStat, UserRole } from '@symbiota2/api-database';
import { CollectionFindStatsParam } from './dto/collection-stats-params.dto';
import { Response } from 'express';
import { CollectionRoleOutputDto } from './dto/collection-role-dto';
import { AssignCollectionRoleDto } from './dto/assign-collection-role.dto';
import { AssignedRoleOutputDto } from './dto/assigned-role-output.dto';
import { ApiUserPermissionName, ApiUserRoleName } from '@symbiota2/data-access';

/**
 * API routes for manipulating specimen collections
 */
@ApiTags('Collections')
@Controller('collections')
export class CollectionController {
    constructor(
        @Inject(InstitutionService)
        private readonly institutionService: InstitutionService,
        @Inject(CollectionService)
        private readonly collections: CollectionService,
        // @InjectQueue(QUEUE_All_COLLECTION_STATS) private readonly queue: Queue
    ) {}

    @Get()
    @ApiOperation({
        summary: "Retrieve a list of specimen collections"
    })
    @ApiResponse({
        status: HttpStatus.OK,
        type: Collection,
        isArray: true
    })
    async findAll(@Query() findAllParams: CollectionFindAllParams): Promise<Collection[]> {
        const collections = await this.collections.findAll(findAllParams);

        if (collections.length === 0) {
            throw new NotFoundException();
        }
        return collections;
    }

    @Get('roles/:id')
    @ApiOperation({
        summary: "Returns users having roles for a specific collection",
        description: "Only available to the collectionAdmin, globalEditors, globalAdmins or users with the 'SuperAdmin' role"
    })
    @ApiResponse({ status: HttpStatus.OK, type: AssignedRoleOutputDto, isArray: true })
    async getRolesForCollection(@Param('id') id: number): Promise<AssignedRoleOutputDto> {
        return await this.collections.getRolesForCollection(id);

    }

    @Get('processStats')
    @ApiOperation({
    summary: "Generate a list of stats for all collections or for a specific category specified by catId."
    })
    @ApiResponse({
    status: HttpStatus.OK,
    type: CollectionStat,
    isArray: true
    })
    async processStats(
        @Query() findStatsParams: CollectionFindStatsParam,
    ) {

        return await this.collections.processCollectionStats(findStatsParams);

    }


    // @Get(':id/progress')
    // async getJobProgress(@Param('id') id: string) {
    //     const job: Job<unknown> = await this.queue.getJob(id);

    //     if (!job) {
    //     return { progress: 'Job not found' };
    //     }

    //     const progress = job.progress();
    //     return { progress };
    // }

    @Get(':id')
    @ApiOperation({
        summary: "Retrieve a specimen collection by ID"
    })
    @ApiResponse({ status: HttpStatus.OK, type: Collection })
    @SerializeOptions({ groups: ['single'] })
    async findByID(@Param('id') id: number): Promise<Collection> {
        const collection = await this.collections.findByID(id);
        if (collection === null) {
            throw new NotFoundException();
        }
        return collection;
    }

    // @Get('/:uid/assigned')
    // @ApiOperation({
    //     summary: "Return a user assignedRoles",
    //     description: "Only available to the user or users with the 'SuperAdmin' role"
    // })
    // // @HasPermission('view_user_assigned_roles') TODO
    // @HttpCode(HttpStatus.OK)
    // @ApiResponse({ status: HttpStatus.OK, type: AssignedRoleOutputDto, isArray: true })
    // async findUserAssignedRoles(@Param('uid') uid: number) {
    //     return await this.collections.getRolesForCollection(uid);
    // }

    @Post()
    @ApiOperation({
        summary: "Create a new specimen collection"
    })
    @HttpCode(HttpStatus.OK)
    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth()
    @ApiResponse({ status: HttpStatus.OK, type: Collection })
    @HasPermission(ApiUserPermissionName.CREATE_NEW_COLLECTION)
    @SerializeOptions({ groups: ['single'] })
    async create(@Body() data: CollectionInputDto): Promise<Collection> {
        const collection = await this.collections.create(data);
        collection.collectionStats = await collection.collectionStats;
        collection.institution = await collection.institution;
        return collection;
    }

    @Post('roles/:uid')
    @ApiOperation({
        summary: "Assign a collectionRole for a user"
    })
    @HttpCode(HttpStatus.OK)
    @UseGuards(JwtAuthGuard)
    @HasPermission(ApiUserPermissionName.ASSIGN_COLLECTION_ROLE)
    @ApiResponse({ status: HttpStatus.OK, type: Collection })
    @SerializeOptions({ groups: ['single'] })
    async AssignRoleForCollection(
        @Body() data: AssignCollectionRoleDto,
        @Param('uid') uid: number
    ): Promise<any> {
        const collection = await this.collections.assignRoleForCollection(
            uid, // user to which assigning a role capturing from the users list
            data.role, // id of the collectionRole
            data.tableName, // table nme within database
            data.collectionID, // tablePrimarykey of the collection to assign
            data.assignedByUID, // logged in user with access to assign
            data.secVar // optionally more features to be added
            );
        return collection;
    }

    @Patch('roles/:uid')
    @ApiOperation({
        summary: "Assign a collectionRole for a user"
    })
    @HttpCode(HttpStatus.OK)
    @UseGuards(JwtAuthGuard)
    @HasPermission(ApiUserPermissionName.UNASSIGN_COLLECTION_ROLE)
    @ApiResponse({ status: HttpStatus.OK, type: Collection })
    @SerializeOptions({ groups: ['single'] })
    async unAssignCollectionRole(
        @Body() data: {collectionID: number, role: ApiUserRoleName},
        @Param('uid') uid: number
    ): Promise<any> {
        const collection = await this.collections.unassignCollectionRole(
            data.collectionID,
            uid,
            data.role,
        );
        return collection;
    }

    @Delete(':id')
    @ApiOperation({
        summary: "Delete a specimen collection by ID"
    })
    @ProtectCollection('id')
    @HasPermission(ApiUserPermissionName.REMOVE_COLLECTION)
    @HttpCode(HttpStatus.NO_CONTENT)
    @ApiResponse({ status: HttpStatus.NO_CONTENT })
    async deleteByID(@Param('id') id: number): Promise<void> {
        const collection = await this.collections.deleteByID(id);
        if (!collection) {
            throw new NotFoundException();
        }
    }

    @Patch(':id')
    @ApiOperation({
        summary: "Update a specimen collection by ID"
    })
    @ProtectCollection('id')
    @HasPermission(ApiUserPermissionName.UPDATE_COLLECTION)
    @ApiResponse({ status: HttpStatus.OK, type: Collection })
    @SerializeOptions({ groups: ['single'] })
    async updateByID(@Param('id') id: number, @Body() data: UpdateCollectionInputDto): Promise<Collection> {
        const collection = await this.collections.updateByID(id, data);
        if (!collection) {
            throw new NotFoundException();
        }

        collection.collectionStats = await collection.collectionStats;
        collection.institution = await collection.institution;

        return collection;
    }
}
