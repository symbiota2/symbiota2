import { Inject, Injectable, Logger } from '@nestjs/common';
import { Brackets, DeleteResult, In, Repository } from 'typeorm';
import { BaseService } from '@symbiota2/api-common'
import { Express } from 'express';
import { StorageService } from '@symbiota2/api-storage';
import * as fs from 'fs';
import { AppConfigService } from '@symbiota2/api-config';

import path from 'path';
import { I18nInputDto } from './dto/I18nInputDto';
import { S3_STORAGE_PROVIDER_ID } from '../../../api-storage/src/lib/s3-client.provider';

// type File = Express.Multer.File

@Injectable()
export class I18nService {
    public static readonly S3_PREFIX = 'i18n'
    // public static readonly assetsFolder = path.join("data", "uploads", "images")
    private static reverseMap = null
    private readonly logger = new Logger(I18nService.name)

    constructor(
        @Inject(AppConfigService)
        private readonly appConfig: AppConfigService,
        @Inject(StorageService)
        private readonly storageService: StorageService)
    {
        // I18nService.libraryFolder = this.appConfig.imageLibrary()
        // this.loadReverseMap()
    }

    public static s3Key(objectName: string): string {
        return [I18nService.S3_PREFIX, objectName].join('/');
    }

    /**
     * Test to see what kind of translation is currently applied to a key
     * @param data The key to test and the language
     * @return String The kind of translation: default, modification, notranslate
     */
    async kindOfTranslation(key: string, language: string): Promise<String> {
        try {
            // First load the reverse key lookup
            if (language == null || key == null) {
                this.logger.warn("I18n service trying to determine translation type, but missing information, key: " + key  + " language: " + language)
                return null
            }  else {
                // console.log("language is " + language)
                const fileName = path.join("config", "assets", "i18n", "reversed." + language + ".json")
                // console.log("filename is " + fileName)
                const reverseLookup = JSON.parse(fs.readFileSync(fileName).toString())
                // Do we have a key
                if (reverseLookup[key] != undefined) {
                    // See where the key currently resides
                    const dir = reverseLookup[key]

                    // Need to determine where the current file resides
                    return path.basename(dir)
                    /*
                    if (path.basename(dir) == "notranslate") {
                        // Currently not translated
                        return "notranslate"
                    } else {
                        return "modification"
                    }
                     */
                } else {
                    this.logger.warn("I18n service trying to find key " +key + " but key was not found.")
                    return null
                }
            }
        }
        catch (e) {
            this.logger.error("Problem in finding translation type of an i18n key " + e)
            return null
        }
    }

    /**
     * Modify a key value pair
     * @param data The data to update
     * @return I18nInputDto The updated data or null (not found or api error)
     */
    async modify(pair: I18nInputDto): Promise<I18nInputDto> {
        try {
            // First load the reverse key lookup
            if (pair.language == null || pair.key == null || pair.value == null || pair.key == "" || pair.value == "") {
                this.logger.warn("I18n service trying to update key/value pair, but missing information, key: " + pair.key + " value: " + pair.value + " language: " + pair.language)
                return null
            }  else {
                const language = pair.language
                //console.log("language is " + language)
                //const fileName = path.join("apps", "ui", "src", "assets", "i18n", "reversed." + language + ".json")
                const fileName = path.join("config", "assets", "i18n", "reversed." + language + ".json")
                //console.log("filename is " + fileName)
                const reverseLookup = JSON.parse(fs.readFileSync(fileName).toString())
                // Do we have a key
                if (reverseLookup[pair.key] != undefined) {
                    // Glob the original file
                    const dir = reverseLookup[pair.key]
                    const destDir = path.dirname(dir)
                    //console.log("dir " + dir + " " + destDir)
                    // Need to determine where the current file resides
                    let destFileName = ""
                    if (path.basename(dir) == "notranslate") {
                        // zzzzzzzzzz
                        if (pair.translatable) {
                            // Currently not translated
                            // destFileName = path.join(path.dirname(dir), "notranslate", language + ".json")
                            destFileName = path.join("config", "i18n", "modifications", "notranslate", language + ".json")
                            //console.log(" destfilename is " + destFileName)
                            //console.log(" file is " + fs.readFileSync(destFileName).toString())
                            const lookup: Map<string,string> = fs.existsSync(destFileName) ?
                                new Map(Object.entries(JSON.parse(fs.readFileSync(destFileName).toString())))
                                : new Map<string,string>()
                            // Delete the updated pair
                            if (lookup.has(pair.key)) {
                                lookup.delete(pair.key)
                                // Write the updated file
                                fs.writeFileSync(destFileName, JSON.stringify(lookup,null,4))
                                this.logger.log("deleted " + pair.key + " to " + pair.value + " in " + destFileName)
                            }


                            // Load the default or translated version
                            // First try the translated version
                            //destFileName = path.join(destDir, "translate", "modifications", language + ".json")
                            destFileName = path.join("config", "i18n", "modifications", "translate", language + ".json")
                            //console.log("destination is " + destFileName)
                            const translateLookup: Map<string,string> = fs.existsSync(destFileName) ?
                                new Map(Object.entries(JSON.parse(fs.readFileSync(destFileName).toString())))
                                : new Map()
                            var newValue = "";
                            var reversedFileName = "";
                            if (translateLookup.has(pair.key)) {
                                newValue = translateLookup[pair.key]
                                reversedFileName = path.dirname(destFileName)
                            } else {
                                //destFileName = path.join(dir, language + ".json")
                                destFileName = path.join("config", "i18n", "modifications", "translate", language + ".json")
                                const translateLookup: Map<string,string> = fs.existsSync(destFileName) ?
                                    new Map(Object.entries(JSON.parse(fs.readFileSync(destFileName).toString())))
                                    : new Map()
                                if (translateLookup.has(pair.key)) {
                                    newValue = translateLookup[pair.key]
                                    reversedFileName = path.dirname(destFileName)
                                } else {
                                    this.logger.warn("I18n service trying to delete key " + pair.key + " but translated key was not found.")
                                    return null
                                }
                            }

                            // Now update the asset
                            //const assetFileName = path.join("apps", "ui", "src", "assets", "i18n", language + ".json")
                            const configFileName = path.join("config", "assets", "i18n", language + ".json")
                            const keyMap = JSON.parse(fs.readFileSync(configFileName).toString())
                            if (keyMap[pair.key] != undefined) {
                                keyMap[pair.key] = newValue
                                //fs.writeFileSync(assetFileName, JSON.stringify(keyMap,null,4))
                                fs.writeFileSync(configFileName, JSON.stringify(keyMap,null,4))
                            } else {
                                this.logger.warn("I18n service trying to delete key " + pair.key + " but key was not found.")
                                return null
                            }

                            // Now update the reverse key lookup file
                            reverseLookup[pair.key] = reversedFileName
                            fs.writeFileSync(fileName, JSON.stringify(reverseLookup,null,4))
                        }
                        // zzzzzzzzzz
                        // Currently not translated
                        destFileName = pair.translatable ?
                            //path.join(destDir, "translate", "modifications", language + ".json")
                            // : path.join(dir, language + ".json")
                            path.join("config", "i18n", "modifications", "translate", language + ".json")
                            : path.join("config", "i18n", "modifications", "notranslate", language + ".json")
                        //console.log(" my path name is " + destFileName)
                    } else {
                        destFileName = pair.translatable ?
                            //path.join(destDir, "modifications", language + ".json")
                            //: path.join(path.dirname(destDir), "notranslate", language + ".json")
                            path.join("config", "i18n", "modifications", "translate", language + ".json")
                            : path.join("config", "i18n", "modifications", "notranslate", language + ".json")
                        //console.log(" my other path name is " + destFileName)
                    }

                    const lookup = fs.existsSync(destFileName) ?
                        JSON.parse(fs.readFileSync(destFileName).toString())
                        : {}
                    // Set the updated pair
                    lookup[pair.key] = pair.value
                    // Write the updated file
                    fs.writeFileSync(destFileName, JSON.stringify(lookup,null,4))
                    this.logger.log("updated " + pair.key + " to " + pair.value + " in " + destFileName)

                    // Now update the asset
                    //const assetFileName = path.join("apps", "ui", "src", "assets", "i18n", language + ".json")
                    const configFileName = path.join("config", "assets", "i18n", language + ".json")
                    const keyMap = JSON.parse(fs.readFileSync(configFileName).toString())
                    //if (keyMap[pair.key] != undefined) {
                        keyMap[pair.key] = pair.value
                        //fs.writeFileSync(assetFileName, JSON.stringify(keyMap,null,4))
                        fs.writeFileSync(configFileName, JSON.stringify(keyMap,null,4))
                    //} else {
                    //    this.logger.warn("I18n service trying to update key " + pair.key + " but key was not found.")
                    //    return null
                    //}

                    // Now update the reverse key lookup file
                    reverseLookup[pair.key] = path.dirname(destFileName)
                    fs.writeFileSync(fileName, JSON.stringify(reverseLookup,null,4))
                    return pair
                } else {
                    this.logger.warn("I18n service trying to update key " + pair.key + " but key was not found.")
                    return null
                }
            }
        }
        catch (e) {
            this.logger.error("Problem in modifying an i18n key/value pair " + e)
            return null
        }
    }

    /**
     * Delete a notranslate key value pair, ignore otherwise
     * @param data The data to update
     * @return I18nInputDto The deleted data or null (not found or api error)
     */
    /*
    async deleteNotranslate(pair: I18nInputDto): Promise<I18nInputDto> {
        try {
            this.logger.log(" deleteNoTranslate started ")
            // First load the reverse key lookup
            if (pair.language == null || pair.key == null || pair.value == null || pair.key == "" || pair.value == "") {
                this.logger.warn("I18n service trying to delete key/value pair, but missing information, key: " + pair.key + " value: " + pair.value + " language: " + pair.language)
                return null
            }  else {
                const language = pair.language
                // console.log("language is " + language)
                const fileName = path.join("apps", "ui", "src", "assets", "i18n", "reversed." + language + ".json")
                //console.log("filename is " + fileName)
                const reverseLookup = JSON.parse(fs.readFileSync(fileName).toString())
                // Do we have a key
                if (reverseLookup[pair.key] != undefined) {
                    // Glob the original file
                    const dir = reverseLookup[pair.key]
                    const destDir = path.dirname(dir)
                    //console.log("dir " + dir + " " + destDir)
                    // Need to determine where the current file resides
                    let destFileName = ""
                    if (path.basename(dir) == "notranslate") {
                        // Currently not translated
                        destFileName = path.join(path.dirname(dir), "notranslate", language + ".json")
                    } else {
                        // Is not a notranslate pair
                        return null
                    }
                    //console.log(" destfilename is " + destFileName)
                    //console.log(" file is " + fs.readFileSync(destFileName).toString())
                    const lookup: Map<string,string> = fs.existsSync(destFileName) ?
                        new Map(Object.entries(JSON.parse(fs.readFileSync(destFileName).toString())))
                        : new Map<string,string>()
                    //console.log(" lookup is " + lookup)
                    for (const key in lookup.keys()) {
                        //console.log("key is " + key)
                    }
                    // Delete the updated pair
                    if (lookup.has(pair.key)) {
                        lookup.delete(pair.key)
                        // Write the updated file
                        fs.writeFileSync(destFileName, JSON.stringify(lookup,null,4))
                        this.logger.log("deleted " + pair.key + " to " + pair.value + " in " + destFileName)
                    }


                    // Load the default or translated version
                    // First try the translated version
                    destFileName = path.join(destDir, "translate", "modifications", language + ".json")
                    //console.log("destination is " + destFileName)
                    const translateLookup: Map<string,string> = fs.existsSync(destFileName) ?
                        new Map(Object.entries(JSON.parse(fs.readFileSync(destFileName).toString())))
                        : new Map()
                    var newValue = "";
                    var reversedFileName = "";
                    if (translateLookup.has(pair.key)) {
                        newValue = translateLookup[pair.key]
                        reversedFileName = path.dirname(destFileName)
                    } else {
                        destFileName = path.join(dir, language + ".json")
                        const translateLookup: Map<string,string> = fs.existsSync(destFileName) ?
                            new Map(Object.entries(JSON.parse(fs.readFileSync(destFileName).toString())))
                            : new Map()
                        if (translateLookup.has(pair.key)) {
                            newValue = translateLookup[pair.key]
                            reversedFileName = path.dirname(destFileName)
                        } else {
                            this.logger.warn("I18n service trying to delete key " + pair.key + " but translated key was not found.")
                            return null
                        }
                    }

                    // Now update the asset
                    const assetFileName = path.join("apps", "ui", "src", "assets", "i18n", language + ".json")
                    const keyMap = JSON.parse(fs.readFileSync(assetFileName).toString())
                    if (keyMap[pair.key] != undefined) {
                        keyMap[pair.key] = newValue
                        fs.writeFileSync(assetFileName, JSON.stringify(keyMap,null,4))
                    } else {
                        this.logger.warn("I18n service trying to delete key " + pair.key + " but key was not found.")
                        return null
                    }

                    // Now update the reverse key lookup file
                    reverseLookup[pair.key] = reversedFileName
                    fs.writeFileSync(fileName, JSON.stringify(reverseLookup,null,4))
                    return pair
                } else {
                    this.logger.warn("I18n service trying to delete key " + pair.key + " but key was not found.")
                    return null
                }
            }
        }
        catch (e) {
            this.logger.error("Problem in modifying an i18n key/value pair " + e)
            return null
        }
    }

     */

}
