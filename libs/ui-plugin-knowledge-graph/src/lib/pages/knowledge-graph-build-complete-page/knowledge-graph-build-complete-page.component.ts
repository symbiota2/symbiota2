import { Component, OnInit } from '@angular/core';
import { FormBuilder} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core'
import { MatDialog } from '@angular/material/dialog';
import { KnowledgeGraphService } from '../../services/knowledge-graph/knowledge-graph.service';


@Component({
    selector: 'knowledge-graph-build-complete',
    standalone: false,
    templateUrl: './knowledge-graph-build-complete-page.component.html',
    styleUrls: ['./knowledge-grraph-build-complete-page.component.scss'],
})

export class KnowledgeGraphBuildCompletePage implements OnInit {
    public done = false
    public someSkipped = false
    public skippedTaxonsDueToMultipleMatch = null
    public skippedTaxonsDueToMismatchRank = null
    public skippedTaxonsDueToMissingName= null

    // list of all the updates to do to status records
    public skippedStatusesDueToMultipleMatch = null
    public skippedStatusesDueToAcceptedMismatch = null
    public skippedStatusesDueToParentMismatch = null
    public skippedStatusesDueToTaxonMismatch = null

    constructor(
        //private readonly userService: UserService,  // TODO: needed for species hiding
        private readonly kgService: KnowledgeGraphService,
        private router: Router,
        private formBuilder: FormBuilder,
        private currentRoute: ActivatedRoute,
        private readonly translate: TranslateService,
        public dialog: MatDialog
    ) {

    }

    /*
    Called when Angular starts
     */
    ngOnInit() {

    }

    // Called when the process finishes
    onSubmit() {
        /*
        this.kgService.getProblemUploadRows().subscribe((rowsList) => {
            this.skippedTaxonsDueToMultipleMatch = rowsList[0]
            this.skippedTaxonsDueToMismatchRank = rowsList[1]
            this.skippedTaxonsDueToMissingName= rowsList[2]

            // list of all the updates to do to status records
            this.skippedStatusesDueToMultipleMatch = rowsList[3]
            this.skippedStatusesDueToAcceptedMismatch = rowsList[4]
            this.skippedStatusesDueToParentMismatch = rowsList[5]
            this.skippedStatusesDueToTaxonMismatch = rowsList[6]

            if (this.skippedTaxonsDueToMultipleMatch.length > 0
                || this.skippedTaxonsDueToMismatchRank.length > 0
                || this.skippedTaxonsDueToMissingName.length > 0
                || this.skippedStatusesDueToMultipleMatch.length > 0
                || this.skippedStatusesDueToAcceptedMismatch.length > 0
                || this.skippedStatusesDueToParentMismatch.length > 0
                || this.skippedStatusesDueToTaxonMismatch.length > 0
            ) {
                this.someSkipped = true
            }
            this.done = true
        })

         */
    }

}
