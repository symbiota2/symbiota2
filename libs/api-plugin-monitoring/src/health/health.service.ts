import { Inject, Injectable, Logger } from '@nestjs/common';
//import { BaseService, csvIterator } from '@symbiota2/api-common';
//import { DwCArchiveParser, getDwcField } from '@symbiota2/dwc';
import { StorageService } from '@symbiota2/api-storage';
//import path from 'path';
import { Socket } from "net";
import { AppConfigService } from '@symbiota2/api-config';
import { EmailService } from '@symbiota2/api-email';
import {
    DEFAULT_DB_HOST,
    DEFAULT_DB_PORT,
    DEFAULT_KIBANA_PORT,
    DEFAULT_KIBANA_SERVER, DEFAULT_PORT, DEFAULT_REDIS_HOST, DEFAULT_REDIS_PORT,
    DEFAULT_STORAGE_SERVER
} from '../../../api-config/src/configuration';

@Injectable()
export class HealthService {
    private static readonly LOGGER = new Logger(HealthService.name);
    private static apiIP : String = '127.0.0.1'
    private static apiPort : number = +DEFAULT_PORT
    private static dbIP : String = DEFAULT_DB_HOST
    private static dbPort : number = +DEFAULT_DB_PORT
    private static kibanaIP : String = DEFAULT_KIBANA_SERVER
    private static kibanaPort : number = +DEFAULT_KIBANA_PORT
    private static storageServer : string = DEFAULT_STORAGE_SERVER
    private static redisIP : string = DEFAULT_REDIS_HOST
    private static redisPort : number = +DEFAULT_REDIS_PORT


    constructor(
        @Inject(AppConfigService)
        private readonly appConfig: AppConfigService,
        @Inject(EmailService)
        private readonly mailService: EmailService,
        @Inject(StorageService)
        private readonly storageService: StorageService
    )
    {
        // Remove the http// from kibana
        console.log(" appconfig is " + this.appConfig)
        console.log(" storage is " + this.storageService)
        console.log(" email is " + this.mailService)
        const kibanaURL = this.appConfig.kibanaServerAsString()
        const result = kibanaURL.split("//")
        HealthService.kibanaIP = (result.length > 1) ? result[1] : kibanaURL
        //HealthService.kibanaIP = "" + this.appConfig.kibanaServerAsString()
        HealthService.kibanaPort = this.appConfig.kibanaPort()
        HealthService.dbPort = this.appConfig.databasePort()
        HealthService.dbIP = this.appConfig.databaseHost()
        HealthService.redisIP = this.appConfig.redisHost()
        HealthService.redisPort = this.appConfig.redisPort()
        console.log(" redis is " + HealthService.redisIP + ":" + HealthService.redisPort)
        const url = this.appConfig.storageServer()
        console.log(" storage server is " + url)

        /*
        const urlList = this.appConfig.apiURL()
        if (urlList.length >= 1) {
            const url = urlList[0]
            console.log("url is " + url)
            const result = url.split("/")
            console.log("result is " + result)
            if (result.length >= 3) {
                const rest = result[2]
                const n = rest.split(":")
                if (n.length > 1) {
                    HealthService.apiIP = n[0]
                    console.log("url is " + n[0])
                    HealthService.apiPort = +n[1]
                    console.log("ip is " + n[1])
                } else {
                    HealthService.apiIP = rest
                }
            }
        }

         */
    }

    /**
     * Service to check if EMail connection is working
     * @returns Observable of response from email verification casted as `number`
     * @returns `of(null)` if errors
     */
    async isEMailUp() : Promise<number> {
        const result = await this.mailService.verifyConnection()
        return result? 1 : 0
    }

    /**
     * Service to check if Storage Service connection is working
     * @returns Observable of response from storage service verification casted as `number`
     * @returns `of(null)` if errors
     */
    async isStorageServiceUp() : Promise<number> {
        const result = await this.storageService.verifyConnection()
        return result? 1 : 0
    }

    /**
     * Service to check if Kibana is working
     * @returns Observable of response from Kibana server casted as `number`
     * @returns `of(null)` if errors
     */
    async isKibanaUp() : Promise<number> {
        const up = await this.TCPConnectPing(HealthService.kibanaIP, 3000, HealthService.kibanaPort)
        return up ? 1 : 0
    }

    /**
     * Service to check if DB Server is working
     * @returns Observable of response from DB server casted as `number`
     * @returns `of(null)` if errors
     */
    async isDBServerUp() : Promise<number> {
        const up = await this.TCPConnectPing(HealthService.dbIP, 3000, HealthService.dbPort)
        return up ? 1 : 0
    }

    /**
     * Service to check if Redis Server is working
     * @returns Observable of response from Redis server casted as `number`
     * @returns `of(null)` if errors
     */
    async isRedisServerUp() : Promise<number> {
        const up = await this.TCPConnectPing(HealthService.redisIP, 3000, HealthService.redisPort)
        return up ? 1 : 0
    }

    /**
     * Service to check if the API is up
     * @returns Observable of response from api casted as `number`
     * @returns `of(null)` if api errors
     */
    async isAPIUp() : Promise<number> {
        return 1
    }

    /**
     * Service to check if the other services is up
     * @returns Observable of response from api casted as `number[]`
     * @returns `of(null)` if api errors
     */
    async servicePings() : Promise<number[]> {
        console.log(" zzzz " + HealthService.apiIP + HealthService.apiPort)
        const up = await this.TCPConnectPing(HealthService.apiIP, 3000, HealthService.apiPort)
        return [1]

    }

    /**
     * Basic TCP ping that returns true if the connection is successful, false if it fails
     * The socket is closed after the connection attempt, no data is exchanged.
     */
    private TCPConnectPing(ipAddress, timeout=5000, port=80): Promise<boolean> {
        return new Promise((resolve) => {
            const socket = new Socket();
            let connected = false;

            // Set a timeout for the connection attempt
            const timer = setTimeout(() => {
                if (!connected) {
                    socket.destroy();
                    resolve(false); // Connection timed out
                }
            }, timeout);

            socket.connect(port, ipAddress, () => {
                clearTimeout(timer);
                connected = true;
                socket.destroy();
                resolve(true); // Connection successful
            });

            socket.on('error', (error) => {
                clearTimeout(timer);
                if (!connected) {
                    resolve(false); // Connection failed due to error
                }
            });
        });
    }

}
