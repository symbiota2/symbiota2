import {
    Controller,
    Get,
    Param,
    Query,
    Post,
    Body,
    HttpStatus,
    HttpCode,
    Delete,
    NotFoundException,
    Patch,
    UseInterceptors,
    UseGuards,
    UploadedFile,
    BadRequestException,
    Req,
    ForbiddenException, ParseArrayPipe, Inject
} from '@nestjs/common';
import { HealthService } from './health.service';
import { ApiTags, ApiResponse, ApiOperation, ApiBearerAuth, ApiBody } from '@nestjs/swagger';
import {
    AuthenticatedRequest,
    JwtAuthGuard,
    SuperAdminGuard,
    TokenService
} from '@symbiota2/api-auth';
import { Express } from 'express';

@ApiTags('Health')
@Controller('health')
export class HealthController {

    constructor(@Inject(HealthService) private readonly health: HealthService) { }

    // Fetch the health of the api
    @Get('isAPIUp')
    @ApiResponse({ status: HttpStatus.OK, type: Number, isArray: false })
    @ApiOperation({
        summary: "ACK for API, returns 1 or an HTTPStatus error if API is down."
    })
    async isAPIUp(): Promise<Number> {
        // const num = await this.health.isAPIUp()
        return Promise.resolve(1)
    }

    @Get('isStorageServiceUp')
    @ApiResponse({ status: HttpStatus.OK, type: Number, isArray: false })
    @ApiOperation({
        summary: "ACK for Storage service, returns 1 or 0/an HTTPStatus error if down."
    })
    async isStorageServiceUp(): Promise<Number> {
        const num = await this.health.isStorageServiceUp()
        return num
    }

    @Get('isKibanaUp')
    @ApiResponse({ status: HttpStatus.OK, type: Number, isArray: false })
    @ApiOperation({
        summary: "ACK for Kibana service, returns 1 or 0/an HTTPStatus error if down."
    })
    async isKibanaUp(): Promise<Number> {
        const num = await this.health.isKibanaUp()
        return num
    }

    @Get('isDBServerUp')
    @ApiResponse({ status: HttpStatus.OK, type: Number, isArray: false })
    @ApiOperation({
        summary: "ACK for DB service, returns 1 or 0/an HTTPStatus error if down."
    })
    async isDBServerUp(): Promise<Number> {
        const num = await this.health.isDBServerUp()
        return num
    }

    @Get('isRedisServerUp')
    @ApiResponse({ status: HttpStatus.OK, type: Number, isArray: false })
    @ApiOperation({
        summary: "ACK for Redis service, returns 1 or 0/an HTTPStatus error if down."
    })
    async isRedisServerUp(): Promise<Number> {
        const num = await this.health.isRedisServerUp()
        return num
    }

    @Get('isEmailUp')
    @ApiResponse({ status: HttpStatus.OK, type: Number, isArray: false })
    @ApiOperation({
        summary: "ACK for Email service, returns 1 or 0/an HTTPStatus error if down."
    })
    async isEmailUp(): Promise<Number> {
        const num = await this.health.isEMailUp()
        return num
    }

}
