import { Module } from '@nestjs/common'
import { DatabaseModule } from '@symbiota2/api-database'
import { AppConfigModule, AppConfigService } from '@symbiota2/api-config';
import { SymbiotaApiPlugin } from '@symbiota2/api-common';
import { HealthController } from './health/health.controller';
import { HealthService } from './health/health.service'
//import { MulterModule } from '@nestjs/platform-express';
//import { promises as fsPromises } from 'fs';
//import { join as pathJoin } from 'path';
import { StorageModule, StorageService } from '@symbiota2/api-storage';
import { EmailService } from '@symbiota2/api-email';
import { NodeMailerProvider } from '../../api-email/src/lib/node-mailer.provider';

@Module({
    imports: [
        AppConfigModule,
        DatabaseModule,
        StorageModule,
    ],
    providers: [
        HealthService,
        NodeMailerProvider,
        EmailService,
    ],
    controllers: [
        HealthController,
    ],
    exports: [
        HealthService,
    ]
})
export class MonitoringModule extends SymbiotaApiPlugin {

}
