import { Component, Inject, Input, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core'

@Component({
    selector: 'symbiota2-delete-unit-dialog-box',
    standalone: false,
    templateUrl: 'delete-unit-dialog.component.html',
    styleUrls: ['delete-unit-dialog.component.scss']
})

export class DeleteUnitDialogComponent {
    name = ""

    constructor(
        @Optional() @Inject(MAT_DIALOG_DATA) public data: string,
        public dialogRef: MatDialogRef<DeleteUnitDialogComponent>
        )
    {
        this.name = data
    }

    doAction(){
        //this.dialogRef.close({event:document.getElementById("action")})
        this.dialogRef.close({event: "delete"})
    }

    closeDialog(){
        this.dialogRef.close({event:'zzzCancel'})
    }

}
