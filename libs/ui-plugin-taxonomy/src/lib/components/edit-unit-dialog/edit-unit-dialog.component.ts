import { Component, Inject, Input, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core'

@Component({
    selector: 'symbiota2-edit-unit-dialog-box',
    standalone: false,
    templateUrl: 'edit-unit-dialog.component.html',
    styleUrls: ['edit-unit-dialog.component.scss']
})

export class EditUnitDialogComponent {
    name = ""

    constructor(
        @Optional() @Inject(MAT_DIALOG_DATA) public data: string,
        public dialogRef: MatDialogRef<EditUnitDialogComponent>
        )
    {
        this.name = data
    }

    doAction(){
        //this.dialogRef.close({event:document.getElementById("action")})
        this.dialogRef.close({event: "delete"})
    }

    closeDialog(){
        this.dialogRef.close({event:'zzzCancel'})
    }

}
