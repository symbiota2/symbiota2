import { BadRequestException, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { RefreshToken, User, UserRole } from '@symbiota2/api-database';
import { Repository } from 'typeorm';
import { BaseService } from '@symbiota2/api-common';
import { CreateUserInputDto, RoleOutputDto } from '../../dto';
import { UserRoleService } from '../role/user-role.service';
import { AddRolePermissionsDto } from '../../controllers/permissions/dtos/add-role-permissions.dto';
import { UserWithAssignedRoles } from '../../dto/extended-user';
import { ConfigService } from '@nestjs/config';
/**
 * CRUD for Users and UserRoles
 */
@Injectable()
export class UserService extends BaseService<User> {
    constructor(
        @Inject(User.PROVIDER_ID)
        private readonly userRepo: Repository<User>,
        @Inject(UserRole.PROVIDER_ID)
        private readonly roleRepo: Repository<UserRole>,
        @Inject(RefreshToken.PROVIDER_ID)
        private readonly refreshTokenRepo: Repository<RefreshToken>,
        @Inject(UserRoleService)
        private readonly userRoleService: UserRoleService,
        @Inject(UserRoleService)
        private readonly roleService: UserRoleService
    ) {

        super(userRepo);
    }

    /**
     * Returns a user with the given username from the database. An
     * optional list of fields can be provided.
     */
    async findByLogin(username: string, fields?: Array<keyof User>, relations?: string[]): Promise<UserWithAssignedRoles> {
        const user = await this.userRepo.findOne({
            select: fields,
            where: { username },
            relations: relations,
        });

        const roles = await this.roleRepo.find();
        const details = roles.filter(role => role.details?.length > 0)
        .map(role => role.details)
        .reduce((acc, val) => acc.concat(val), [])
        .filter(detail => detail.assignedForUID === user?.uid);

        const assignedRoles = Array.isArray(details) ? details : [details];
        const modifiedUser = Object.assign(user, { assignedRoles: assignedRoles })
        return modifiedUser;
    }

    /**
     * Returns the user with the given email address from the database. An
     * optional list of fields can be provided
     */
    async findByEmail(email: string, fields?: Array<keyof User>): Promise<User> {
        return this.userRepo.findOne({
            select: fields,
            where: { email }
        });
    }

    /**
     * Returns the user with the given login/password from the database. An
     * optional list of fields can be provided.
     */
    async findByLoginWithPassword(username: string, password: string, fields?: Array<keyof User>): Promise<User> {
        return this.userRepo.createQueryBuilder()
            .select(fields)
            .where('username = :username', { username })
            .andWhere('password = PASSWORD(:password)', { password })
            .getOne();
    }

    /**
     * Updates the profile data for the given user in the database
     */
    async patchProfileData(uid: number, userData: Partial<User>): Promise<User> {
        return this.userRepo.save({ uid, ...userData });
    }

    /**
     * Returns all users from the database. An optional list of fields can be
     * provided.
     */
    async findAll(fields?: [keyof User]): Promise<User[]> {
        if (fields) {
            return this.userRepo.find({ select: fields });
        }

        return this.userRepo.find({ relations: ['roles']});
    }

    /**
     * Updates the lastLogin for the given user (to the current date/time)
     */
    async updateLastLogin(uid: number): Promise<boolean> {
        const updateQuery = await this.userRepo.update(
            { uid },
            { lastLogin: new Date() }
        );
        return updateQuery.affected > 0;
    }

    /**
     * Resets the password for the given username and returns the user's email
     * address
     */
    async resetPassword(username: string, newPassword: string): Promise<string> {
        console.log("New password is " + newPassword)
        return null
        await this.userRepo.createQueryBuilder()
            .update()
            .set({ password: () => `PASSWORD('${newPassword}')` })
            .where('username = :username', { username })
            .execute();

        const user = await this.userRepo.findOne({where: { username: username },  select: ['email'] });
        return user ? user.email : null;
    }

    /**
     * Creates a new user in the database (for user profile page)
     */
    async createProfile(userData: CreateUserInputDto): Promise<User> {
        //const guestRole = await this.roleService.findRole('guest');
        const { password, roleid, ...profileData } = userData;
        await this.userRepo.createQueryBuilder()
            .insert()
            .values({ ...profileData, password: () => `PASSWORD('${password}')` })
            .execute();

        return this.userRepo.findOne({where: { username: profileData.username }});
    }

    /**
     * Creates a new user in the database and assign role (for administrators only)
     */
     async addUser(
        data: Partial<CreateUserInputDto>
    ): Promise<User> {
        // const {role, password, ...userData} = data;
        const {password, roleid,  ...userData} = data;
        await this.userRepo.createQueryBuilder()
            .insert()
            .values({ ...userData, password: () => `PASSWORD('${password}')` })
            .execute();

        const user = await this.userRepo.findOne({where: { username: userData.username }});

        //await this.userRoleService.assignRoleAndPermissions(user.uid, r)

        return user;
    }

    /**
     * update current user and its role in the database (for administrators only)
     */
    //  async assignRole(uid: number, roleid: number): Promise<any> {

    //     const user = await this.userRepo.findOne({where: {uid}, relations: ['role']});
    //     const role = await user?.role;
    //     if (role?.id) throw new BadRequestException('User has already a role.');

    //     await this.userRepo.update(uid, {
    //         role: {id: roleid},
    //     })

    //     Promise.resolve(new RoleOutputDto(role))
    // }


}
