/* eslint-disable @nrwl/nx/enforce-module-boundaries */
import { BadRequestException, ConflictException, Inject, Injectable, InternalServerErrorException, UnauthorizedException } from "@nestjs/common";
import { Repository } from 'typeorm';
import { EmailService } from "@symbiota2/api-email";
import { Permission, PermissionRequest, User } from '@symbiota2/api-database';
import { PermissionRequestDto } from "../../controllers/permissions/dtos/permission-request.dto";
import { PermissionInputDto } from "../../controllers/permissions/dtos/permission-input.dto";
import { TokenService } from "../token/token.service";
import { PermissionData } from "../../controllers/permissions/dtos/permissionData.dto";
import { ApiUserRoleName } from "@symbiota2/data-access";

@Injectable()
export class PermissionService {
    portalManagerEmail = 'ali.elham1252@gmail.com';
    prefix = '';
    constructor(
        @Inject(Permission.PROVIDER_ID)
        private repo: Repository<Permission>,
        @Inject(PermissionRequest.PROVIDER_ID)
        private reqRepo: Repository<PermissionRequest>,
        @Inject(User.PROVIDER_ID)
        private user: Repository<User>,
        @Inject(EmailService)
        private emailService: EmailService,
        @Inject(TokenService)
        private token: TokenService,
    ) {}

    async getAll() {
        const permissions = await this.repo.find();
        return permissions;
    }

    async findOne(id: number) {
        if (!id) throw new BadRequestException('Invalid ID');
        const permission = await this.repo.findOne({where: {id}});
        if (!permission) throw new BadRequestException(`Permission with ID ${id} does not exit.`);
        return permission;
    }

    async addPermission(data: PermissionInputDto): Promise<Permission> {
        const permission = this.repo.create(data)
        try {
            return await this.repo.save(permission)
        } catch (error) {
            if (error.code === 'ER_DUP_ENTRY') // duplicate permission
            throw new ConflictException('Permission already exists!');
            else
            throw new InternalServerErrorException('An error occurred processing your request!')
        }
    }

    async updatePermission(id:number, data: PermissionInputDto): Promise<Permission> {

        if (!id) throw new BadRequestException('Invalid permission id.');

        const permission = await this.repo.findOneOrFail({where: {id}});
        if (!permission) throw new BadRequestException('Requested permission does not exist.');
        const updatedPermissionExists = await this.repo.findOne({where: {name: data.name}});
        if (updatedPermissionExists) throw new ConflictException('Updated name already exist as a permission. Try another name.');
        permission.name = data.name;

        await this.repo.save(permission);

        return permission;
    }

    async requestPermission(permissionData: PermissionRequestDto, access: string) {
        this.prefix = 'S2 Permission Request Form: ' + permissionData.subject;

        // save request to db
        if (!access) throw new UnauthorizedException('You are not allowed to make this request!');
        const currentUser = await this.token.findRefreshToken(access);
        const users = await this.user.find({relations: ['roles']});
        console.log('all users for request permissions: ', users)
        const portalManager = users.find(user => user.roles.some(r => r.name === ApiUserRoleName.PORTAL_MANAGER))
        if (!portalManager) throw new BadRequestException('No one with PortalManager Role exists.')
        const email = portalManager.email;

        if (!currentUser.uid) throw new UnauthorizedException('You are not allowed to make this request!');
        const newPermissionRequest = this.reqRepo.create({requesterUID: currentUser.uid, title: permissionData.subject, description: permissionData.body})
        const request = await this.reqRepo.save(newPermissionRequest);

        // send request via email to portal manager
        await this.emailService.send(this.prefix, permissionData.body, email);

        return new PermissionData(request)
    }

    async findAllPermissionRequests() {
        return await this.reqRepo.find();
        // const qb = this.reqRepo.createQueryBuilder('r')

            // qb.leftJoin('r.requester', 'req')
            //     .select(['req.username', 'req.firstName', 'req.lastName'])
            // qb.leftJoin('r.fulfiller', 'f')
            //     .select(['f.username', 'f.firstName', 'f.lastName'])

            // return await qb.getMany();
    }

    async updatePermissionReqStatus(requesterUID: number, status: string) {
        return await this.reqRepo.update(requesterUID, {currentStatus: status})
    }

    async deletePermission(id: number) {
        return this.repo.delete({id})
    }
}
