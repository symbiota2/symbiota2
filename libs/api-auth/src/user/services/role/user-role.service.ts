import { BadRequestException, ConflictException, Inject, Injectable, NotFoundException, UnauthorizedException } from "@nestjs/common";
import { IsNull, Repository } from 'typeorm';
import { UserRole, User } from "@symbiota2/api-database";
import { RoleOutputDto } from "../../dto";
import { PermissionOutputDto } from "../../controllers/permissions/dtos/permission-output.dto";
import { ApiUserRoleName } from "@symbiota2/data-access";
import { AddRolePermissionsDto } from "../../controllers/permissions/dtos/add-role-permissions.dto";
import { UpdateRolePermissionsDto } from "../../controllers/permissions/dtos";
import { TokenService } from "../token/token.service";

@Injectable()
export class UserRoleService {

    private static instance: UserRoleService;

    constructor(
        @Inject(UserRole.PROVIDER_ID)
        private roleRepo: Repository<UserRole>,
        @Inject(User.PROVIDER_ID)
        private userRepo: Repository<User>,
        @Inject(TokenService)
        private readonly tokens: TokenService,
    ) {}


    async findAllRoles(): Promise<RoleOutputDto[]> {
        const roles = await this.roleRepo.find({ relations: ['permissions']});
        // const roles = await this.roleRepo.find();

        const roleDtos = roles.map(async (role) => {
            const permissionDtos = (await role.permissions).map(async (permission) => {
                const permissionDto = new PermissionOutputDto(permission);
                console.log("permissionDto " + permissionDto.name)
                return permissionDto;
            })
            const roleDto = new RoleOutputDto(role);
            roleDto.permissions = await Promise.all(permissionDtos);
            console.log("role Dto " + roleDto.name)
            return roleDto;
        })
        return await Promise.all(roleDtos);
    }

    async findUserRole(uid: number): Promise<RoleOutputDto[]> {
        if (!uid) throw new BadRequestException('Invalid user id');
        const user = await this.userRepo.findOne({where: {uid}, relations: ['roles', 'roles.permissions']});
        if(!user) throw new BadRequestException('This user does not exist in the system.');
        const roleDtos = user.roles.map(async role => {
            const roleDto = new RoleOutputDto(role);
            const perDtos = role.permissions.map(permission => {
                const perDto = new PermissionOutputDto(permission);
                return perDto;
            });

            roleDto.permissions = await Promise.all(perDtos)
            return roleDto;
        })
        return Promise.all(roleDtos)
    }

    // async findUserAssignedRoles(uid: number): Promise<AssignedRoleOutputDto[]> {
    //     if (!uid) throw new BadRequestException('Invalid user id');
    //     const user = await this.userRepo.findOne({where: {uid}});
    //     if(!user) throw new BadRequestException('This user does not exist in the system.');
    //     const assignedRoles = (await this.roleRepo.find()).filter(role => {
    //         if (role.details.length > 0) {
    //             if (role.details.some(detail => detail.assignedForUID === uid)) {
    //                 return true;
    //             }
    //             return false;
    //         }
    //         return false;
    //     })

    //     const assignedRolesDto = assignedRoles.map(role => {
    //         const roleDto = new AssignedRoleOutputDto(role);
    //         return roleDto;
    //     })

    //     return assignedRolesDto;
    // }

    async findRole(id: number): Promise<RoleOutputDto[]> {

        const roles = await this.roleRepo.find({ relations: ['permissions'], where: {id}});

        const roleDtos = roles.map(async (role) => {
            const permissionDtos = (await role.permissions).map(async (permission) => {
                const permissionDto = new PermissionOutputDto(permission);
                return permissionDto;
            })
            const roleDto = new RoleOutputDto(role);
            roleDto.permissions = await Promise.all(permissionDtos);
            return roleDto;
        })
        return await Promise.all(roleDtos);
    }

    async findByIds(ids: number[]): Promise<UserRole[]> {
        return await this.roleRepo.createQueryBuilder('roles')
          .whereInIds(ids)
          .getMany();
      }

    // async findOne(uid: number, name: ApiUserRoleName): Promise<RoleOutputDto> {
    //     const role = await this.roleRepo.findOne({ relations: ['permissions'], where: { uid, name }});
    //     const permissionDtos = role.permissions.map(async (permission) => {
    //         const permissionDto = new PermissionOutputDto(await permission);
    //         return permissionDto;
    //     })
    //     const roleDto = new RoleOutputDto(role);
    //     roleDto.permissions = await Promise.all(permissionDtos)

    //     return Promise.resolve(roleDto);
    // }

    async addNewRole(
        rolePermissions: AddRolePermissionsDto,
    ) {
        const allRoles = await this.roleRepo.find(
            {where: { ...rolePermissions.role }}
        );
        if (allRoles.find(role => role.name === rolePermissions.role.name)) {
            throw new ConflictException('Role already exist!');
        }
        const newRole = await this.roleRepo.create({ permissions: rolePermissions.getIds, ...rolePermissions.role});

        await this.roleRepo.save(newRole);


        return allRoles.map((role) => new RoleOutputDto(role));
    }

    async updateRole(
        id: number,
        rolePermissions: AddRolePermissionsDto,
    ): Promise<RoleOutputDto> {

        if (!id) throw new BadRequestException('Invalid role id.');

        //await this.roleRepo.update({id}, role);

        const existingRoles = await this.roleRepo.find();
        let foundRole = null;
        if (existingRoles.some(role => role.id === id)) {
            foundRole = existingRoles.filter(role => role.id === id)
        } else {
            throw new BadRequestException(`Role with ID ${id} does not exist.`);
        }

        if (existingRoles.filter(role => role.name !== foundRole[0].name).find(
            role => role.name === rolePermissions.role.name
        )) throw new ConflictException('Role with this name already exist.')

        const Role = await this.roleRepo.create({ permissions: rolePermissions.getIds, ...rolePermissions.role});
        // Object.assign(foundRole, Role);
        // await this.roleRepo.save({id, Role});
        Object.assign(foundRole[0], { permissions: rolePermissions.getIds, ...rolePermissions.role});
        await this.roleRepo.save(foundRole[0])

        return new RoleOutputDto(foundRole[0]);

    }

    // async assignRole(
    //     uid: number,
    //     roleid: number,
    // ):Promise<any> {
    //     if (!uid) throw new BadRequestException('Invalid user id');
    //     const user = await this.userRepo.findOne({where: {uid}, relations: ['role']});
    //     const role = await user?.role;
    //     if (role?.id) throw new BadRequestException('User has already a role.');

    //     const roles = await this.roleRepo.find()

    //     if (!(roles.some(role => role.id === roleid))) throw new BadRequestException('Invalid role id');

    //     // tracking role uid. not needed anymore.
    //     // await this.roleRepo.update(roleid, {
    //     //     uid: uid
    //     // })
    //     return await this.userRepo.update(uid, {
    //         role: {id: roleid},
    //     });
    // }

    // modfied for multiple roles
    async assignRole(uid: number, roleIds: number[]): Promise<void> {
        if (!uid) {
          throw new BadRequestException('Invalid user id');
        }

        const user = await this.userRepo.findOne({
          where: { uid }
        });

        if (!user) {
          throw new NotFoundException('User not found');
        }

        const roles = await this.roleRepo.find();

        const existingRoleIds = roles.map(role => role.id);

        if (!(roleIds.every(roleId => existingRoleIds.includes(roleId)))) throw new BadRequestException('Invalid roleIDs')
        //const newRoles = await this.findByIds(roleIds)
        const newRoles = roles.filter(role => roleIds.includes(role.id))
        //user.roles = newRoles;

        user.roles = [...newRoles];

        await this.userRepo.save(user);
    }

    // async updateUserRole(uid: number, roleid: number) {
    //     if (!uid) throw new BadRequestException('Invalid user id');
    //     const user = await this.userRepo.findOne({where: {uid}, relations: ['role']});
    //     const role = await user?.role;
    //     if (!role?.id) throw new BadRequestException('This user has no role.');

    //     const roles = await this.roleRepo.find()
    //     if (!(roles.some(role => role.id === roleid))) throw new BadRequestException('Invalid role id');

    //     return await this.userRepo.update(uid, {
    //         role: {id: roleid},
    //     })
    // }

    //update updateUserRole to allow multiple roles:
    async updateUserRoles(uid: number, roleIds: number[]) {
        if (!uid) throw new BadRequestException('Invalid user id');
        const user = await this.userRepo.findOne({ where: { uid }});
        if (!user) throw new BadRequestException('User not found');
        const roles = await this.roleRepo.find();
        //if (!roles || roles.length !== roleIds.length) throw new BadRequestException('Invalid role ids');
        const existingRoleIds = roles.map(role => role.id)
        if (!(roleIds.every(roleId => existingRoleIds.includes(roleId)))) throw new BadRequestException('Invalid roleIds')

        const newRoles = roles.filter(role => roleIds.includes(role.id))
        // user.roles = roles;
        user.roles = [...newRoles]
        return await this.userRepo.save(user);
    }

    async updateRolePermissions(id: number, rolePermissions: AddRolePermissionsDto) {
        if (rolePermissions.role) throw new BadRequestException('Invalid input');
        if (!id) throw new BadRequestException('Invalid Role ID.');
        //const Role = await this.roleRepo.create({ permissions: permissions.getIds});
        const foundRole = await this.roleRepo.findOne({where: {id}});
        if (foundRole) {
            const Role = await this.roleRepo.create({ permissions: rolePermissions.getIds, ...foundRole});
            await this.roleRepo.save(Role);
        }

        return new RoleOutputDto(foundRole);
    }

    async unassignRole(uid: number, rid: number, key: string) {

        const token = await this.tokens.findRefreshToken(key);

        if (!uid) throw new BadRequestException('Invalid user id');
        const user = await this.userRepo.findOne({where: {uid}, relations: ['roles']});
        const roles = await user?.roles;
        if (roles.length === 0) throw new BadRequestException('User has no role assigned!');

        if (uid === token.uid) throw new BadRequestException("You can't unassign your own roles. Please ask another admin to do it.")

        // tracking role uid not needed anymore.
        // await this.roleRepo.update(role.id, {
        //     uid: null
        // })

        // for if unassigning one role at the time
        //const newRoles = roles.filter(role => role.id !== rid);
        user.roles = [];
        return await this.userRepo.save(user)
        // return await this.userRepo.update(uid, {
        //     role: {id: null},
        // })
    }

    async deleteRole(rid: number) {
        return this.roleRepo.delete({ id: rid });
    }
}
