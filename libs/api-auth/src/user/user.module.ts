/* eslint-disable @nrwl/nx/enforce-module-boundaries */
import { Global, Module } from '@nestjs/common';
import { UserService } from './services/user/user.service';
import { UserController } from './controllers/user.controller';
import { DatabaseModule } from '@symbiota2/api-database';
import { UserRoleController } from './controllers/role/user-role.controller';
import { TokenService } from './services/token/token.service';
import { AppConfigModule } from '@symbiota2/api-config';
import { AppJwtModule } from '../app-jwt.module';
import { ForgotPasswordProcessor } from './services/queues/forgot-password.processor';
import { ForgotPasswordQueue } from './services/queues/forgot-password.queue';
import { EmailModule } from '@symbiota2/api-email';
import { ForgotUsernameQueue } from './services/queues/forgot-username.queue';
import { ForgotUsernameProcessor } from './services/queues/forgot-username.processor';
import { NotificationService } from './services/notification/notification.service';
import { NotificationController } from './controllers/notification/notification.controller';
import { PermissionController } from './controllers/permissions/permission.controller';
import { PermissionService } from './services/permission/permission.service';
import { UserRoleService } from './services/role/user-role.service';

/** 
 * Module for user authorization and profile management.
 * Heavy use of https://docs.nestjs.com/security/authentication
 */
// @Global()
@Module({
    imports: [
        AppJwtModule,
        AppConfigModule,
        DatabaseModule,
        EmailModule,
        ForgotUsernameQueue,
        ForgotPasswordQueue
    ],
    providers: [
        UserService,
        TokenService,
        ForgotUsernameProcessor,
        ForgotPasswordProcessor,
        NotificationService,
        PermissionService,
        UserRoleService,
    ],
    controllers: [
        UserController,
        UserRoleController,
        NotificationController,
        PermissionController,
    ],
    exports: [
        UserService,
        TokenService,
        NotificationService,
        PermissionService,
        UserRoleService
    ]
})
export class UserModule {}
