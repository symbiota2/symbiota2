import { User } from '@symbiota2/api-database';
import { ApiUserRoleName } from '@symbiota2/data-access';
export interface UserWithAssignedRoles extends User {
    assignedRoles: { 
        tableName: string | null,
        tablePrimaryKey: number | null,
        secondaryVariable: string | null,
        access: ApiUserRoleName | null, 
        assignedByUID: number | null,
        assignedForUID: number | null,
        default: boolean | false,
    }[];
}