import { ApiProperty } from '@nestjs/swagger';
import {
    IsEnum, IsInt,
    IsNotEmpty,
    IsOptional
} from 'class-validator';
import { Type } from 'class-transformer';
import { ApiAssignedRole, ApiCreateUserRoleData, ApiUserRoleName } from '@symbiota2/data-access';
import { PermissionInputDto } from '../controllers/permissions/dtos/permission-input.dto';

/**
 * Object representing the body of a POST request for adding a new role to
 * a user
 */
export class RoleInputDto implements ApiCreateUserRoleData {
    //@ApiProperty()
    @IsOptional()
    id?: number;

    //@ApiProperty()
    @IsEnum(ApiUserRoleName)
    @IsNotEmpty()
    name: ApiUserRoleName;

    // @ApiProperty({ required: false })
    // @Type(() => Number)
    // @IsInt()
    // @IsOptional()
    // tablePrimaryKey?: number;

    //@ApiProperty({ required: false})
    @IsOptional()
    details?: ApiAssignedRole[]
}
