import { UserInputDto } from "./user.input.dto";

export class UpdateUserRoleDto extends UserInputDto {}