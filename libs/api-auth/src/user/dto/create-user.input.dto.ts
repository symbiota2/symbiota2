import { UserInputDto } from "./user.input.dto";
import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { ApiCreateUserData, ApiUserRoleName } from '@symbiota2/data-access';
import { AddRolePermissionsDto } from '../../user/controllers/permissions/dtos/add-role-permissions.dto';
import { RoleInputDto } from "./role.input.dto";

/**
 * Object representing a new user. Contains profile data and a username and
 * password.
 */
export class CreateUserInputDto extends UserInputDto implements ApiCreateUserData {

    //@ApiProperty()
    @IsString()
    @IsNotEmpty()
    username: string;

    //@ApiProperty()
    @IsString()
    @IsNotEmpty()
    password: string;
}
