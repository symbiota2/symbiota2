import { Exclude, Expose } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { UserRole } from '@symbiota2/api-database';
import { ApiUserRoleName, ApiUserRole } from '@symbiota2/data-access';
import { PermissionOutputDto } from '../controllers/permissions/dtos/permission-output.dto';
import { IsOptional } from 'class-validator';

/**
 * Object representing the body of a response that contains a user role
 */
@Exclude()
export class RoleOutputDto implements ApiUserRole {
    constructor(role: UserRole) {
        Object.assign(this, role);
    }

    //@ApiProperty()
    @Expose()
    id: number;

    //@ApiProperty()
    @Expose()
    uid: number;

    //@ApiProperty()
    @Expose()
    name: ApiUserRoleName;

    //@ApiProperty()
    // @Expose()
    // tableName: string | null;

    //@ApiProperty()
    // @Expose()
    // tablePrimaryKey: number | null;

    //@ApiProperty()
    // @Expose()
    // assignedByUID: number | null;

    //@ApiProperty()
    details: {
        tableName: string | null,
        tablePrimaryKey: number | null,
        secondaryVariable: string | null,
        access: ApiUserRoleName | null,
        assignedByUID: number | null,
        assignedForUID: number | null,
        default: boolean | false,
    }[];

    //@ApiProperty()
    @Expose()
    permissions: PermissionOutputDto[]
}
