import {
    BadRequestException,
    Body,
    Controller, Delete,
    Get, HttpCode, HttpStatus,
    Inject, NotFoundException,
    Param,
    Patch,
    Post,
    Put,
    Req,
    UseGuards
} from '@nestjs/common';
import {
    ApiBearerAuth, ApiOperation,
    ApiResponse,
    ApiTags
} from '@nestjs/swagger';
import { ApiUserPermissionName } from '@symbiota2/data-access';
import { RoleOutputDto } from '../../dto/role.output.dto';
import { Repository } from 'typeorm';
import { JwtAuthGuard } from '../../../auth/guards/jwt-auth.guard';
import { RoleInputDto } from '../../dto/role.input.dto';
import { UserRole, Permission } from '@symbiota2/api-database';
import { CurrentUserGuard } from '../../../auth/guards/current-user.guard';
import { SuperAdminGuard } from '../../../auth/guards/super-admin/super-admin.guard';
import { PermissionOutputDto } from '../permissions/dtos/permission-output.dto';
import { PermissionUpdateDto } from '../permissions/dtos/permission-update.dto';
import { UserRoleService } from '../../services/role/user-role.service';
import { UpdateRolePermissionsDto } from '../permissions/dtos/update-role-permissions.dto';
import { AddRolePermissionsDto } from '../permissions/dtos/add-role-permissions.dto';
import { HasPermission } from '../permissions/decorators/has-permission.decorator';
import { Request } from 'express';

@ApiTags('Users')
@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
@Controller('userroles')
export class UserRoleController {
    constructor(
        @Inject(UserRole.PROVIDER_ID)
        private readonly roleRepo: Repository<UserRole>,
        @Inject(Permission.PROVIDER_ID)
        private readonly permissionRepo: Repository<Permission>,
        @Inject(UserRoleService)
        private userRole: UserRoleService
    ) { }


    @Get('/all')
    @ApiOperation({
        summary: "Get a list of available roles",
        description: "Only available to a user with the 'SuperAdmin' role. it can be given to any user as well."
    })
    @ApiResponse({ status: HttpStatus.OK, type: RoleOutputDto, isArray: true })
    @HasPermission(ApiUserPermissionName.VIEW_ROLES)
    async getAllRoles(): Promise<RoleOutputDto[]> {
        return await this.userRole.findAllRoles();
    }

    @Get('/:uid/user')
    @ApiOperation({
        summary: "Return a user globalRoles and permissions",
        description: "Only available to the user or users with the 'SuperAdmin' role"
    })
    @HasPermission(ApiUserPermissionName.VIEW_USER_ROLE_AND_PERMISSIONS)
    @HttpCode(HttpStatus.OK)
    @ApiResponse({ status: HttpStatus.OK, type: RoleOutputDto, isArray: true })
    async findUserRole(@Param('uid') uid: number) {
        return await this.userRole.findUserRole(uid);
    }

    // @Get('/:uid/userAssigned')
    // @ApiOperation({
    //     summary: "Return a user assignedRoles",
    //     description: "Only available to the user or users with the 'SuperAdmin' role"
    // })
    // @HasPermission('view_user_assigned_roles')
    // @HttpCode(HttpStatus.OK)
    // @ApiResponse({ status: HttpStatus.OK, type: AssignedRoleOutputDto, isArray: true })
    // async findUserAssignedRoles(@Param('uid') uid: number) {
    //     return await this.userRole.findUserAssignedRoles(uid);
    // }

    @Get('/:rid/role')
    @ApiOperation({
        summary: "Return a role and a list of permissions in S2",
        description: "Only available to the user or users with the 'SuperAdmin' role"
    })
    @HasPermission(ApiUserPermissionName.VIEW_ROLE_AND_PERMISSIONS)
    @HttpCode(HttpStatus.OK)
    @ApiResponse({ status: HttpStatus.OK, type: RoleOutputDto, isArray: true })
    async findRole(@Param('rid') rid: number): Promise<RoleOutputDto[]> {
        return await this.userRole.findRole(rid);
    }

    @Post()
    @ApiOperation({
        summary: "Add a new Role to S2",
        description: "Only available to users with the 'SuperAdmin' role"
    })
    @HasPermission(ApiUserPermissionName.CREATE_NEW_ROLE)
    @HttpCode(HttpStatus.OK)
    @ApiResponse({ status: HttpStatus.OK, type: RoleOutputDto, isArray: false })
    async addNewRole(
        @Body() rolePermissions: AddRolePermissionsDto
    ) {
        return await this.userRole.addNewRole(rolePermissions)
    }

    @Patch(':rid/role')
    @ApiOperation({
        summary: "Update a role and its permissions in S2",
        description: "Only available to users with the 'SuperAdmin' role"
    })
    @HasPermission(ApiUserPermissionName.UPDATE_ROLE)
    @HttpCode(HttpStatus.OK)
    @ApiResponse({ status: HttpStatus.OK, type: RoleOutputDto, isArray: false })
    async UpdateRole(
        @Param('rid') id: number,
        @Body() rolePermissions: AddRolePermissionsDto,
    ) {
        //console.log('rid: ', id, typeof id)
        return await this.userRole.updateRole(id, rolePermissions)
    }

    @Post('/:uid/user')
    @ApiOperation({
        summary: "Assign Role/Roles to a User",
        description: "Only available to users with the 'SuperAdmin' role"
    })
    @HasPermission(ApiUserPermissionName.ASSIGN_ROLE)
    @HttpCode(HttpStatus.OK)
    @ApiResponse({ status: HttpStatus.OK, type: RoleOutputDto, isArray: false })
    async assignRole(
        @Param('uid') uid: number,
        @Body('rids') rids: number[],
    ) {
        return await this.userRole.assignRole(uid, rids)
    }

    @Patch('/:uid/user')
    @ApiOperation({
        summary: "Update a User Roles",
        description: "Only available to users with the 'SuperAdmin' role"
    })
    @HasPermission(ApiUserPermissionName.UPDATE_USER_ROLES)
    @HttpCode(HttpStatus.OK)
    @ApiResponse({ status: HttpStatus.OK, type: RoleOutputDto, isArray: false })
    async updateUserRole(
        @Param('uid') uid: number,
        @Body('rids') rids: number[],
    ) {
        return await this.userRole.updateUserRoles(uid, rids)
    }


    @Patch('/:rid/permissions')
    @ApiOperation({
        summary: "Update a Role Permissions",
        description: "Only available to users with the 'SuperAdmin' role"
    })
    @HasPermission(ApiUserPermissionName.UPDATE_ROLE_PERMISSIONS)
    @HttpCode(HttpStatus.OK)
    @ApiResponse({ status: HttpStatus.OK, type: RoleOutputDto, isArray: false })
    async updateRolePermissions(
        @Param('rid') rid: number,
        @Body() permissions: AddRolePermissionsDto,
    ) {
        return await this.userRole.updateRolePermissions(rid, permissions)
    }


    // @Patch('/:uid/user')
    // @ApiOperation({
    //     summary: "Update a User role and optionally its permissions",
    //     description: "Only available to users with the 'SuperAdmin' role"
    // })
    // // @UseGuards(SuperAdminGuard)
    // @HttpCode(HttpStatus.OK)
    // @ApiResponse({ status: HttpStatus.OK, type: RoleOutputDto, isArray: false })
    // async updateUserRoleAndPermissions(
    //     @Param('uid') uid: number,
    //     @Body() rolePermissions: Partial<UserRole>,
    // ) {
    //     return await this.userRole.updateUserRole(uid, rolePermissions)
    // }

    // @Patch(':rid')
    // @ApiOperation({
    //     summary: "Update a user permissions based on role ID`",
    //     description: "Only available to users with the 'SuperAdmin' role"
    // })
    // @UseGuards(SuperAdminGuard)
    // @HttpCode(HttpStatus.OK)
    // @ApiResponse({ status: HttpStatus.OK, type: RoleOutputDto, isArray: true })
    // async updateRole(
    //     @Param('id') uid: number,
    //     @Param('rid') rid: number,
    //     @Body('permissions') ids: number[],
    // ): Promise<PermissionOutputDto[]> {

    //     const role = await this.roleRepo.findOne({where: { uid, id: rid }});
    //     console.log('old role: ', role)
    //     if (role.id !== rid ) throw new BadRequestException('Invalid user or role!');
    //     Object.assign(role, {permissions: ids.map(id => ({id}))});
    //     console.log('object: ', {uid, permissions: ids.map(id => ({id}))})
    //     console.log('role: ', role)
    //     await this.roleRepo.save(role);

    //     console.log("Updated role");
    //     const allRoles = await this.roleRepo.find({ uid });
    //     return allRoles.map((role) => new RoleOutputDto(role));
    // }

    @Delete('/:uid/user')
    @ApiOperation({
        summary: "Unassign a role from a user",
        description: "Only available to users with the 'SuperAdmin' role"
    })
    @HasPermission(ApiUserPermissionName.UNASSIGN_ROLES)
    @ApiResponse({ status: HttpStatus.OK, type: RoleOutputDto, isArray: true })
    async unassignUserRole(
        @Param('uid') uid: number,
        @Body('rid') rid: number,
        @Req() request: Request
    ) {

        return await this.userRole.unassignRole(uid, rid, request.cookies['refreshToken']);
    }

    @Delete('/:rid/role')
    @ApiOperation({
        summary: "Delete a role from S2",
        description: "Only available to users with the 'SuperAdmin' role"
    })
    @HasPermission(ApiUserPermissionName.REMOVE_ROLE)
    @ApiResponse({ status: HttpStatus.OK, type: RoleOutputDto, isArray: true })
    async deleteRole(
        @Param('rid') rid: number) {

        const result = await this.userRole.deleteRole(rid);
        if (!result.affected) {
            throw new NotFoundException();
        }

        return this.getAllRoles()
    }

}
