/* eslint-disable @nrwl/nx/enforce-module-boundaries */
import {
    applyDecorators,
    SetMetadata,
    UseGuards
} from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { JwtAuthGuard } from 'libs/api-auth/src/auth/guards';
import { PermissionGuard } from '../guards/permission.guard';

export function HasPermission(access: string) {
    return applyDecorators(
        SetMetadata('access', access),
        ApiBearerAuth(),
        UseGuards(PermissionGuard)
    )
}

// export function IsPublic(public_route = false) {
//     return applyDecorators(
//         SetMetadata('public_route', public_route),
//         UseGuards(JwtAuthGuard)
//     )
// }



// // alternative to above
// // export const HasPermission = (access: string) => SetMetadata('access', access)
