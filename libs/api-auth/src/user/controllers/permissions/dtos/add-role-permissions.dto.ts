import { IsArray, IsNotEmpty, IsOptional } from "class-validator";
import { RoleInputDto } from "../../../dto";

export class AddRolePermissionsDto {
    @IsNotEmpty()
    @IsOptional()
    role: RoleInputDto;

    @IsArray()
    @IsOptional()
    permissions: number[];

    get getIds() {
        if (this.permissions) return this.permissions.map((id: number) => ({id}))
        else return null;
    }
}