import { User } from "@symbiota2/api-database";
import { Exclude, Expose } from "class-transformer";

@Exclude()
export class PermissionReqFulDto {
    constructor(user: User) {
        Object.assign(this, user)
    }

    @Expose()
    username: string;

    @Expose()
    firstName: string;

    @Expose()
    lastName: string;
}