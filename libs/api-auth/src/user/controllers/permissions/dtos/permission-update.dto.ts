import { Exclude, Expose } from "class-transformer";
import { PermissionInputDto } from "./permission-input.dto";

@Exclude()
export class PermissionUpdateDto {
    @Expose()
    permissions: PermissionInputDto[]
}