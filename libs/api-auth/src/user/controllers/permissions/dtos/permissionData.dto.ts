import { PermissionRequest, User } from "@symbiota2/api-database";
import { Exclude, Expose } from "class-transformer";

@Exclude()
export class PermissionData {
    constructor(permissionRequest: PermissionRequest) {
        Object.assign(this, permissionRequest)
    }

    @Expose()
    id: number;

    @Expose()
    title: string;

    @Expose()
    description: string;

    @Expose()
    requesterUID: number;

    @Expose()
    fulfillerUID: number;

    @Expose()
    currentStatus: string;

    @Expose()
    priority: number | null;

    @Expose()
    date: Date;

    @Expose()
    statusTrack: any[];

    // @Expose()
    // approvedDate: Date;

    // @Expose()
    // unassignDate: Date;

    // @Expose()
    // requester: User[]

    // @Expose()
    // fulfiller:User[]
}