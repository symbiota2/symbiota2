import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class PermissionOutputDto {
    constructor(permission: PermissionOutputDto) {
        Object.assign(this, permission)
    }

    //@ApiProperty()
    @Expose()
    id: number;

    //@ApiProperty()
    @Expose()
    name: string;
}
