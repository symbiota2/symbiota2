import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class PermissionInputDto  {
    //@ApiProperty()
    @IsNotEmpty()
    @IsString()
    name: string;
}
