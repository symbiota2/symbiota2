import { IsNotEmpty } from "class-validator";

export class PermissionRequestDto {

    @IsNotEmpty()
    subject?: string;

    @IsNotEmpty()
    body: string;
}