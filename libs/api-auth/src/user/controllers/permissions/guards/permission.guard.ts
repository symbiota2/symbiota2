/* eslint-disable @nrwl/nx/enforce-module-boundaries */
import { Reflector } from '@nestjs/core';
import {
    CanActivate,
    ExecutionContext,
    Inject,
    Injectable
} from '@nestjs/common';
import { AppConfigService } from '@symbiota2/api-config';
import { JwtAuthGuard, RefreshCookieGuard } from 'libs/api-auth/src/auth/guards';
import { AuthenticatedRequest } from 'libs/api-auth/src/auth/dto/authenticated-request';
import { UserRoleService } from '../../../services/role/user-role.service';
import { RoleOutputDto } from '../../../dto';
import { ApiUserPermissionName, ApiUserRoleName } from '@symbiota2/data-access';

@Injectable()
export class PermissionGuard extends JwtAuthGuard implements CanActivate {
    roleIDs: number[] = [];
    hasAccess=false;
    constructor(
        private readonly reflector2: Reflector,
        @Inject(AppConfigService)
        private readonly config: AppConfigService,
        @Inject(UserRoleService)
        private readonly userRole: UserRoleService
    ) {
        super(config)
    }

    async canActivate(context: ExecutionContext): Promise<boolean> {

        if (!(await super.canActivate(context))) {
            return false;
        }

        if (!this.config.isAuthEnabled()) {
            return true;
        }

        const access = this.reflector2.get<string>('access', context.getHandler());

        if (!access) {
            return true;
        }

        const http = context.switchToHttp();
        const request = http.getRequest<AuthenticatedRequest>();

        // console.log('user role from permission: ', request.user)
        // console.log('assignedRoles from inside guard: ', await request.user?.assignedRoles)




        // TODO: will be implemented better later (through ui)
        // new modified code for assignedRoles
        if (request.user?.assignedRoles?.length > 0) {
            for (const assignedRole of request.user?.assignedRoles) {
                if (assignedRole.access === ApiUserRoleName.SUPER_ADMIN) {
                    return true
                }
                if (assignedRole.access === ApiUserRoleName.COLLECTION_ADMIN &&
                    access === ApiUserPermissionName.VIEW_USERS ||
                    access === ApiUserPermissionName.ASSIGN_COLLECTION_ROLE ||
                    access === ApiUserPermissionName.UNASSIGN_COLLECTION_ROLE
                ) {
                    return true;
                }
            }
        }

        if (request.user?.roles.length > 0) {

            for(const role of request.user?.roles) {
                const rolePermissions: RoleOutputDto[] = await this.userRole.findRole(Number(role.id));
                this.hasAccess = rolePermissions[0].permissions.some(p => p.name === access);
                if (this.hasAccess) return this.hasAccess;
            }
            return this.hasAccess;
        }

        // old stable code for globalRoles
        // if (await request.user?.roles.length > 0) {

        //     for(const role of request.user?.roles) {
        //         const rolePermissions: RoleOutputDto[] = await this.userRole.findRole(Number(role.id));
        //         this.hasAccess = rolePermissions[0].permissions.some(p => p.name === access);
        //         if (this.hasAccess) return this.hasAccess;
        //     }
        //     return this.hasAccess;
        // }
    }
}
