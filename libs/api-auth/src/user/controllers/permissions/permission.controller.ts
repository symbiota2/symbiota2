import {
    Body,
    Controller,
    Get,
    HttpStatus,
    Post,
    UnauthorizedException,
    Req,
    Patch,
    Param,
    Delete,
    NotFoundException, Inject
} from '@nestjs/common';
import { PermissionRequestDto } from "./dtos/permission-request.dto";
import { PermissionService } from "../../services/permission/permission.service";
import { PermissionOutputDto } from "./dtos/permission-output.dto";
import { ApiOperation, ApiResponse } from "@nestjs/swagger";
import { PermissionInputDto } from "./dtos/permission-input.dto";
import { Request } from 'express';
import { PermissionData } from "./dtos/permissionData.dto";
import { HasPermission } from './decorators/has-permission.decorator';

@Controller('permissions')
export class PermissionController {

    constructor(
        @Inject(PermissionService)
        private perService: PermissionService)
    {}

    @Get()
    @HasPermission('view_permissions')
    @ApiResponse({ status: HttpStatus.OK, type: PermissionOutputDto, isArray: true })
    @ApiOperation({
        summary: "Retrieve a list of permissions"
    })
    async getAll() {
        const permissions = await this.perService.getAll();
        const permissionDtos = permissions.map(async (permission) => {
            const permissionDto = new PermissionOutputDto(permission);
            return permissionDto
        })
        return Promise.all(permissionDtos)
    }

    @Get('/:id')
    @HasPermission('view_permission')
    @ApiResponse({ status: HttpStatus.OK, type: PermissionOutputDto, isArray: true })
    @ApiOperation({
        summary: "Retrieve a single permission"
    })
    async findOne(@Param('id') id: number) {
        const permission = await this.perService.findOne(id);
        const permissionDto = new PermissionOutputDto(permission);
        return permissionDto;
    }

    @Get('/requests')
    @HasPermission('view_requested_permissions')
    @ApiResponse({ status: HttpStatus.OK, type: PermissionData, isArray: true })
    @ApiOperation({
        summary: "Retrive a list of permission requests"
    })
    async getAllRequests() {
        const requests = await this.perService.findAllPermissionRequests();
        const requestDtos = requests.map(async (request) => {
            const reqDto = new PermissionData(request);
            return reqDto;
        })
        return Promise.all(requestDtos)
    }

    @Post()
    @HasPermission('create_new_permission')
    @ApiResponse({ status: HttpStatus.OK, type: PermissionData, isArray: false })
    @ApiOperation({
        summary: "Add new permission to S2"
    })
    async addPermission(@Body() body: PermissionInputDto) {
        return await this.perService.addPermission(body);
    }

    @Patch('/:id')
    @HasPermission('edit_permission')
    @ApiResponse({ status: HttpStatus.OK, type: PermissionData, isArray: false })
    @ApiOperation({
        summary: "Edit/update permission in S2"
    })
    async updatePermission(
        @Param('id') id: number,
        @Body() body: PermissionInputDto
    ) {
        return await this.perService.updatePermission(id, body);
    }

    @Post('/request')
    @ApiResponse({ status: HttpStatus.OK, type: PermissionData, isArray: false })
    @ApiOperation({
        summary: "Request a new permission if user is logged in"
    })
    requestPermissions(@Body() body: PermissionRequestDto, @Req() req: Request) {
        return this.perService.requestPermission(body, req.cookies['refreshToken'])
    }

    // found this one very tricky, since user does not know what roles/permissions they
    // need so that an admin/portal manager could simply approve.
    // @Patch('/request/:reqid')
    // @ApiResponse({ status: HttpStatus.OK, type: PermissionData, isArray: false })
    // @ApiOperation({
    //     summary: "Approve/upapprove a permission request"
    // })
    // updateRequestStatus(
    //     @Param('reqid') reqid: number,
    //     @Body('status') status: string,
    // ) {
    //     return this.perService.updatePermissionReqStatus(reqid, status)
    // }

    @Delete('/:id')
    @HasPermission('remove_permission')
    @ApiResponse({ status: HttpStatus.OK, type: PermissionData, isArray: true })
    @ApiOperation({
        summary: "Delete a permission by id from S2"
    })
    async deletePermission(
        @Param('id') id: number,
    ) {
        const result = await this.perService.deletePermission(id);
        if (!result.affected) throw new NotFoundException()

        return await this.getAll()
    }
}
