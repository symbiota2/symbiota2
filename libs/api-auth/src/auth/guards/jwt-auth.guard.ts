import { Injectable, ExecutionContext, Inject } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';
import { AppConfigService } from '@symbiota2/api-config';

/**
 * Guard for populating the current user based on the JWT sent with the request
 * ../strategies/jwt.strategy.ts,
 * (see https://docs.nestjs.com/security/authentication and
 * https://docs.nestjs.com/guards)
 */
@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
    reflector: Reflector;

    constructor(
        @Inject(AppConfigService)
        protected readonly configService: AppConfigService
    ) {
        super();
    }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        if (!this.configService.isAuthEnabled()) {
            return true;
        }

        // this.reflector = new Reflector;
        // const publicRoute = this.reflector.get<boolean>('public_route', context.getHandler())

        // console.log('publicRoute: ', publicRoute)

        // if (publicRoute) {
        //     return true;
        // }

        return super.canActivate(context) as Promise<boolean>;
    }
}
