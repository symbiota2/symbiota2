import { NavBarLink, SymbiotaUiPlugin } from '@symbiota2/ui-common';
import { Route } from '@angular/router';
declare function uiPluginCollectionCustomMenu(): NavBarLink[]

export class LocalMenu {
    public static routes(): Route[] {
        return [];
    }

    static navBarLinks(): NavBarLink[] {
        const menu = uiPluginCollectionCustomMenu()
        return menu;
    }
}

