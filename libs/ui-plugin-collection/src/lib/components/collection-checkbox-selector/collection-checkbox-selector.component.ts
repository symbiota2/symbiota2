import {
    Component,
    EventEmitter,
    Input,
    Injectable,
    Output, OnChanges, SimpleChanges, OnInit, ChangeDetectorRef
} from '@angular/core';
import { CollectionService } from '../../services/collection.service';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { SelectionModel } from '@angular/cdk/collections';
import { MatCheckbox } from '@angular/material/checkbox';
import { CollectionListItem } from '../../dto/Collection.output.dto';
import { CollectionCategory } from '../../dto/Category.output.dto';
import { filter, map } from 'rxjs/operators';

const ROOT_NODE_ID = 65536;

class TreeNode {
    id: number;
    name: string;
    icon: string;
    type: string;
    children: TreeNode[];

    constructor(id: number, icon: string, type: string) {
        this.id = id;
        this.name = '';
        this.icon = icon;
        this.type = type
        this.children = [];
    }

    hasChildren(): boolean {
        return this.children.length > 0;
    }

    isRootNode(): boolean {
        return this.id === ROOT_NODE_ID;
    }

    getChildIDs(): number[] {
        return this.children.map((child) => child.id);
    }
}

@Injectable()
export class CollectionTreeData {
    tabSelected = 0
    public data
        = this.collectionService.categories.pipe(
        map((categories) => {
            const rootNode = new TreeNode(ROOT_NODE_ID, '', null);

            categories.forEach((category) => {
                rootNode.children.push(this.buildTreeData(category));
            });

            return [rootNode];
        })
    );


    constructor(private collectionService: CollectionService) {
    }

    setTabSelected(tab) {
        this.tabSelected = tab
        this.data = this.collectionService.categories.pipe(
            map((categories) => {
                const rootNode = new TreeNode(ROOT_NODE_ID, '', null);

                categories.forEach((category) => {
                    rootNode.children.push(this.buildTreeData(category));
                });

                return [rootNode];
            })
        );
    }

    mapIt() {
        this.collectionService.categories.pipe(
            map((categories) => {
                const rootNode = new TreeNode(ROOT_NODE_ID, '', null);

                categories.forEach((category) => {
                    rootNode.children.push(this.buildTreeData(category));
                });

                return [rootNode];
            })
        );
    }

    buildTreeData(currentNode: CollectionListItem | CollectionCategory): TreeNode {
        const newNode = new TreeNode(
            currentNode.id,
            currentNode.icon,
            null
        );

        if ('category' in currentNode) {
            const category = currentNode as CollectionCategory;
            newNode.name = category.category;

            category.collections.forEach((collection) => {
                newNode.children.push(this.buildTreeData(collection));
            });
        }
        else {
            newNode.name = currentNode.collectionName;
            newNode.type = currentNode.type
        }

        return newNode;
    }
}

@Component({
    selector: 'symbiota2-collection-checkbox-selector',
    standalone: false,
    templateUrl: './collection-checkbox-selector.component.html',
    styleUrls: ['./collection-checkbox-selector.component.scss'],
    providers: [CollectionTreeData]
})
export class CollectionCheckboxSelectorComponent implements OnChanges, OnInit {
    @Input() collectionIDs: number[];
    @Input() selectAll = false;
    @Output() collectionIDsChange = new EventEmitter<number[]>();
    selectedTabIndex = 0

    public treeControl = new NestedTreeControl<TreeNode>(node => node.children);
    public dataSource = new MatTreeNestedDataSource<TreeNode>();

    public collectionSelections = new SelectionModel<number>(true);

    constructor(private treeDataManager: CollectionTreeData) { }

    ngOnInit() {
        this.buildList()
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.collectionIDs) {
            this.collectionSelections.clear();
            this.collectionSelections.select(...changes.collectionIDs.currentValue);
        }
    }

    onTabChanged(event) {
        this.treeDataManager.setTabSelected(event.index)
        this.buildList()
    }

    buildList() {
        this.treeDataManager.data.subscribe((treeNodesRaw) => {
            const treeNodes = treeNodesRaw.filter((node) => {
                 return (node.type == null) || (
                (this.selectedTabIndex == 1 && node.type == 'Preserved Specimens') ||
                (this.selectedTabIndex == 2 && (node.type == 'General Observations' || node.type == 'Observations')) ||
                (this.selectedTabIndex == 0)
            )})

            const rootNode = treeNodes[0];

            // Filter nodes base on selected tab
            const rootChildren = []
            rootNode.children.forEach((category) => {
                const children = category.children
                const newChildren = children.filter((node) => {
                    return (node.type == null) || (
                        (this.selectedTabIndex == 1 && node.type == 'Preserved Specimens') ||
                        (this.selectedTabIndex == 2 && (node.type == 'General Observations' || node.type == 'Observations')) ||
                        (this.selectedTabIndex == 0)
                    )
                })
                category.children = newChildren
                if (newChildren.length > 0) {
                    rootChildren.push(category)
                }
            });

            rootNode.children = rootChildren

            /*
            this.selectAll = true
            this.collectionIDs = []
             */

            // We always want to update the list, not just on the first go
            //if (this.selectAll && this.collectionIDs.length === 0) {
                const selectedIDs = [];
                rootNode.children.forEach((category) => {
                    const collectionIDs = category.getChildIDs();
                    selectedIDs.push(...collectionIDs);
                });
                this.collectionIDsChange.emit(selectedIDs);
            //}
            this.dataSource.data = treeNodes;
            this.treeControl.expand(rootNode);
        })

    }

    nodeHasChildren(_: number, node: TreeNode) {
        return node.hasChildren();
    }

    nodeSomeChildrenSelected(node: TreeNode, initialValue = false): boolean {
        const currentSelections = this.collectionSelections.selected;

        if (initialValue) {
            return true;
        }

        if (this.nodeAllChildrenSelected(node)) {
            return false;
        }

        return node.children.reduce<boolean>((someSelected, currentNode) => {
            if (currentNode.hasChildren()) {
                return this.nodeSomeChildrenSelected(currentNode, someSelected);
            }

            return someSelected || currentSelections.includes(currentNode.id);

        }, initialValue);
    }

    nodeAllChildrenSelected(node: TreeNode, initialValue = true): boolean {
        const currentSelections = this.collectionSelections.selected;

        if (initialValue) {
            return node.children.reduce<boolean>((allSelected, currentNode) => {
                if (currentNode.hasChildren()) {
                    return this.nodeAllChildrenSelected(currentNode, allSelected);
                }

                return allSelected && currentSelections.includes(currentNode.id);

            }, initialValue);
        }

        return false;
    }

    toggleCollection(checkbox: MatCheckbox, collectionID: number) {
        if (checkbox.checked) {
            this.collectionSelections.select(collectionID);
        }
        else {
            this.collectionSelections.deselect(collectionID);
        }

        this.collectionIDsChange.emit(this.collectionSelections.selected);
    }

    // So we can finish all recursive updates before emitting an event
    toggleCategory(checkbox: MatCheckbox, node: TreeNode) {
        this._toggleCategory(checkbox, node);
        this.collectionIDsChange.emit(this.collectionSelections.selected);
    }

    private _toggleCategory(checkbox: MatCheckbox, node: TreeNode) {
        if (node.isRootNode()) {
            node.children.forEach((categoryNode) => {
                this._toggleCategory(checkbox, categoryNode);
            });
        }
        else {
            const childIDs = node.getChildIDs();

            if (checkbox.checked) {
                this.collectionSelections.select(...childIDs);
            }
            else {
                this.collectionSelections.deselect(...childIDs);
            }
        }
    }
}
