import {
    ApiCollectionInstitutionOutput,
    ApiCollectionListItem, ApiCollectionOutput, ApiCollectionStatsOutput
} from '@symbiota2/data-access';
import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class CollectionListItem implements ApiCollectionListItem {
    constructor(collection: ApiCollectionListItem) {
        Object.assign(this, collection);
    }

    @Expose() id: number;
    @Expose() collectionName: string;
    @Expose() icon: string;
    @Expose() collectionCode: string;
    @Expose() institutionCode: string;
    @Expose() type: string;
}

@Exclude()
export class CollectionListStats {

    constructor(stats: any) {
        Object.assign(this, stats)
    }

    @Expose() recordCount: number;
    @Expose() georeferencedCount: number;
    @Expose() familyCount: number;
    @Expose() genusCount: number;
    @Expose() speciesCount: number;
}
