import { Inject, Injectable } from '@nestjs/common';
import {
    Collection,
    Occurrence,
    OccurrenceComment
} from '@symbiota2/api-database';
import { Repository } from 'typeorm';
import { UpdateStatusDto } from './dto/update-status.dto';

@Injectable()
export class OccurrenceCommentService {
    constructor(@Inject(OccurrenceComment.PROVIDER_ID) private readonly comments: Repository<OccurrenceComment>) { }

    findByID(commentID: number): Promise<OccurrenceComment> {
        return this.comments.findOne({ where: {id: commentID }})
    }

    findByOccurrenceID(occurrenceID: number): Promise<OccurrenceComment[]> {
        return this.comments.find({ where: { occurrenceID }, relations: ['commenter', 'occurrence'] });
    }

    async findByCollectionID(collectionID: number): Promise<OccurrenceComment[]> {
        // const data = await this.comments.createQueryBuilder('c')
        //     .select()
        //     .innerJoin(Occurrence, 'o', 'c.occurrenceID = o.id')
        //     .innerJoin(Collection, 'coll', 'o.collectionID = coll.id')
        //     .where('coll.id = :collectionID', { collectionID })
        //     .getMany();

        return await this.comments.createQueryBuilder('c')
        .select()
        .innerJoinAndSelect(Occurrence, 'o', 'c.occurrenceID = o.id')
        .innerJoinAndSelect(Collection, 'coll', 'o.collectionID = coll.id')
        .leftJoinAndSelect('c.occurrence', 'occurrence')
        .leftJoinAndSelect('c.commenter', 'commenter')
        .where('coll.id = :collectionID', { collectionID })
        .getMany();
    }

    async add(occurrenceID: number, commenterUID: number, commentText: string): Promise<OccurrenceComment> {
        let comment = this.comments.create({ occurrenceID, uid: commenterUID, comment: commentText });
        comment = await this.comments.save(comment);
        comment.commenter = await comment.commenter;
        return comment;
    }

    async findCollectionIDForComment(commentID: number): Promise<number> {
        const comment = await this.comments.findOne({ where: {id: commentID }});
        if (!comment) {
            return null;
        }

        const qb = await this.comments.createQueryBuilder('c')
            .select('coll.id as collectionID')
            .innerJoin(Occurrence, 'o', 'c.occurrenceID = o.id')
            .innerJoin(Collection, 'coll', 'o.collectionID = coll.id')
            .where('c.id = :commentID', { commentID: comment.id })
            .getRawOne<{ collectionID: number }>();

        return qb.collectionID;
    }

    async deleteByID(id: number): Promise<boolean> {
        const results = await this.comments.delete({ id });
        return results.affected > 0;
    }

    async updateStatus(commentData: UpdateStatusDto): Promise<OccurrenceComment> {
        const comment = await this.comments.findOne({ where: {id: commentData.id }});
        // console.log('id: ', id, ' status: ', status)
        // console.log('comment : ', comment)
        comment.reviewStatus = commentData.status;
        return await this.comments.save(comment)

    }
}
