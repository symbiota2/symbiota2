import { ApiProperty } from '@nestjs/swagger';
import { IsIn, IsInt, IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class UpdateStatusDto {
    //@ApiProperty()
    @IsInt()
    @IsNotEmpty()
    id: number;

    //@ApiProperty()
    @IsIn([0, 1])
    @IsNotEmpty()
    status: number
}
