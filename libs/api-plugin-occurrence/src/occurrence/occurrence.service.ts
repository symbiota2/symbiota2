import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import {
    Occurrence,
    OccurrenceUpload,
    OccurrenceUploadFieldMap, TaxaEnumTreeEntry,
    Taxon,
    TaxonomicAuthority, TaxonomicStatus
} from '@symbiota2/api-database';
import { DeepPartial, EntityTarget, FindManyOptions, getConnection, Repository } from 'typeorm';
import { FindAllParams } from './dto/find-all-input.dto';
import {
    ApiCollectionListItem,
    ApiImageListItem,
    ApiOccurrence,
    ApiOccurrenceListItem,
    ApiTaxonSearchCriterion
} from '@symbiota2/data-access';
import { Geometry } from 'wkx';
import { InjectQueue } from '@nestjs/bull';
import { QUEUE_ID_OCCURRENCE_UPLOAD_CLEANUP } from '../queues/occurrence-upload-cleanup.queue';
import { Queue } from 'bull';
import { OccurrenceUploadCleanupJob } from '../queues/occurrence-upload-cleanup.processor';
import { QUEUE_ID_OCCURRENCE_UPLOAD } from '../queues/occurrence-upload.queue';
import { OccurrenceUploadJob } from '../queues/occurrence-upload.processor';
import { BaseService, csvIterator, withTempDir, zipFiles } from '@symbiota2/api-common';
import {
    TaxonomicAuthorityService,
    TaxonomicEnumTreeService,
    TaxonomicStatusService, TaxonomicUnitService,
    TaxonService
} from '@symbiota2/api-plugin-taxonomy';
import { TaxonFindAllParams } from '../../../api-plugin-taxonomy/src/taxon/dto/taxon-find-parms';
import { google } from '@google-cloud/translate/build/protos/protos';
import Any = google.protobuf.Any;
import { ReadStream } from 'typeorm/browser/platform/BrowserPlatformTools';
import { pipeline, Readable, Transform } from 'stream';
import fs, { createReadStream, createWriteStream, promises as fsPromises } from 'fs';
import { getKGEdge, getKGNode, getKGProperty, KnowledgeGraphBuilder } from '@symbiota2/knowledgeGraph';
import { DataFactory } from 'rdf-data-factory';
import { join as pathJoin } from 'path';
import { AppConfigService } from '@symbiota2/api-config';
import { tap } from 'rxjs/operators';
import * as jsonToXML from 'js2xmlparser';
import { RawSqlResultsToEntityTransformer } from 'typeorm/query-builder/transformer/RawSqlResultsToEntityTransformer';
import { OccurrenceList, OccurrenceListItem } from './dto/occurrence-list';
import { ColumnMetadata } from 'typeorm/metadata/ColumnMetadata';
import { RelationMetadata } from 'typeorm/metadata/RelationMetadata';
import { KGEdgeType } from '@symbiota2/api-knowledge-graph';
import { DwcArchiveBuilder } from '@symbiota2/dwc';
import { isDefined } from 'class-validator';
import { util } from 'prettier';

type _OccurrenceFindAllItem = Pick<Occurrence, 'id' | 'catalogNumber' | 'taxonID' | 'scientificNameComputed' | 'scientificName' |'latitude' | 'longitude' | 'dbpk' | 'basisOfRecord' | 'occurrenceGUID' | 'otherCatalogNumbers' | 'ownerInstitutionCode' |
'datasetID' | 'family' | 'genus' | 'specificEpithet' | 'taxonRank' | 'infraspecificEpithet' | 'taxonRemarks' |
'identifiedBy' | 'dateIdentified' | 'identificationReferences' | 'identificationRemarks' | 'identificationQualifier' |
'typeStatus' | 'recordedByNames' | 'recordNumber' | 'recordedByID' | 'collectionID' |
'associatedCollectors' | 'latestDateCollected' | 'startDayOfYear' | 'endDayOfYear' | 'verbatimEventDate' |
'habitat' | 'substrate' | 'fieldNotes' | 'fieldNumber' | 'eventID' | 'occurrenceRemarks' | 'informationWithheld' |
'dataGeneralizations' | 'associatedOccurrences' | 'associatedTaxa' | 'dynamicProperties' | 'verbatimAttributes' |
'behavior' | 'reproductiveCondition' | 'cultivationStatus' | 'establishmentMeans' | 'lifeStage' | 'sex' |
'individualCount' | 'samplingProtocol' | 'samplingEffort' | 'preparations' | 'locationID' | 'country' |
'stateProvince' |'county' | 'municipality' | 'waterBody' | 'locality' | 'localitySecurity' | 'localitySecurityReason' |
'geodeticDatum' | 'coordinateUncertaintyInMeters' | 'footprintWKT' | 'coordinatePrecision' | 'locationRemarks' |
'verbatimCoordinates' | 'verbatimCoordinateSystem' | 'georeferenceProtocol' | 'degreeOfEstablishment' | 'associatedSequences' |
'earliestAgeOrLowestStage' | 'earliestEonOrLowestEonothem' | 'earliestEpochOrLowestSeries' | 'earliestEraOrLowestErathem' |
'earliestPeriodOrLowestSystem' | 'eventRemarks' | 'eventTime' | 'measurementRemarks' | 'parentEventID' | 'associatedOrganisms' |
'associatedReferences' | 'countryCode' | 'datasetName' | 'subgenus' | 'georeferenceSources' | 'georeferenceVerificationStatus' |
'georeferenceRemarks' | 'minimumElevationInMeters' | 'maximumElevationInMeters' | 'verbatimElevation' | 'minimumDepthInMeters' |
'maximumDepthInMeters' | 'verbatimDepth' | 'previousIdentifications' | 'disposition' | 'storageLocation' | 'genericColumn1' |
'genericColumn2' | 'modified' | 'language' | 'observerUID' | 'processingStatus' | 'recordEnteredBy' | 'duplicateQuantity' |
'labelProject' | 'dynamicFields' | 'initialTimestamp' | 'lastModifiedTimestamp' >;
type OccurrenceFindAllItem = _OccurrenceFindAllItem & { collection: ApiCollectionListItem };

type OccurrenceFindAllList = {
    count: number;
    data: OccurrenceFindAllItem[];
}

/**
 * Service for retrieving occurrence records from the database
 */
@Injectable()
export class OccurrenceService {
    private static readonly DWCA_CREATE_LIMIT = 1024;

    constructor(
        @Inject(Occurrence.PROVIDER_ID)
        private readonly occurrenceRepo: Repository<Occurrence>,
        @Inject(OccurrenceUpload.PROVIDER_ID)
        private readonly uploadRepo: Repository<OccurrenceUpload>,
        @Inject(Taxon.PROVIDER_ID)
        private readonly taxonRepository: Repository<Taxon>,
        @Inject(TaxonService)
        private readonly taxonService: TaxonService,
        @Inject(TaxonomicUnitService)
        private readonly taxonomicUnitService: TaxonomicUnitService,
        @Inject(AppConfigService)
        private readonly appConfig: AppConfigService,
        @Inject(TaxonomicAuthorityService)
        private readonly taxonomicAuthorityService: TaxonomicAuthorityService,
        @Inject(TaxonomicStatusService)
        private readonly taxonomicStatusService: TaxonomicStatusService,
        @InjectQueue(QUEUE_ID_OCCURRENCE_UPLOAD_CLEANUP)
        private readonly uploadCleanupQueue: Queue<OccurrenceUploadCleanupJob>,
        @InjectQueue(QUEUE_ID_OCCURRENCE_UPLOAD)
        private readonly uploadQueue: Queue<OccurrenceUploadJob>) { }

    /** For Download
     * Retrieves a list of occurrence records
     * @param findAllOpts Filter options for the query
     * @return OccurrenceFindAllList A subset of the list of occurrences determined
     * by limit and offset, and a count of all occurrences matching the query
     */
     async downloadAll(findAllOpts: FindAllParams): Promise<OccurrenceFindAllList> {
        const { limit, offset, ...params } = findAllOpts;
        let qb = this.occurrenceRepo.createQueryBuilder('o')
            .innerJoin('o.collection', 'c')
            .limit(limit)
            .offset(offset);

        if (findAllOpts.collectionID) {
            if (Array.isArray(findAllOpts.collectionID)) {
                qb = qb.where('o.collectionID IN (:...collectionID)', { collectionID: params.collectionID });
            }
            else {
                qb = qb.where('o.collectionID = :collectionID', { collectionID: params.collectionID });
            }
        }

        if (findAllOpts.taxonSearchCriterion && findAllOpts.taxonSearchStr) {
            const searchStr = `${ findAllOpts.taxonSearchStr }%`;
            switch (findAllOpts.taxonSearchCriterion) {
                case ApiTaxonSearchCriterion.taxonID:
                    qb.andWhere(
                        'o.taxonID = :taxonID',
                        {taxonID: searchStr}
                    )
                    break;
                case ApiTaxonSearchCriterion.familyOrSciName:
                    // Get synonyms if needed
                    const allNames = [searchStr]
                    if (findAllOpts.includeSynonyms) {
                        // @ts-ignore
                        const authority = new TaxonomicAuthority()
                        const taxonomicAuthorityID = await authority.getDefaultAuthorityID()

                        // @ts-ignore
                        const taxonParams : TaxonFindAllParams = {scientificName: searchStr, taxonAuthorityID: taxonomicAuthorityID}
                        const taxons : Taxon[] = await this.taxonService.findAll(taxonParams)
                        for (let taxon of taxons) {
                            const syns : TaxonomicStatus[] = await this.taxonomicStatusService.findSynonyms(taxon.id,taxonParams)
                                // .findByTIDWithSynonyms(taxon.id)
                            for (let syn of syns) {
                                const taxon = await syn.taxon
                                allNames.push(taxon.scientificName)
                            }
                        }
                        for (let name of allNames) {
                            qb.andWhere(
                                '(o.scientificName LIKE :sciNameOrFamily or o.family LIKE :sciNameOrFamily)',
                                { sciNameOrFamily: name }
                            )
                        }
                    } else {
                        qb.andWhere(
                            '(o.scientificName LIKE :sciNameOrFamily or o.family LIKE :sciNameOrFamily)',
                            { sciNameOrFamily: searchStr }
                        )
                    }
                    break;
                case ApiTaxonSearchCriterion.scientificName:
                    qb.andWhere(
                        'o.scientificNameComputed LIKE :scientificName',
                        { scientificName: searchStr }
                    );
                    break;
                case ApiTaxonSearchCriterion.family:
                    qb.andWhere(
                        'o.family LIKE :family',
                        { family: searchStr }
                    );
                    break;
                case ApiTaxonSearchCriterion.higherTaxonomy:
                    const authority = new TaxonomicAuthority()
                    const taxonomicAuthorityID : number = await authority.getDefaultAuthorityID()
                    const familyNames : String[] = await this.taxonService.findDescendantsNames(searchStr, 'Family', taxonomicAuthorityID)
                    if (familyNames == null || familyNames.length == 0) {
                        qb.andWhere('false')
                    } else {
                        qb.andWhere("family IN (:...names)", {names: familyNames})
                    }
                    //console.log("names " + familyNames.length)
                    /*
                    for (let name of familyNames) {
                        console.log("name is " + name.scientificName)
                        qb.andWhere(
                            '(o.family = :sciNameOrFamily)',
                            { sciNameOrFamily: name }
                        )
                    }

                     */
                    break;
                case ApiTaxonSearchCriterion.commonName:
                    // TODO: Taxon plugin returns common names for a taxon?
                    break;
                default:
                    break;
            }
        }

        if (findAllOpts.minimumElevationInMeters !== undefined) {
            qb.andWhere(
                'o.minimumElevationInMeters = :minElevation',
                { minElevation: findAllOpts.minimumElevationInMeters }
            );
        }

        if (findAllOpts.maximumElevationInMeters !== undefined) {
            qb.andWhere(
                'o.maximumElevationInMeters = :maxElevation',
                { maxElevation: findAllOpts.maximumElevationInMeters }
            );
        }

        if (findAllOpts.minLatitude !== undefined) {
            qb.andWhere(
                'o.latitude >= :minLat',
                { minLat: findAllOpts.minLatitude }
            )
        }

        if (findAllOpts.minLongitude !== undefined) {
            qb.andWhere(
                'o.longitude >= :minLon',
                { minLon: findAllOpts.minLongitude }
            )
        }

        if (findAllOpts.maxLatitude !== undefined) {
            qb.andWhere(
                'o.latitude <= :maxLat',
                { maxLat: findAllOpts.maxLatitude }
            )
        }

        if (findAllOpts.maxLongitude !== undefined) {
            qb.andWhere(
                'o.longitude <= :maxLon',
                { maxLon: findAllOpts.maxLongitude }
            )
        }

        const remainingLocalityKeys = [
            'country',
            'county',
            'locality',
            'stateProvince',
        ];

        for (const searchKey of remainingLocalityKeys) {
            if (findAllOpts[searchKey]) {
                qb.andWhere(`o.${searchKey} IS NOT NULL`);
                qb.andWhere(
                    `o.${ searchKey } LIKE :searchStr`,
                    { searchStr: `${ findAllOpts[searchKey] }%` }
                );
            }
        }

        if (findAllOpts.collectorLastName) {
            qb.andWhere('o.recordedBy IS NOT NULL')
            qb.andWhere(
                `o.recordedBy LIKE :collectorLastName`,
                { collectorLastName: `%${findAllOpts.collectorLastName}%` }
            )
        }

        if (findAllOpts.minEventDate) {
            qb.andWhere(`o.eventDate IS NOT NULL`)
            qb.andWhere(
                `o.eventDate >= :minEventDate`,
                { minEventDate: findAllOpts.minEventDate }
            );
        }

        if (findAllOpts.maxEventDate) {
            qb.andWhere(`o.eventDate IS NOT NULL`)
            qb.andWhere(
                `o.eventDate <= :maxEventDate`,
                { maxEventDate: findAllOpts.maxEventDate }
            );
        }

        if (findAllOpts.catalogNumber) {
            qb.andWhere(`o.catalogNumber IS NOT NULL`)
            qb.andWhere(
                `o.catalogNumber LIKE :catalogNumber`,
                { catalogNumber: `${findAllOpts.catalogNumber}%` }
            )
        }

        if (findAllOpts.limitToSpecimens === true) {
            qb.andWhere('o.basisOfRecord = "PreservedSpecimen"');
        }

        if (findAllOpts.limitToGenetic === true) {
            qb.andWhere('(select count(*) from omoccurgenetic g where g.occid = o.id) > 0');
        }

        if (findAllOpts.limitToImages === true) {
            qb.andWhere('(select count(*) from images i where i.occid = o.id) > 0');
        }

        if (findAllOpts.geoJSON) {
            let poly;
            const geojsonStr = Buffer.from(findAllOpts.geoJSON, 'base64');

            try {
                const geojson = JSON.parse(geojsonStr.toString('utf-8'));
                poly = Geometry.parseGeoJSON(geojson).toWkt();
            }
            catch (e) {
                throw new BadRequestException(`Invalid GeoJSON: ${e.toString()}`);
            }

            const searchPolyAsWKB = `PolyFromText('${poly}')`;
            const occurrenceAsPoint = "Point(o.longitude, o.latitude)";

            qb = qb.andWhere(`ST_CONTAINS(${searchPolyAsWKB}, ${occurrenceAsPoint})`);
        }

        // TODO: Make this a query param
        //qb.addOrderBy('o.id', 'ASC');

        const [data, count] = await qb.getManyAndCount();

        return {
            count,
            data: await Promise.all(
                data.map(async (occurrence) => {
                    const { collection, ...props } = occurrence;
                    return {
                        ...props,
                        collection: await collection,
                    }
                })
            )
        };
    }

    private async createArchive(archiveName: string) {

        const dataDir = await this.appConfig.dataDir();
        return new Promise<void>((resolve, reject) => {
            withTempDir(dataDir, async (tmpDir) => {
                try {
                    const archivePath = pathJoin(tmpDir, archiveName);

                    resolve();

                } catch (e) {
                    reject(e);
                }
            });
        });
    }

    /** For Download
     * Retrieves a list of occurrence records
     * @param findAllOpts Filter options for the query
     * @return OccurrenceFindAllList A subset of the list of occurrences determined
     * by limit and offset, and a count of all occurrences matching the query
     */
    async downloadAllForFileWrite(findAllOpts: FindAllParams): Promise<string> {
        findAllOpts.limit = 50000
        const { limit, offset, ...params } = findAllOpts
        // Get the data directory location
        const dataDir = await this.appConfig.dataDir()
        const archiveName = "occurrences"

        // Fetch current taxonomic authority
        const taxonomicAuthorityID = await this.taxonomicAuthorityService.findDefault()
        // Get all the units for this authority, ignore the limit, set to an arbitraty value
        const taxonUnits = await this.taxonomicUnitService.findAll({limit: 1000000, offset: 0, id: null})

        //console.log(" taxon units " + taxonUnits.length)
        // Need to determine the rank ids for various ranks
        let orderRankID = 0
        let phylumRankID = 0
        let classRankID = 0
        // This ignores the idea that each kingdom has separate rankID values, otherwise, need to
        // do the entire query by kingdom or include the taxon units table in the joins below
        taxonUnits.forEach((unit) =>  {
            //console.log("name " + unit.rankName)
            if (unit.rankName.toLowerCase() == 'order') {
                orderRankID = unit.rankID
            }
            if (unit.rankName.toLowerCase() == 'class') {
                classRankID = unit.rankID
            }
            if (unit.rankName.toLowerCase() == 'phylum') {
                phylumRankID = unit.rankID
            }
        })
        // console.log("values " + taxonomicAuthorityID + " " + classRankID + " " + orderRankID + " " + phylumRankID)
        let qb = this.occurrenceRepo.createQueryBuilder('o')
            .innerJoin('o.collection', 'c')
            .leftJoin('o.taxon', 't')
            //.leftJoin(TaxaEnumTreeEntry, 'tek', 't.id = tek.taxonID AND tek.taxonAuthorityID = 1')
            //.leftJoin('tek.parentTaxon', 'tk')
            //.where("tk.rankID IN (:...ranks)", {ranks: [10,30,60,100]})
            //.leftJoin(TaxaEnumTreeEntry, 'tek', 't.id = tek.taxonID AND tek.taxonAuthorityID = 1')
            //.innerJoin('tek.parentTaxon', 'tk', 'tk.rankID = 10')
            .leftJoin(TaxaEnumTreeEntry, 'tec', 't.id = tec.taxonID AND tec.taxonAuthorityID = :authID', {authID: taxonomicAuthorityID.valueOf()})
            .innerJoin('tec.parentTaxon', 'tc', 'tc.rankID = :rankID', {rankID: classRankID})
            .leftJoin(TaxaEnumTreeEntry, 'teo', 't.id = teo.taxonID AND teo.taxonAuthorityID = :authID', {authID: taxonomicAuthorityID.valueOf()})
            .innerJoin('teo.parentTaxon', 'to', 'to.rankID = :rankID', {rankID: orderRankID})
            .leftJoin(TaxaEnumTreeEntry, 'tep', 't.id = tep.taxonID AND tep.taxonAuthorityID = :authID', {authID: taxonomicAuthorityID.valueOf()})
            .innerJoin('tep.parentTaxon', 'tp', 'tp.rankID = :rankID', {rankID: phylumRankID})
            .limit(limit)
            //.offset(offset)
            //.select('o.id as foo')
            .select([
                    'o.id as id',
                    'c.institutionCode as institutionCode',
                    'c.collectionCode as collectionCode',
                    'o.ownerInstitutionCode as ownerInstitutionCode',
                    'o.basisOfRecord as basisOfRecord',
                    'o.occurrenceGUID as occurrenceGUID',
                    'o.catalogNumber as catalogNumber',
                    'o.otherCatalogNumbers as otherCatalogNumbers',
                    't.kingdomName as kingdom',
                    'tp.scientificName as phylum',
                    'tc.scientificName as class',
                    'to.scientificName as orderName',
                    'o.family as family',
                    'o.day as day',
                    'o.month as month',
                    'o.year as year',
                    'o.scientificName as scientificName',
                    'o.scientificNameComputed as scientificNameComputed',
                    'o.taxonID as taxonID',
                    't.author as scientificNameAuthor',
                    'o.genus as genus',
                    'o.specificEpithet as specificEpithet',
                    'o.taxonRank as taxonRank',
                    'o.infraspecificEpithet as infraspecificEpithet',
                    'o.identifiedBy as identifiedBy',
                    'o.dateIdentified as dateIdentified',
                    'o.identificationReferences as identificationReferences',
                    'o.identificationRemarks as identificationRemarks',
                    'o.taxonRemarks as taxonRemarks',
                    'o.identificationQualifier as identificationQualifier',
                    'o.typeStatus as typeStatus',
                    'o.recordedBy as recordedBy',
                    'o.recordedByID as recordedByID',
                    'o.associatedCollectors as associatedCollectors',
                    'o.recordNumber as recordNumber',
                    'o.eventDate as eventDate',
                    'o.startDayOfYear as startDayOfYear',
                    'o.endDayOfYear as endDayOfYear',
                    'o.verbatimEventDate as verbatimEventDate',
                    'o.occurrenceRemarks as occurrenceRemarks',
                    'o.habitat as habitat',
                    'o.substrate as substrate',
                    'o.verbatimAttributes as verbatimAttributes',
                    'o.fieldNumber as fieldNumber',
                    'o.informationWithheld as informationWithheld',
                    'o.dataGeneralizations as dataGeneralizations',
                    'o.dynamicProperties as dynamicProperties',
                    'o.associatedTaxa as associatedTaxa',
                    'o.reproductiveCondition as reproductiveCondition',
                    'o.establishmentMeans as establishmentMeans',
                    'o.cultivationStatus as cultivationStatus',
                    'o.lifeStage as lifeStorage',
                    'o.sex as sex',
                    'o.individualCount as individualCount',
                    'o.samplingProtocol as samplingProtocol',
                    'o.samplingEffort as samplingEffort',
                    'o.preparations as preparations',
                    'o.country as country',
                    'o.stateProvince as stateProvince',
                    'o.county as county',
                    'o.municipality as municipality',
                    'o.locality as locality',
                    'o.locationRemarks as locationRemarks',
                    'o.localitySecurity as localitySecurity',
                    'o.localitySecurityReason as localitySecurityReason',
                    'o.latitude as decimalLatitude',
                    'o.longitude as decimalLongitude',
                    'o.geodeticDatum as geodeticDatum',
                    'o.coordinateUncertaintyInMeters as coordinateUncertaintyInMeters',
                    'o.verbatimCoordinates as verbatimCoordinates',
                    'o.geoReferencedBy as geoReferencedBy',
                    'o.georeferenceProtocol as georeferenceProtocol',
                    'o.georeferenceSources as georeferenceSources',
                    'o.georeferenceVerificationStatus as georeferenceVerificationStatus',
                    'o.georeferenceRemarks as georeferenceRemarks',
                    'o.minimumElevationInMeters as minimumElevationInMeters',
                    'o.maximumElevationInMeters as maximumElevationInMeters',
                    'o.minimumDepthInMeters as minimumDepthInMeters',
                    'o.maximumDepthInMeters as maximumDepthInMeters',
                    'o.verbatimDepth as verbatimDepth',
                    'o.verbatimElevation as verbatimElevation',
                    'o.disposition as disposition',
                    'o.language as language',
                    'o.recordEnteredBy as recordEnteredBy',
                    'o.modified as modified',
                    'o.dbpk as sourcePrimaryKey',
                    'o.collectionID as collId'
                    //'o.recordId',
                    //'o.references'
                ]
            )
            //.orderBy('o.id');

        if (findAllOpts.collectionID) {
            if (Array.isArray(findAllOpts.collectionID)) {
                qb = qb.where('o.collectionID IN (:...collectionID)', { collectionID: params.collectionID });
            }
            else {
                qb = qb.where('o.collectionID = :collectionID', { collectionID: params.collectionID });
            }
        }

        if (findAllOpts.taxonSearchCriterion && findAllOpts.taxonSearchStr) {
            const searchStr = `${ findAllOpts.taxonSearchStr }%`;
            switch (findAllOpts.taxonSearchCriterion) {
                case ApiTaxonSearchCriterion.taxonID:
                    qb.andWhere(
                        'o.taxonID = :taxonID',
                        {taxonID: searchStr}
                    )
                    break;
                case ApiTaxonSearchCriterion.familyOrSciName:
                    // Get synonyms if needed
                    const allNames = [searchStr]
                    if (findAllOpts.includeSynonyms) {
                        // @ts-ignore
                        const authority = new TaxonomicAuthority()
                        const taxonomicAuthorityID = await authority.getDefaultAuthorityID()

                        // @ts-ignore
                        const taxonParams : TaxonFindAllParams = {scientificName: searchStr, taxonAuthorityID: taxonomicAuthorityID}
                        const taxons : Taxon[] = await this.taxonService.findAll(taxonParams)
                        for (let taxon of taxons) {
                            const syns : TaxonomicStatus[] = await this.taxonomicStatusService.findSynonyms(taxon.id,taxonParams)
                            // .findByTIDWithSynonyms(taxon.id)
                            for (let syn of syns) {
                                const taxon = await syn.taxon
                                allNames.push(taxon.scientificName)
                            }
                        }
                        for (let name of allNames) {
                            qb.andWhere(
                                '(o.scientificName LIKE :sciNameOrFamily or o.family LIKE :sciNameOrFamily)',
                                { sciNameOrFamily: name }
                            )
                        }
                    } else {
                        qb.andWhere(
                            '(o.scientificName LIKE :sciNameOrFamily or o.family LIKE :sciNameOrFamily)',
                            { sciNameOrFamily: searchStr }
                        )
                    }
                    break;
                case ApiTaxonSearchCriterion.scientificName:
                    qb.andWhere(
                        'o.scientificNameComputed LIKE :scientificName',
                        { scientificName: searchStr }
                    );
                    break;
                case ApiTaxonSearchCriterion.family:
                    qb.andWhere(
                        'o.family LIKE :family',
                        { family: searchStr }
                    );
                    break;
                case ApiTaxonSearchCriterion.higherTaxonomy:
                    const authority = new TaxonomicAuthority()
                    const taxonomicAuthorityID : number = await authority.getDefaultAuthorityID()
                    const familyNames : String[] = await this.taxonService.findDescendantsNames(searchStr, 'Family', taxonomicAuthorityID)
                    if (familyNames == null || familyNames.length == 0) {
                        qb.andWhere('false')
                    } else {
                        qb.andWhere("family IN (:...names)", {names: familyNames})
                    }
                    break;
                case ApiTaxonSearchCriterion.commonName:
                    // TODO: Taxon plugin returns common names for a taxon?
                    break;
                default:
                    break;
            }
        }

        if (findAllOpts.minimumElevationInMeters !== undefined) {
            qb.andWhere(
                'o.minimumElevationInMeters = :minElevation',
                { minElevation: findAllOpts.minimumElevationInMeters }
            );
        }

        if (findAllOpts.maximumElevationInMeters !== undefined) {
            qb.andWhere(
                'o.maximumElevationInMeters = :maxElevation',
                { maxElevation: findAllOpts.maximumElevationInMeters }
            );
        }

        if (findAllOpts.minLatitude !== undefined) {
            qb.andWhere(
                'o.latitude >= :minLat',
                { minLat: findAllOpts.minLatitude }
            )
        }

        if (findAllOpts.minLongitude !== undefined) {
            qb.andWhere(
                'o.longitude >= :minLon',
                { minLon: findAllOpts.minLongitude }
            )
        }

        if (findAllOpts.maxLatitude !== undefined) {
            qb.andWhere(
                'o.latitude <= :maxLat',
                { maxLat: findAllOpts.maxLatitude }
            )
        }

        if (findAllOpts.maxLongitude !== undefined) {
            qb.andWhere(
                'o.longitude <= :maxLon',
                { maxLon: findAllOpts.maxLongitude }
            )
        }

        const remainingLocalityKeys = [
            'country',
            'county',
            'locality',
            'stateProvince',
        ];

        for (const searchKey of remainingLocalityKeys) {
            if (findAllOpts[searchKey]) {
                qb.andWhere(`o.${searchKey} IS NOT NULL`);
                qb.andWhere(
                    `o.${ searchKey } LIKE :searchStr`,
                    { searchStr: `${ findAllOpts[searchKey] }%` }
                );
            }
        }

        if (findAllOpts.collectorLastName) {
            qb.andWhere('o.recordedBy IS NOT NULL')
            qb.andWhere(
                `o.recordedBy LIKE :collectorLastName`,
                { collectorLastName: `%${findAllOpts.collectorLastName}%` }
            )
        }

        if (findAllOpts.minEventDate) {
            qb.andWhere(`o.eventDate IS NOT NULL`)
            qb.andWhere(
                `o.eventDate >= :minEventDate`,
                { minEventDate: findAllOpts.minEventDate }
            );
        }

        if (findAllOpts.maxEventDate) {
            qb.andWhere(`o.eventDate IS NOT NULL`)
            qb.andWhere(
                `o.eventDate <= :maxEventDate`,
                { maxEventDate: findAllOpts.maxEventDate }
            );
        }

        if (findAllOpts.catalogNumber) {
            qb.andWhere(`o.catalogNumber IS NOT NULL`)
            qb.andWhere(
                `o.catalogNumber LIKE :catalogNumber`,
                { catalogNumber: `${findAllOpts.catalogNumber}%` }
            )
        }

        if (findAllOpts.limitToSpecimens === true) {
            qb.andWhere('o.basisOfRecord = "PreservedSpecimen"');
        }

        if (findAllOpts.limitToGenetic === true) {
            qb.andWhere('(select count(*) from omoccurgenetic g where g.occid = o.id) > 0');
        }

        if (findAllOpts.limitToImages === true) {
            qb.andWhere('(select count(*) from images i where i.occid = o.id) > 0');
        }

        if (findAllOpts.geoJSON) {
            let poly;
            const geojsonStr = Buffer.from(findAllOpts.geoJSON, 'base64');

            try {
                const geojson = JSON.parse(geojsonStr.toString('utf-8'));
                poly = Geometry.parseGeoJSON(geojson).toWkt();
            }
            catch (e) {
                throw new BadRequestException(`Invalid GeoJSON: ${e.toString()}`);
            }

            const searchPolyAsWKB = `PolyFromText('${poly}')`;
            const occurrenceAsPoint = "Point(o.longitude, o.latitude)";

            qb = qb.andWhere(`ST_CONTAINS(${searchPolyAsWKB}, ${occurrenceAsPoint})`);
        }

        qb = qb.andWhere('o.taxonID IS NOT NULL')
        //const list = await getConnection().getMetadata(Occurrence).ownColumns.map(column => [column.propertyName, column.databaseName] as const)
        //const map = new Map<string, string>()
        //list.forEach((m) => {map.set("o_" + m[1],m[0])})

        try {
            const tmpDir = await fsPromises.mkdtemp(pathJoin(dataDir, 'tmp-'))
            const archivePath = tmpDir
            var fileStream = require("fs");
            const csvFileWriteStream = fileStream.createWriteStream(pathJoin(archivePath,"occurrences.csv"))
            const xmlFileWriteStream = fileStream.createWriteStream(pathJoin(archivePath,"occurrences.xml"))
            const queryStream  = await qb.stream()
            let gotHeader = false
            let headers = null
            queryStream.on("data", chunk => {
                //let values = Object.values(JSON.parse(JSON.stringify(chunk)));
                //console.log("values are " + values)
                const result = JSON.parse(JSON.stringify(chunk))
                if (!gotHeader) {
                    gotHeader = true
                    // get the header
                    headers = Object.keys(result)
                    csvFileWriteStream.write(headers.join(',') + "\n")
                }
                const row = this.transformRow(result)
                //const row = this.processRow(result)
                const csv = this.rowToCSV(headers, row)
                const xml = this.rowToXML(headers, row)

                csvFileWriteStream.write(csv)
                xmlFileWriteStream.write(xml)
                //fileWriteStream.write(this.objectToCSV(this.processData(results)))
                //console.log(`streaming: ${JSON.stringify(result)}`);
                //console.log('occurrence ' + result.id)
            })
            queryStream.on('error', (err) => {
                console.log("Error in query " + err);
            })
            queryStream.on('end', () => {
                console.log("Closing file, all done ")
                csvFileWriteStream.close()
                xmlFileWriteStream.close()
                zipFiles(pathJoin(archivePath,"occurrences.zip"),
                    [pathJoin(archivePath,"occurrences.csv"),
                        pathJoin(archivePath,"occurrences.xml")])
            })
            // Wait for the end of the read pipe, waking up every second
            while (queryStream.readable===true) {
                await new Promise(f => setTimeout(f, 1000))
            }
            //const finished = util. .promisify(queryStream.finished)
            /*
            for await (const chunk of queryStream) {
                const result = JSON.parse(JSON.stringify(chunk))
                if (!gotHeader) {
                    gotHeader = true
                    // get the header
                    headers = Object.keys(result)
                    csvFileWriteStream.write(headers.join(',') + "\n")
                }
                const row = this.transformRow(result)
                //const row = this.processRow(result)
                const csv = this.rowToCSV(headers, row)
                const xml = this.rowToXML(headers, row)
            }

             */
            console.log("done with read")
            return pathJoin(archivePath,"occurrences.zip")

        } catch (e) {
            console.log("Error opening file for writing occurrence records!" + e)
        }

        return ""
    }

    async createOccurrenceDownload(name: string): Promise<string> {
        /*
        // Map of graph nodes
        const nodeMap : Map<string,KGGraphNode> = new Map()

        // Get its tags
        const tags = {
            graphName: graphName,
            public: opts.publish.toString()
        }

        // Look at the metadata to build up a list of nodes, edges, and properties to process
        const db = getConnection()
        db.entityMetadatas.forEach((entityMeta) => {
            const nodeType = getKGNode(entityMeta.target)

            if (nodeType) {
                // This type has been decorated as a KG node
                // Get a list of its columns
                const columns : ColumnMetadata[] = entityMeta.columns

                // Process the object associated with the node type
                // Is the current graph in this type?
                let nodeValue = null
                for (let i = 0; i < nodeType.length; i++) {
                    if (nodeType[i].graph == graphName) {
                        // This node is in the graph being built
                        nodeValue = nodeType[i].url
                    }
                }

                // We can skip the rest if the node isn't in the graph
                if (nodeValue == null) {
                    return
                }

                // Set up the mapping
                const propertyMapValues = new Map<string, string>()
                const edgeMapValues = new Map<string, KGEdgeType>()
                const mapValue = {
                    url: nodeValue,
                    meta: entityMeta,
                    edges: edgeMapValues,
                    properties: propertyMapValues,
                    keys: []
                }
                nodeMap.set(entityMeta.targetName, mapValue)

                // Now let's deal with the properties
                const propertyMap = entityMeta.propertiesMap
                const ids = entityMeta.primaryColumns
                // console.log(" keys key is " + entityMeta.name)
                const resolvedKeys = []
                // Get the names of the columns for the primary key
                // eslint-disable-next-line prefer-const
                for (let key in ids) {
                    // console.log(" key " + propertyMap)
                    resolvedKeys.push(columns[key].propertyName)
                }
                mapValue.keys = resolvedKeys

                // Run through the properties
                // eslint-disable-next-line prefer-const
                for (let key in propertyMap) {
                    const propertyType = getKGProperty(entityMeta.target, key)
                    if (propertyType) {

                        // Let's check to see if property is in graph
                        let propertyUrl = null
                        for (let i = 0; i < propertyType.length; i++) {
                            if (propertyType[i].graph == graphName) {
                                propertyUrl = propertyType[i].url
                            }
                        }
                        if (propertyUrl != null) {
                            propertyMapValues.set(key,propertyUrl)
                        }
                    }
                }

                const linkMap = new Map();
                // eslint-disable-next-line prefer-const
                for (let index in entityMeta.relations) {
                    const relationMeta : RelationMetadata = entityMeta.relations[index]
                    linkMap.set(relationMeta.propertyName, relationMeta)
                }

                // process the edges
                // eslint-disable-next-line prefer-const
                for (let key in propertyMap) {
                    const edgeType = getKGEdge(entityMeta.target, key)
                    if (edgeType) {
                        // console.log(" Edge " + key)
                        let edgeUrl = null
                        for (const index in edgeType) {
                            // console.log("KG Edge " + index + " " + edgeType[index] + " ")
                            // Let's check to see if edge is in graph
                            if (edgeType[index].graph == graphName) {
                                edgeUrl = edgeType[index].url
                            }
                        }
                        if (edgeUrl != null) {
                            console.log("KG Edge setting " + key + " " + edgeUrl)
                            const relationMeta = linkMap.get(key)
                            edgeMapValues.set(key,
                                {
                                    url: edgeUrl,
                                    name: relationMeta.inverseEntityMetadata.target.name,
                                    keys: relationMeta.inverseEntityMetadata.primaryColumns
                                })
                        }
                    }
                }

            }
        })

        this.logger.log("Creating knowledge graph ")

        await this.createGraph(
            graphName,
            nodeMap,
            tags
        )
    */
        return name
    }

    transformRow(row) {
        //const value = JSON.stringify(row)
        return row
    }

    /*
    processRow(row : Occurrence) {
        const download = {
            id: row.id,
            institutionCode: row.institutionCode,
            collectionCode: row.collectionCode,
            ownerInstitutionCode: row.ownerInstitutionCode,
            basisOfRecord: row.basisOfRecord,
            occurrenceID: row.occurrenceGUID,
            catalogNumber: row.catalogNumber,
            otherCatalogNumber: row.otherCatalogNumbers,
            kingdom: row.kingdomName,
            phylum: row.phylumName,
            class: row.className,
            order: row.orderName,
            family: row.family,
            day: row.day,
            month: row.month,
            year: row.year,
            scientificName: row.scientificName,
            scientificNameComputed: row.scientificNameComputed,
            taxonID: row.taxonID,
            scientificNameAuthorship: row.scientificNameAuthorship,
            genus: row.genus,
            specificEpithet: row.specificEpithet,
            taxonRank: row.taxonRank,
            intraspecificEpithet: row.infraspecificEpithet,
            identifiedBy: row.identifiedBy,
            dateIdentified: row.dateIdentified,
            identificationReferences: row.identificationReferences,
            identificationRemarks: row.identificationRemarks,
            taxonRemarks: row.taxonRemarks,
            identificationQualifier: row.identificationQualifier,
            typeStatus: row.typeStatus,
            recordedBy: row.recordedBy,
            recordedByID: row.recordedByID,
            associatedCollectors: row.associatedCollectors,
            recordNumber: row.recordNumber,
            eventDate: row.eventDate,
            startDayOfYear: row.startDayOfYear,
            endDayOfYear: row.endDayOfYear,
            verbatimEventDate: row.verbatimEventDate,
            occurrenceRemarks: row.occurrenceRemarks,
            habitat: row.habitat,
            substrate: row.substrate,
            verbatimAttributes: row.verbatimAttributes,
            fieldNumber: row.fieldNumber,
            informationWithheld: row.informationWithheld,
            dataGeneralizations: row.dataGeneralizations,
            dynamicProperties: row.dynamicProperties,
            associatedTaxa: row.associatedTaxa,
            reproductiveCondition: row.reproductiveCondition,
            establishmentMeans: row.establishmentMeans,
            cultivationStatus: row.cultivationStatus,
            lifeState: row.lifeStage,
            sex: row.sex,
            individualCount: row.individualCount,
            samplingProtocol: row.samplingProtocol,
            samplingEffort: row.samplingEffort,
            preparations: row.preparations,
            country: row.country,
            stateProvince: row.stateProvince,
            county: row.county,
            municipality:row.municipality,
            locality: row.locality,
            locationRemarks: row.locationRemarks,
            localitySecurity: row.localitySecurity,
            localitySecurityReason: row.localitySecurityReason,
            decimalLatitude: row.latitude,
            decimalLongiitude: row.longitude,
            geodeticDatum: row.geodeticDatum,
            coordinateUncertaintyInMeters: row.coordinateUncertaintyInMeters,
            verbatimCoordinates: row.verbatimCoordinates,
            georeferencedBy: row.georeferencedBy,
            georeferenceProtocol: row.georeferenceProtocol,
            georeferenceSources: row.georeferenceSources,
            georeferenceVerificationStatus: row.georeferenceVerificationStatus,
            georeferenceRemarks: row.georeferenceRemarks,
            minimumElevationInMeters: row.minimumElevationInMeters,
            maximumElevationInMeters: row.maximumElevationInMeters,
            minimumDepthInMeters: row.minimumDepthInMeters,
            maximumDepthInMeters: row.maximumDepthInMeters,
            verbatimDepth: row.verbatimDepth,
            verbatimElevation: row.verbatimElevation,
            disposition: row.disposition,
            language: row.language,
            recordEnteredBy: row.recordEnteredBy,
            modified: row.modified,
            soucePrimaryKey: row.dbpk,
            collId: row.collectionID,
            //recordId: row.recordId,
            //references: row.references,
        }

        //console.log("phylum " + row.taxonID + download.phylum)
        //console.log("family " + download.family)
        //console.log("class " + download.class)
        //console.log("kingdom " + download.kingdom)
        //console.log("order " + download.order)
        return download
    }

     */

    processData(occurrences : OccurrenceList) {
        const { data } = occurrences;
        const download = data.map(row => ({
            id: row.id,
            institutionCode: row.collection.institutionCode,
            collectionCode: row.collection.collectionCode,
            ownerInstitutionCode: row.ownerInstitutionCode,
            basisOfRecord: row.basisOfRecord,
            occurrenceID: row.occurrenceGUID,
            catalogNumber: row.catalogNumber,
            otherCatalogNumber: row.otherCatalogNumbers,
            kingdom: row.kingdom,
            phylum: row.phylum,
            class: row.class,
            order: row.order,
            family: row.family,
            day: row.day,
            month: row.month,
            year: row.year,
            scientificName: row.scientificName,
            scientificNameComputed: row.scientificNameComputed,
            taxonID: row.taxonID,
            scientificNameAuthorship: row.scientificNameAuthorship,
            genus: row.genus,
            specificEpithet: row.specificEpithet,
            taxonRank: row.taxonRank,
            intraspecificEpithet: row.infraspecificEpithet,
            identifiedBy: row.identifiedBy,
            dateIdentified: row.dateIdentified,
            identificationReferences: row.identificationReferences,
            identificationRemarks: row.identificationRemarks,
            taxonRemarks: row.taxonRemarks,
            identificationQualifier: row.identificationQualifier,
            typeStatus: row.typeStatus,
            recordedBy: row.recordedByNames,
            recordedByID: row.recordedByID,
            associatedCollectors: row.associatedCollectors,
            recordNumber: row.recordNumber,
            eventDate: row.eventTime,
            startDayOfYear: row.startDayOfYear,
            endDayOfYear: row.endDayOfYear,
            verbatimEventDate: row.verbatimEventDate,
            occurrenceRemarks: row.occurrenceRemarks,
            habitat: row.habitat,
            substrate: row.substrate,
            verbatimAttributes: row.verbatimAttributes,
            fieldNumber: row.fieldNumber,
            informationWithheld: row.informationWithheld,
            dataGeneralizations: row.dataGeneralizations,
            dynamicProperties: row.dynamicProperties,
            associatedTaxa: row.associatedTaxa,
            reproductiveCondition: row.reproductiveCondition,
            establishmentMeans: row.establishmentMeans,
            cultivationStatus: row.cultivationStatus,
            lifeState: row.lifeStage,
            sex: row.sex,
            individualCount: row.individualCount,
            samplingProtocol: row.samplingProtocol,
            samplingEffort: row.samplingEffort,
            preparations: row.preparations,
            country: row.country,
            stateProvince: row.stateProvincecounty,
            county: row.stateProvincecounty,
            municipality:row.municipality,
            locality: row.locality,
            locationRemarks: row.locationRemarks,
            localitySecurity: row.localitySecurity,
            localitySecurityReason: row.localitySecurityReason,
            decimalLatitude: row.latitude,
            decimalLongiitude: row.longitude,
            geodeticDatum: row.geodeticDatum,
            coordinateUncertaintyInMeters: row.coordinateUncertaintyInMeters,
            verbatimCoordinates: row.verbatimCoordinates,
            georeferencedBy: row.georeferenceSources,
            georeferenceProtocol: row.georeferenceProtocol,
            georeferenceSources: row.georeferenceSources,
            georeferenceVerificationStatus: row.georeferenceVerificationStatus,
            georeferenceRemarks: row.georeferenceRemarks,
            minimumElevationInMeters: row.minimumElevationInMeters,
            maximumElevationInMeters: row.maximumElevationInMeters,
            minimumDepthInMeters: row.minimumDepthInMeters,
            maximumDepthInMeters: row.maximumDepthInMeters,
            verbatimDepth: row.verbatimDepth,
            verbatimElevation: row.verbatimElevation,
            disposition: row.disposition,
            language: row.language,
            recordEnteredBy: row.recordEnteredBy,
            modified: row.modified,
            soucePrimaryKey: row.dbpk,
            collId: row.collectionID,
            recordId: row.recordNumber,
            references: row.associatedReferences
        }))

        return download
    }

    rowToCSV(headers, row) {
        const values = headers.map(header => {
            const escaped = (''+row[header]).replace(/"/g, '\\"');
            return `"${escaped}"`;
        })

        return values.join(',') + "\n"
    }

    rowToXML(headers, row) {
        let result = ""
        try {
            result = jsonToXML.parse('occurrence', row)
        }
        catch(e){
            console.log("Error in XML string conversion " + e.Message)
            result = ""
        }
        return result;
    }

    objectToCSV(data) {
        const csvRows = []

        //console.log("objectToCSV")
        // get the header
        const headers = Object.keys(data[0]);
        csvRows.push(headers.join(','));

        // loop over the rows
        for (const row of data) {
            const values = headers.map(header => {
                const escaped = (''+row[header]).replace(/"/g, '\\"');
                return `"${escaped}"`;
            })
            csvRows.push(values.join(','))
        }
        return csvRows.join('\n');
    }

    objectToXML(data) {
        //console.log("objectToXML")
        return jsonToXML.parse('occurrences', data)
    }

    private convertToCSV(arr) {
        const array = [Object.keys(arr[0])].concat(arr)

        return array.map(it => {
            return Object.values(it).toString()
        }).join('\n')
    }

    /*
    private async createCSV(
        csvName: string,
        queryStream: Readable
    ) {
        const db = getConnection()
        const dataDir = await this.appConfig.dataDir()
        const factory = new DataFactory()

        const input = new Readable({
            objectMode: true,
            // eslint-disable-next-line @typescript-eslint/no-empty-function
            read: () => {
            }
        })

        return new Promise<void>((resolve, reject) => {
            withTempDir(dataDir, async (tmpDir) => {
                try {

                    const csvPath = pathJoin(tmpDir, csvName)
                    const writeStream = createWriteStream(csvPath)
                    const output = queryStream //serializerJsonld.import(input)

                    writeStream.on('error', (err) => {
                        console.log("Writestream error " + err);
                    });

                    output.on('data', line => {
                        writeStream.write(line)
                        //console.log(jsonld)
                    })

                    output.on('end', () => {
                        writeStream.close()
                    })


                    let entities = await queryStream.pipe(writeStream)
                    let entities2 = await repo.find({
                        //...findOpts,
                        relations: edges,
                        take: KnowledgeGraphService.DB_LIMIT,
                        skip: offset
                    });

                    while (entities.length > 0) {
                            await Promise.all(entities.map((o) => kgBuilder
                                .addEntity(
                                    input,
                                    factory,
                                    value.meta.targetName,
                                    value.url,
                                    value.keys,
                                    value.properties,
                                    value.edges,
                                    o)));
                            offset += entities.length;

                            entities = await repo.find({
                                // ...findOpts,
                                relations: edges,
                                take: KnowledgeGraphService.DB_LIMIT,
                                skip: offset
                            });
                        }
                    }

                    input.emit("end")
                    //await this.storage.putObject(uploadPath, createReadStream(graphPath), objectTags);
                    resolve()

                } catch (e) {
                    //this.logger.log("Error building graph " + graphName + " error is " + e)
                    reject(e)
                }
            })

    }
     */

    /**
     * Retrieves a specific occurrence by ID
     * @param id The occurrence ID
     * @return ApiOccurrence The occurrence record
     */
    async findByID(id: number): Promise<ApiOccurrence> {
        if (!Number.isInteger(id)) {
            throw new BadRequestException('ID must be an integer');
        }

        const { collection, ...props } = await this.occurrenceRepo.findOne(
            {where: { id: id },
                    relations: {collection: true }}
        );
        return {
            collection: await collection,
            ...props
        }
    }

    /**
     * Creates a new occurrence record
     * @param collectionID The collection in which the occurrence should be created
     * @param occurrenceData The occurrence fields
     * @return ApiOccurrence The created occurrence
     */
    async create(collectionID: number, occurrenceData: DeepPartial<Occurrence>): Promise<ApiOccurrence> {
        const occurrence = this.occurrenceRepo.create({ collectionID, ...occurrenceData });
        const { collection, ...props } = await this.occurrenceRepo.save(occurrence);
        return {
            collection: await collection,
            ...props
        };
    }

    /**
     * Creates a list of occurrences
     * @param collectionID The collection to which the occurrences should be added
     * @param occurrenceData The list of occurrences
     */
    async createMany(collectionID: number, occurrenceData: DeepPartial<Occurrence>[]): Promise<void> {
        const occurrences = occurrenceData.map((o) => {
            return { ...o, collectionID };
        });

        await this.occurrenceRepo.createQueryBuilder()
            .insert()
            .values(occurrences)
            .execute();
    }

    /**
     * Returns a list of the fields of the occurrence entity
     */
    getOccurrenceFields(): string[] {
        const entityColumns = this.occurrenceRepo.metadata.columns;
        return entityColumns.map((c) => c.propertyName);
    }

    /**
     * Creates a new upload in the database
     * @param filePath The path to the file containing occurrences
     * @param mimeType The mimeType of the file
     * @param fieldMap Object describing how upload fields map to the occurrence database
     */
    async createUpload(filePath: string, mimeType: string, fieldMap: OccurrenceUploadFieldMap): Promise<OccurrenceUpload> {
        let upload = this.uploadRepo.create({ filePath, mimeType, fieldMap, uniqueIDField: 'catalogNumber' });
        upload = await this.uploadRepo.save(upload);

        const tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);

        await this.uploadCleanupQueue.add({
            id: upload.id,
            deleteAfter: tomorrow,
        });

        return upload;
    }

    /**
     * @param id The idea of the upload to update
     * @param uniqueIDField The field that uniquely represents a row in an upload csv
     * @param fieldMap A mapping of csv fields to database fields
     * @returns The modified upload
     */
    async patchUploadFieldMap(id: number, uniqueIDField: string, fieldMap: OccurrenceUploadFieldMap): Promise<OccurrenceUpload> {
        const upload = await this.uploadRepo.findOne({where: {id: id}});
        if (!upload) {
            return null;
        }
        await this.uploadRepo.save({
            ...upload,
            uniqueIDField,
            fieldMap
        });
        return upload;
    }

    /**
     * Find a given OccurrenceUpload by id
     */
    async findUploadByID(id: number): Promise<OccurrenceUpload> {
        return this.uploadRepo.findOne({where: {id: id}});
    }

    /**
     * Delete a given occurrenceUpload by id
     */
    async deleteUploadByID(id: number): Promise<boolean> {
        const upload = await this.uploadRepo.delete({ id });
        return upload.affected > 0;
    }

    /**
     * @return Object The value of uniqueField for each row in
     * csvFile along with a count of the null values
     */
    async countCSVNonNull(csvFile: string, uniqueField: string): Promise<{ uniqueValues: any[], nulls: number }> {
        const uniqueFieldValues = new Set();
        let nulls = 0;

        try {
            for await (const batch of csvIterator<Record<string, unknown>>(csvFile)) {
                for (const row of batch) {
                    const fieldVal = row[uniqueField];
                    if (fieldVal) {
                        uniqueFieldValues.add(fieldVal);
                    }
                    else {
                        nulls += 1;
                    }
                }
            }
        } catch (e) {
            throw new Error('Error parsing CSV');
        }

        return { uniqueValues: [...uniqueFieldValues], nulls };
    }

    /**
     * Count the number of distict values of field among all occurrences with collectionID.
     * isIn specifies a filter for the values.
     *
     * Example:
     * countOccurrences(1, 'scientificaName', ['cat', 'dog'])
     * Counts the number of occurrences in collectionID 1 that have 'cat' or 'dog' as a scientificName
     */
    async countOccurrences(collectionID: number, field: string, isIn: any[]): Promise<number> {
        if (isIn.length === 0) {
            return 0;
        }

        const result = await this.occurrenceRepo.createQueryBuilder('o')
            .select([`COUNT(DISTINCT o.${field}) as cnt`])
            .where(`o.collectionID = :collectionID`, { collectionID })
            .andWhere(`o.${field} IS NOT NULL`)
            .andWhere(`o.${field} IN (:...isIn)`, { isIn })
            .getRawOne<{ cnt: number }>();

        return result.cnt;
    }

    /**
     * Add the Upload with uploadID to the queue for processing,
     * specifying the user & collection that the Upload belongs to
     */
    async startUpload(uid: number, collectionID: number, uploadID: number): Promise<void> {
        await this.uploadQueue.add({ uid, collectionID, uploadID });
    }

    /**
     * @param id The database ID of the resource
     * @return boolean Whether the resource was found and deleted
     */
    async deleteByID(id: number): Promise<boolean> {
        const result = await this.occurrenceRepo.delete(id);
        return result.affected > 0;
    }
}
