import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Type } from 'class-transformer';
import {
    ApiImageListItem,
    ApiOccurrenceList,
    ApiOccurrenceListItem
} from '@symbiota2/data-access';
import { Collection, Image } from '@symbiota2/api-database';

@Exclude()
export class OccurrenceListItem implements ApiOccurrenceListItem {
    constructor(occurrence: ApiOccurrenceListItem) {
        Object.assign(this, occurrence);
    }

    //@ApiProperty()
    @Expose()
    id: number;

    @ApiProperty({ type: Collection })
    @Expose()
    @Type(() => Collection)
    collection: Collection;

    @ApiProperty({ type: Image })
    @Expose()
    @Type(() => Image)
    images: Image[];


    //@ApiProperty()
    @Expose()
    catalogNumber: string;

    //@ApiProperty()
    @Expose()
    taxonID: number;

    //@ApiProperty()
    @Expose()
    scientificNameComputed: string;

    //@ApiProperty()
    @Expose()
    scientificName: string;

    //@ApiProperty()
    @Expose()
    scientificNameAuthorship: string;

    //@ApiProperty()
    @Expose()
    latitude: number;

    //@ApiProperty()
    @Expose()
    longitude: number;

    //@ApiProperty()
    @Expose()
    collectionID: number;

    //@ApiProperty()
    @Expose()
    dbpk: string;

    //@ApiProperty()
    @Expose()
    basisOfRecord: string;

    //@ApiProperty()
    @Expose()
    occurrenceID: string;

    //@ApiProperty()
    @Expose()
    year: number | null;

    //@ApiProperty()
    @Expose()
    month: number | null;

    //@ApiProperty()
    @Expose()
    day: number | null;

    //@ApiProperty()
    @Expose()
    kingdom: string;

    //@ApiProperty()
    @Expose()
    phylum: string;

    //@ApiProperty()
    @Expose()
    class: string;

    //@ApiProperty()
    @Expose()
    order: string;


    //@ApiProperty()
    @Expose()
    occurrenceGUID: string;

    //@ApiProperty()
    @Expose()
    otherCatalogNumbers: string;

    //@ApiProperty()
    @Expose()
    ownerInstitutionCode: string;

    //@ApiProperty()
    @Expose()
    datasetID: string;

    //@ApiProperty()
    @Expose()
    family: string;

    //@ApiProperty()
    @Expose()
    genus: string;

    //@ApiProperty()
    @Expose()
    specificEpithet: string;

    //@ApiProperty()
    @Expose()
    taxonRank: string;

    //@ApiProperty()
    @Expose()
    infraspecificEpithet: string;

    //@ApiProperty()
    @Expose()
    taxonRemarks: string;

    //@ApiProperty()
    @Expose()
    identifiedBy: string;

    //@ApiProperty()
    @Expose()
    dateIdentified: Date | null;

    //@ApiProperty()
    @Expose()
    identificationReferences: string;

    //@ApiProperty()
    @Expose()
    identificationRemarks: string;

    //@ApiProperty()
    @Expose()
    identificationQualifier: string;

    //@ApiProperty()
    @Expose()
    typeStatus: string;

    //@ApiProperty()
    @Expose()
    recordedByNames: string;

    //@ApiProperty()
    @Expose()
    recordNumber: string;

    //@ApiProperty()
    @Expose()
    recordedByID: string;

    //@ApiProperty()
    @Expose()
    associatedCollectors: string;

    //@ApiProperty()
    @Expose()
    latestDateCollected: string;

    //@ApiProperty()
    @Expose()
    startDayOfYear: number | null;

    //@ApiProperty()
    @Expose()
    endDayOfYear: number | null;

    //@ApiProperty()
    @Expose()
    verbatimEventDate: string;

    //@ApiProperty()
    @Expose()
    habitat: string;

    //@ApiProperty()
    @Expose()
    substrate: string;

    //@ApiProperty()
    @Expose()
    fieldNotes: string;

    //@ApiProperty()
    @Expose()
    fieldNumber: string;

    //@ApiProperty()
    @Expose()
    eventID: string;

    //@ApiProperty()
    @Expose()
    occurrenceRemarks: string;

    //@ApiProperty()
    @Expose()
    informationWithheld: string;

    //@ApiProperty()
    @Expose()
    dataGeneralizations: string;

    //@ApiProperty()
    @Expose()
    associatedOccurrences: string;

    //@ApiProperty()
    @Expose()
    associatedTaxa: string;

    //@ApiProperty()
    @Expose()
    dynamicProperties: string;

    //@ApiProperty()
    @Expose()
    verbatimAttributes: string;

    //@ApiProperty()
    @Expose()
    behavior: string;

    //@ApiProperty()
    @Expose()
    reproductiveCondition: string;

    //@ApiProperty()
    @Expose()
    cultivationStatus: number | null;

    //@ApiProperty()
    @Expose()
    establishmentMeans: string;

    //@ApiProperty()
    @Expose()
    lifeStage: string;

    //@ApiProperty()
    @Expose()
    sex: string;

    //@ApiProperty()
    @Expose()
    individualCount: string;

    //@ApiProperty()
    @Expose()
    samplingProtocol: string;

    //@ApiProperty()
    @Expose()
    samplingEffort: string;

    //@ApiProperty()
    @Expose()
    preparations: string;

    //@ApiProperty()
    @Expose()
    locationID: string;

    //@ApiProperty()
    @Expose()
    country: string;

    //@ApiProperty()
    @Expose()
    stateProvincecounty: string;

    //@ApiProperty()
    @Expose()
    municipality: string;

    //@ApiProperty()
    @Expose()
    waterBody: string;

    //@ApiProperty()
    @Expose()
    locality: string;

    //@ApiProperty()
    @Expose()
    localitySecurity: number | null;

    //@ApiProperty()
    @Expose()
    localitySecurityReason: string;

    //@ApiProperty()
    @Expose()
    geodeticDatum: string;

    //@ApiProperty()
    @Expose()
    coordinateUncertaintyInMeters: number | null;

    //@ApiProperty()
    @Expose()
    footprintWKT: string;

    //@ApiProperty()
    @Expose()
    coordinatePrecision: number;

    //@ApiProperty()
    @Expose()
    locationRemarks: string;

    //@ApiProperty()
    @Expose()
    verbatimCoordinates: string;

    //@ApiProperty()
    @Expose()
    verbatimCoordinateSystem: string;

    //@ApiProperty()
    @Expose()
    georeferenceProtocol: string;

    //@ApiProperty()
    @Expose()
    degreeOfEstablishment: string;

    //@ApiProperty()
    @Expose()
    associatedSequences: string;

    //@ApiProperty()
    @Expose()
    earliestAgeOrLowestStage: string;

    //@ApiProperty()
    @Expose()
    earliestEonOrLowestEonothem: string;

    //@ApiProperty()
    @Expose()
    earliestEpochOrLowestSeries: string;

    //@ApiProperty()
    @Expose()
    earliestEraOrLowestErathem: string;

    //@ApiProperty()
    @Expose()
    earliestPeriodOrLowestSystem: string;

    //@ApiProperty()
    @Expose()
    eventRemarks: string;

    //@ApiProperty()
    @Expose()
    eventTime: string;

    //@ApiProperty()
    @Expose()
    measurementRemarks: string;

    //@ApiProperty()
    @Expose()
    parentEventID: string;

    //@ApiProperty()
    @Expose()
    associatedOrganisms: string;

    //@ApiProperty()
    @Expose()
    associatedReferences: string;

    //@ApiProperty()
    @Expose()
    countryCode: string;

    //@ApiProperty()
    @Expose()
    datasetName: string;

    //@ApiProperty()
    @Expose()
    subgenus: string;

    //@ApiProperty()
    @Expose()
    georeferenceSources: string;

    //@ApiProperty()
    @Expose()
    georeferenceVerificationStatus: string;

    //@ApiProperty()
    @Expose()
    georeferenceRemarks: string;

    //@ApiProperty()
    @Expose()
    minimumElevationInMeters: number | null;

    //@ApiProperty()
    @Expose()
    maximumElevationInMeters: number | null;

    //@ApiProperty()
    @Expose()
    verbatimElevation: string;

    //@ApiProperty()
    @Expose()
    minimumDepthInMeters: number | null;

    //@ApiProperty()
    @Expose()
    maximumDepthInMeters: number | null;

    //@ApiProperty()
    @Expose()
    verbatimDepth: string;

    //@ApiProperty()
    @Expose()
    previousIdentifications: string;

    //@ApiProperty()
    @Expose()
    disposition: string;

    //@ApiProperty()
    @Expose()
    storageLocation: string;

    //@ApiProperty()
    @Expose()
    genericColumn1: string;

    //@ApiProperty()
    @Expose()
    genericColumn2: string;

    //@ApiProperty()
    @Expose()
    modified: Date | null;

    //@ApiProperty()
    @Expose()
    language: string;

    //@ApiProperty()
    @Expose()
    observerUID: number | null;

    //@ApiProperty()
    @Expose()
    processingStatus: string;

    //@ApiProperty()
    @Expose()
    recordEnteredBy: string;

    //@ApiProperty()
    @Expose()
    duplicateQuantity: number | null;

    //@ApiProperty()
    @Expose()
    labelProject: string;

    //@ApiProperty()
    @Expose()
    dynamicFields: string;

    //@ApiProperty()
    @Expose()
    initialTimestamp: Date | null;

    //@ApiProperty()
    @Expose()
    lastModifiedTimestamp: Date
}

@Exclude()
export class OccurrenceList implements ApiOccurrenceList {
    constructor(count: number, data: ApiOccurrenceListItem[]) {
        this.count = count;
        this.data = data.map((o) => new OccurrenceListItem(o));
    }

    //@ApiProperty()
    @Expose()
    count: number;

    @ApiProperty({ type: OccurrenceListItem, isArray: true })
    @Expose()
    @Type(() => OccurrenceListItem)
    data: OccurrenceListItem[];
}
