
import { ActionRequest } from './ActionRequest.entity';
import { ActionRequestType } from './ActionRequestType.entity'
import { PermissionRequest } from './permission-request.entity';
        
export const providers = [
    ActionRequest.getProvider<ActionRequest>(),
    ActionRequestType.getProvider<ActionRequestType>(),
    PermissionRequest.getProvider<PermissionRequest>()
];
