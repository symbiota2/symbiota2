import {
    AfterUpdate,
    Column,
    Entity,
    Index,
    JoinColumn,
    JoinTable,
    ManyToMany,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import { ActionRequestType } from './ActionRequestType.entity';
import { User } from '../user/User.entity';
import { EntityProvider } from '../../entity-provider.class';

interface StatusTrack {status: string, requesterUID: number, fulfillerUID: number, date: Date};

// @Index(['typeName'])
@Index(['requesterUID'])
@Index(['fulfillerUID'])
@Entity('permissionrequest')
export class PermissionRequest extends EntityProvider {
    @PrimaryGeneratedColumn({ type: 'bigint', name: 'permissionrequestid' })
    id: number;

    @Column('int', { name: 'uid_requestor', unsigned: true })
    requesterUID: number;

    @Column('int', { name: 'uid_fullfillor', unsigned: true, nullable: true })
    fulfillerUID: number;

    @Column('varchar', { name: 'title', length: 250})
    title: string

    @Column('varchar', { name: 'description', nullable: true, length: 900 })
    description: string;

    @Column('int', { name: 'priority', nullable: true })
    priority: number | null;

    @Column('varchar', { name: 'status', default: 'unapproved', length: 12 })
    currentStatus: string;

    @Column('timestamp', {
        name: 'requestdate',
        default: () => 'CURRENT_TIMESTAMP()',
    })
    date: Date;

    @ManyToMany(() => User, { cascade: true})
    @JoinTable({
        name: 'user_permissionrequests',
        joinColumn: { name: 'requester_id', referencedColumnName: 'requesterUID'},
        inverseJoinColumn: {name: 'fulfiller_id', referencedColumnName: 'uid'}
    })
    fulfillers: User[];

    // @Column('simple-json', { name: 'statusTrack' })
    // statusTrack: StatusTrack[];

    // @Column('datetime', { name: 'approveddate', nullable: true })
    // approvedDate: Date;

    // @Column('datetime', { name: 'unassigndate', nullable: true })
    // unassignDate: Date;

    // @OneToMany(() => User, user => user.permissionRequest)
    // @JoinColumn([{ name: 'uid_requestor', referencedColumnName: 'uid' }])
    // requester: Promise<User[]>;

    // @OneToMany(() => User, user => user.permissionRequest)
    // @JoinColumn([{ name: 'uid_fullfillor', referencedColumnName: 'uid' }])
    // fulfiller: Promise<User[]>;

    //@AfterUpdate()
    // async updateStatus(newStatus: Partial<StatusTrack>) {
    //     this.statusTrack.push(
    //         {
    //             status: newStatus.status,
    //             requesterUID: this.requesterUID,
    //             fulfillerUID: newStatus.fulfillerUID,
    //             date: new Date()
    //         }
    //     )
    //     this.currentStatus = this.statusTrack.pop().status;
    // }
}
