import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import { EntityProvider } from '../../entity-provider.class';

@Entity('userpermissions')
export class Permission extends EntityProvider {
    @PrimaryGeneratedColumn({ type: 'int', name: 'userpermissionid', unsigned: true })
    id: number;

    @Column( 'varchar', {unique: true})
    name: string;
}
