import {
    Column,
    Entity,
    Index,
    JoinColumn,
    JoinTable,
    ManyToMany,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from './User.entity';
import { EntityProvider } from '../../entity-provider.class';
import { ApiUserRoleName } from '@symbiota2/data-access';
import { Permission } from './UserPermission.entity';

//@Index('Index_userroles_table', ['tableName', 'tablePrimaryKey'])
@Index(['details'])
@Index(['name', 'uid'], { unique: true })
@Entity('userroles')
export class UserRole extends EntityProvider {

    @PrimaryGeneratedColumn({ type: 'int', name: 'userroleid', unsigned: true })
    id: number;

    @Column('int', { name: 'uid', nullable: true, unsigned: true })
    uid: number;

    // Role name
    @Column('varchar', { name: 'role', length: 45})
    name: ApiUserRoleName;

    // @Column('varchar', { name: 'tablename', nullable: true, length: 45 })
    // tableName: string | null;

    // @Column('int', { name: 'tablepk', nullable: true })
    // tablePrimaryKey: number | null;

    // @Index('index_details', ['details'], { json: true })
    @Column('simple-json', { name: 'details', nullable: true, default: '[]' })
    details: {
        tableName: string | null,
        tablePrimaryKey: number | null,
        secondaryVariable: string | null,
        access: ApiUserRoleName | null,
        assignedByUID: number | null,
        assignedForUID: number | null,
        default: boolean | false,
    }[];
    // @Column('varchar', {
    //     name: 'secondaryVariable',
    //     nullable: true,
    //     length: 45
    // })
    // secondaryVariable: string | null;

    // @Column({ default: false })
    // default: boolean;

    @Column('varchar', { name: 'notes', nullable: true, length: 250 })
    notes: string | null;

    // @Column('int', { name: 'uidassignedby', nullable: true, unsigned: true })
    // assignedByUID: number | null;

    @Column('timestamp', {
        name: 'initialtimestamp',
        default: () => 'CURRENT_TIMESTAMP()',
    })
    initialTimestamp: Date;

    // @ManyToOne(() => User, (user) => user.roles, {
    //     onDelete: 'CASCADE',
    //     onUpdate: 'CASCADE',
    // })
    // @JoinColumn([{ name: 'uid', referencedColumnName: 'uid' }])
    // user: Promise<User> | User;

    // @ManyToOne(() => User, (users) => users.assignedRoles, {
    //     onDelete: 'SET NULL',
    //     onUpdate: 'CASCADE',
    // })
    // @JoinColumn([{ name: 'uidassignedby', referencedColumnName: 'uid' }])
    // assigner: Promise<User>;

    // @OneToMany(() => User, (user) => user.role)
    // user: Promise<User[]> | User[];

    @ManyToMany(() => User, user => user.roles, { onDelete: 'CASCADE' })
    user: Promise<User[]> | User[];

    @ManyToMany(() => Permission, { cascade: true})
    @JoinTable({
        name: 'role_permissions',
        joinColumn: { name: 'role_id', referencedColumnName: 'id'},
        inverseJoinColumn: {name: 'permission_id', referencedColumnName: 'id'}
    })
    permissions: Permission[];
}
