


# Configure Symbiota2
Symbiota2 can be configured for your installation in three ways.

1. **Environment** - Create and edit the `.env` file to set up environment variables.
2. **Menus** - Edit the menus files in `config/menus` directory to adjust local menus.
3. **Internationalization** - The edits to user interface phrases are stored in the `config/i18n` directory.

## Environment
Any variable without a default is required for the Symbiota2 API to run.  
For development, a [.env file](https://www.npmjs.com/package/dotenv) can be used.

| Environment variable                     | Description                                                                             | Default                      |  
|------------------------------------------|-----------------------------------------------------------------------------------------|------------------------------|  
| SYMBIOTA2_NODE_ENV                       | 'development' or 'production', determines log level, etc.                               | 'production'                 |  
| SYMBIOTA2_APP_PORT                       | The port the the API server will run on                                                 | 8080                         |  
| SYMBIOTA2_APP_UI                         | The url of the local UI (for CORS)                                                      | 'https://demo.symbiota2.org'       |  
| SYMBIOTA2_APP_DATA_DIR                   | The data directory for the API server                                                   | './data'                     |  
| SYMBIOTA2_DATABASE_TYPE                  | The scheme for the database uri                                                         | 'mariadb'                    |  
| SYMBIOTA2_DATABASE_HOST                  | The database host                                                                       | '127.0.0.1'                  |  
| SYMBIOTA2_DATABASE_PORT                  | The database port                                                                       | 3306                         |  
| SYMBIOTA2_DATABASE_VOLUME                | The database volume for the docker compose                                              | s2-db:/var/lib/mysql         |  
| SYMBIOTA2_DATABASE_NAME                  | The name of the database on DATABASE_HOST                                               | 'symbiota'                   |  
| SYMBIOTA2_DATABASE_USER                  | The user used to connection to DATABASE_NAME                                            | 'root'                       |  
| SYMBIOTA2_DATABASE_PASSWORD              | The password for DATABASE_USER                                                          | 'password'                   |  
| SYMBIOTA2_IMAGE_LIBRARY                  | Directory where the image library lives                                                 | 'imglib'                     |  
| SYMBIOTA2_REDIS_HOST                     | The redis server to use for asynchronous job queues                                     | '127.0.0.1'                  |  
| SYMBIOTA2_REDIS_PORT                     | The port for REDIS_HOST                                                                 | 6379                         |  
| SYMBIOTA2_ENABLE_ELASTICSEARCH           | Will Elasticsearch be used?                                                             | 1                            |  
| SYMBIOTA2_ELASTICSEARCH_VERSION          | Version of elasticsearch to use                                                         | "7.17.0"                     |  
| SYMBIOTA2_ELASTICSEARCH_DATA_FOLDER      | Where is Elasticsearch data stored?                                                     | "./elasticsearch/data"       |  
| SYMBIOTA2_ELASTICSEARCH_PORT             | Port for Elasticsearch                                                                  | 9200                         |  
| SYMBIOTA2_ELASTICSEARCH_USER             | Elasticsearch user                                                                      | elastic                      |  
| SYMBIOTA2_ELASTICSEARCH_PASSWORD         | Elasticsearch password (change this!)                                                   | hello                        |  
| SYMBIOTA2_ELASTICSEARCH_ENV_JAVA_OPTIONS | Set java options, such as memory use for Elasticsearch                                  | "ES_JAVA_OPTS=-Xmx4g -Xms4g" |  
| SYMBIOTA2_KIBANA_PORT                    | Port for Kibana                                                                         | 5601                         |  
| SYMBIOTA2_LOGSTASH_PORT                  | Port for Logstash                                                                       | 5600                         |  
| SYMBIOTA2_LOGSTASH_ENV_JAVA_OPTIONS      | Java options for Logstash                                                               | "-Xmx256m -Xms256m"          |  
| SYMBIOTA2_LOGSTASH_VERSION               | Version of Logstash to use                                                              | "7.16.3"                     |  
| SYMBIOTA2_LOGSTASH_FOLDER                | Folder for pipeline definitions                                                         | "./logstash"                 |  
| SYMBIOTA2_LOGSTASH_CONFIG_FOLDER         | Folder for logstash config                                                              | "./logstash/config"          |  
| SYMBIOTA2_MINIO_ROOT_USER                | Minio root password                                                                     | root                         |  
| SYMBIOTA2_MINIO_ROOT_PASSWORD            | Minio root password                                                                     | password                     |  
| SYMBIOTA2_S3_HOST                        | The server to use for S3                                                                | '127.0.0.1'                  |  
| SYMBIOTA2_S3_PORT1                       | A port to use on S3_HOST                                                                | 9000                         |  
| SYMBIOTA2_S3_PORT2                       | Another port to use on S3_HOST                                                          | 9001                         |  
| SYMBIOTA2_SMTP_HOST                      | The smtp server to use for sending emails                                               | '127.0.0.1'                  |  
| SYMBIOTA2_SMTP_PORT                      | The port to use on SMTP_HOST                                                            | 25                           |  
| SYMBIOTA2_SMTP_USER                      | The optional user to use for authentication with SMTP_HOST                              | ''                           |  
| SYMBIOTA2_SMTP_PASSWORD                  | The optional password for SMTP_USER                                                     | ''                           |  
| SYMBIOTA2_SMTP_SENDER                    | The email address that emails will originate from                                       | 'noreply@symbiota2.org'      |  
| SYMBIOTA2_STORAGE_SERVER                 | The S3 server to use for backend storage                                                | http://127.0.0.1:9000        |  
| SYMBIOTA2_STORAGE_USER                   | The AWS Access Key ID (or equivalent) to use for authentication with STORAGE_SERVER     | 'symbiota2'                  |  
| SYMBIOTA2_STORAGE_PASSWORD               | The AWS Secret Access Key (or equivalent) to use for authentication with STORAGE_SERVER | 'password'                   |  
| SYMBIOTA2_STORAGE_BUCKET                 | The S3 bucket to use on STORAGE_SERVER                                                  | 'symbiota2'                  |  
| SYMBIOTA2_ENABLE_AUTH                    | FOR DEBUGGING ONLY: Set to 0 to disable API authentication                              | 1                            |  

## Menus
Navigation bar menus for each user interface plugin can be configured by editing the file in the `config/menus` directory.  For example to replace the `Image` drop-down menu, edit the `config/menus/ui-plugin-image-custom-menu.js` directory.  The file will be incorporated into Symbiota2 in the next build of the system (or if using Docker run of `docker container restart s2-ui-setup`).
## Internationalization

The UI uses [ngx-translate's http loader](http://www.ngx-translate.com/) to load internationalization files as JSON.

1. Run `npm run i18n:init libs/<my-plugin>/src/i18n` to initialize an internationalization directory.

2. Create a default language file `libs/<my-plugin>/src/i18n/translate/default`.  
   The file should be a two letter language prefix (known to Google translate) with  
   a suffix of `.json`.  For example if you want to use English as the default to   
   translate to other files, then create the file `libs/<my-plugin>/src/i18n/translate/default/en.json`

3. Edit the language file as needed.

4. (See below about getting a Google API translation key first). Run `npm run i18n:translate libs/<my-plugin>` to translate the default language  
   to the other languages in the project.  This will generate files in `npm run i18n:init libs/<my-plugin>/src/i18n/translate/default/generated`  
   for the other languages in the project.  
   Alternatively use ngx-translate's  
   [translate pipe or translation service](https://github.com/ngx-translate/core#5-use-the-service-the-pipe-or-the-directive)  
   to create language-independent text in the UI.
7. Run `npm run i18n` to merge all core & plugin internationalization files into [apps/ui/src/assets/i18n](./apps/ui/src/assets/i18n)  
   where angular can serve them.

Symbiota2 has support for refreshing all translations using the Google Translate API provided by Google Cloud.

1. Acquire the API key from S2 Google Drive folder to use the Google Cloud instance and place it into a file named `gCloudTranslateKey.txt` in the `bin` directory of symbiota2 project structure.

2. Run `npm run i18n:translate` to translate all files or `npm run i18n:translate <plugin directory names>` where plugin directory names is a list of one or more of the following options:

    - ui-plugin-collection
    - ui-plugin-image
    - ui-plugin-occurrence
    - ui-plugin-taxonomy
    - ui
    - all

Example command to translate the ui-plugin-collection and ui-plugin-image i18n files:

`npm run i18n:translate ui-plugin-collection ui-plugin-image`  
  
