To install with Docker do the following steps.  This assumes a Linux system.
1. Create a Symbiota2 directory and navigate to the directory.
>       mkdir symbiota2
>       cd symbiota2
2. Git pull - Get the project code
>       git clone https://gitlab.com/symbiota2.git
3. Configure Symbiota2 - Follow the [configuration instructions](configuration.md).  At the very least you will edit the environment variables in the `.env` file.  The environment variables are described in the configuration instructions.  Create and edit the `.env` file to set these variables.
>       vi .env
4. Fix Elasticsearch vm-max count - Elasticsearch needs to adjust a virtual memory parameter.  To set it once, follow the [Elasticsearch setting vm-max-map-count instructions](https://www.elastic.co/guide/en/elasticsearch/reference/current/vm-max-map-count.html).  But this is not a permanent fix so you'll need to edit a system configuration file as described in this [Stack Overflow thread](https://stackoverflow.com/questions/42889241/how-to-increase-vm-max-map-count).
5. Run Docker - Use Docker to pull the images and set up the containers to run Symbiota2.  The -d flag is to run in detached mode.  The -f flags specify the Docker compose files with instructions for installing the code.
>       docker-compose -f docker-s2-dbs.yml -f docker-s2-remote.yml -f docker-s2-elk.yml up -d
6. Useful Docker commands
   Bring the Docker compose down (stopping the services).
>      docker-compose -f docker-s2-dbs.yml -f docker-s2-remote.yml -f docker-s2-elk.yml up down
Checking the logs for the s2-ui container.
>      docker container logs s2-ui
List the images.
>      docker image ls
Stop a container (usually stop the logstash image)
>      docker container stop s2-logstash
