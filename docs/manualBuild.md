## Manual Re-Build

Pull the source and merge with local changes using `git pull --rebase`.

Run `ng build ui` to build the UI.

Run `nx build api` to build the API.

The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
