## Docker Re-Build of Symbiota2
If you are running Symbiota2 with Docker the system can be synchronized to the latest version as follows.

1. Bring down the running system - First shut down the Symbiota2 services that are currently running.

>     docker-compose -f docker-s2-dbs.yml -f docker-s2-remote.yml -f
>     docker-s2-elk.yml down

2. Git the source - This step probably can be skipped since the config and documentation should not change frequently, but in case they do then we have to git the new version of the code.
>     git pull --rebase
3. Remove the current Docker image - Without removing the image we can't be sure that Docker will grab the latest image, so need to explicitly remove it.
>     docker rmi -f registry.gitlab.com/symbiota2/symbiota2
4. Restart Symbiota2 on Docker - Compose the system and get it running again.
>     docker-compose -f docker-s2-dbs.yml -f docker-s2-remote.yml -f docker-s2-elk.yml up -d
