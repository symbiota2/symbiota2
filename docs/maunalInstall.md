How to get an instance running on a Linux box.
1. Install docker

   `sudo apt install docker-compose`
2. Install nodeJS and manager

   `sudo apt install npm`
3. Clone Symbiota2

   `sudo apt install git` (if Git isn't installed)
   
   `git clone https://gitlab.com/symbiota2.git`
4. Install nx development environment (`npm i npx`)
5. Start Symbiota databases and ELK stack

   `sudo docker-compose -f docker-s2-dbs.yml -f docker-s2-elk.yml up -d`
6. Build and run Symbiota2  

   `npm run start:dev`  (run in development mode)

   `npm run start`          (run in production mode)
7. Go to the web-address that now appears in your terminal window. Congratulations! You're running Symbiota2!  
