#This is a sample Image
FROM node:20.15.1

#ENV NODE_ENV production

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
#COPY package*.json ./
COPY . .

# RUN apt-get update -y && apt-get install nginx -y

RUN npm install
# If you are building your code for production
#RUN npm ci --omit=dev

RUN npm run build

# Bundle app source
#COPY dist dist
#COPY node_modules node_modules

# Stage 2: Serve app with nginx server

# Use official nginx image as the base image
# FROM nginx
#

# Copy the build output to replace the default nginx contents.
# COPY --from=build /usr/src/app/dist/apps/ui /usr/share/nginx/html

# Expose port 82
# EXPOSE 80

EXPOSE 8080
#EXPOSE 4200

#CMD ["echo","Building"]
##CMD ["npm", "run", "build"]
#CMD ["echo","Starting"]
#CMD ["npm", "run", "start:dev"]
#USER node
#CMD ["echo","Image created"]
#CMD [ "node", "dist/apps/api/main.js" ]
