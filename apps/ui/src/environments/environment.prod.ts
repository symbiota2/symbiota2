export const environment = {
    production: true,
    apiUrl: 'https://api.demo.symbiota2.org/api/v1',
    defaultLanguage: 'en',
    appTitle: "Symbiota2 Demo",
    recaptchaSiteKey: '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI',
    // For mapping functionality
    tilesUrl: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    tilesToken: '',
    enableFooter: true,
    kibanaServer: 'http://localhost',
    kibanaPort: '5601',
    kibanaDashboardImageSearch: '/app/dashboards?auth_provider_hint=anonymous1#/view/fc340bf0-82df-11ec-b9ae-27af917973e5',
    kibanaDashboardCollectionStats: '/app/dashboards?auth_provider_hint=anonymous1#/view/a9f7a9a0-bb79-11ec-87e4-7372471162d9',
    kibanaDashboardSpatialModule: '/app/dashboards?auth_provider_hint=anonymous1#/view/3050b510-a931-11ec-8fad-591789a7e073',
};
