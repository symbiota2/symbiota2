import { NgModule } from "@angular/core";
import { RouterModule, Route } from "@angular/router";
import { HomePage } from "./pages/home/home.component";
import { UserProfilePage } from './pages/user-profile/user-profile.component';
import { CreateUserProfilePage } from './pages/create-user-profile/create-user-profile.component';
import {
    ROUTE_FORGOT_PASSWORD,
    ROUTE_FORGOT_USERNAME,
    ROUTE_SITEMAP,
    ROUTE_USER_LIST,
    ROUTE_USER_CREATE,
    ROUTE_USER_PROFILE,
    ROUTE_ADD_USER,
    ROUTE_UPDATE_USER,
    ROUTE_ROLES_LIST,
    ROUTE_ROLE_UPDATE,
    ROUTE_ROLE_CREATE,
    ROUTE_ROLES_REQUESTED,
    ROUTE_PERMISSION_LIST,
    ROUTE_PERMISSION_CREATE,
    ROUTE_PERMISSION_UPDATE,
} from '@symbiota2/ui-common';
import { ForgotPasswordPage } from './pages/forgot-password/forgot-password.component';
import { ForgotUsernamePage } from './pages/forgot-username/forgot-username.component';
import { UserlistPageComponent } from "./pages/userlist-page/userlist-page.component";
import { AddUserPage } from "./pages/add-user/add-user.page";
import { UpdateUserPage } from "./pages/update-user/update-user.page";
import { RolelistPageComponent } from './pages/rolelist-page/rolelist-page.component';
import { UpdateRolePage } from "./pages/update-role/update-role.page";
import { CreateRolePage } from './pages/create-role/create-role.page';
import { RequestedRolesPage } from "./pages/requested-roles/requested-roles.page";
import { PermissionCreateFormComponent } from './components/create-permission-form/create-permission-form.component';
import { PermissionlistPageComponent } from './pages/permissionlist-page/permissionlist-page.component';
import { UpdatePermissionFormComponent } from "./components/update-permission-form/update-permission-form.component";

const defaultRoutes: Route[] = [
    { path: HomePage.ROUTE, component: HomePage },
    { path: ROUTE_USER_PROFILE, component: UserProfilePage },
    { path: ROUTE_ADD_USER, component: AddUserPage },
    { path: ROUTE_UPDATE_USER, component: UpdateUserPage},
    { path: ROUTE_USER_LIST, component: UserlistPageComponent },
    { path: ROUTE_USER_CREATE, component: CreateUserProfilePage },
    { path: ROUTE_FORGOT_PASSWORD, component: ForgotPasswordPage },
    { path: ROUTE_FORGOT_USERNAME, component: ForgotUsernamePage },
    { path: ROUTE_ROLES_LIST, component: RolelistPageComponent },
    { path: ROUTE_ROLE_UPDATE, component: UpdateRolePage },
    { path: ROUTE_ROLE_CREATE, component: CreateRolePage },
    { path: ROUTE_ROLES_REQUESTED, component: RequestedRolesPage },
    { path: ROUTE_PERMISSION_CREATE, component: PermissionCreateFormComponent },
    { path: ROUTE_PERMISSION_LIST, component: PermissionlistPageComponent },
    { path: ROUTE_PERMISSION_UPDATE, component: UpdatePermissionFormComponent },
];

/**
 * Configures routing for the core of the UI. Additional (plugin) routes can be
 * added at runtime via the ui-common plugin.
 */
@NgModule({
    imports: [
        RouterModule.forRoot(
            defaultRoutes,
            {
                //relativeLinkResolution: 'legacy',
                onSameUrlNavigation: 'reload',
                anchorScrolling: 'enabled'
            }
        )
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
