import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {
    AlertService,
    User,
    UserService,
    PluginService,
    NavBarLink,
    ROUTE_USER_PROFILE,
    ROUTE_USER_CREATE,
    ROUTE_SITEMAP,
} from '@symbiota2/ui-common';
import { MatDialog } from '@angular/material/dialog';
import { LoginDialog } from '../login-dialog/login-dialog.component';
import { Router } from '@angular/router';
import { HomePage } from '../../pages/home/home.component';
import { ApiUserNotification } from '@symbiota2/data-access';
import { NotificationDialog } from './notification-dialog/notification-dialog.component';
import { Observable } from 'rxjs';
import { filter, map, shareReplay } from 'rxjs/operators';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { customLinksEnd, customLinksStart, superAdminLinks } from './custom-navbarlinks';
import { CheckEmailService } from '../../app.service';
import { MonitoringService } from '../../../../../../libs/ui-common/src/lib/monitoring';
import { HealthDialog } from '../health-dialog/health-dialog.component';
//import { MatSelectModule } from '@angular/material';
const getAdminLinks$ = new Observable(
    function subscribe(subscriber) {
        // const id = setInterval(() => {
        //     subscriber.next('hi');
        // }, 1000);
    });

@Component({
    selector: 'symbiota2-navbar',
    standalone: false,
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
    isHandset$: Observable<boolean> = this.breakpointObserver
        .observe([Breakpoints.Small, Breakpoints.Handset])
        .pipe(
            map((result) => result.matches),
            shareReplay()
        );

    navigationData: any;

    pluginLinks$ = this.plugins.navBarLinks$;
    adminLinks$: any = null;
    currentUser$ = this.userService.currentUser;
    notifications = this.userService.notifications;
    notificationCount = this.userService.notificationCount;
    apiIsUp : boolean = true

    readonly ROUTE_HOME = HomePage.ROUTE;
    readonly ROUTE_PROFILE = ROUTE_USER_PROFILE;
    readonly ROUTE_CREATE_PROFILE = ROUTE_USER_CREATE;

    searchMenu: NavBarLink;
    toolMenu: NavBarLink;
    user: User;
    isSuperAdmin: boolean;
    editing = false
    canEdit = false

    constructor(
        private readonly userService: UserService,
        private readonly alertService: AlertService,
        private readonly dialog: MatDialog,
        private readonly translate: TranslateService,
        private readonly plugins: PluginService,
        private readonly router: Router,
        private readonly breakpointObserver: BreakpointObserver,
        private readonly checkEmail: CheckEmailService,
        private readonly monitor: MonitoringService,
    ) {}

    ngOnInit(): void {
        //Check if superAdmin
        this.currentUser$

            .pipe(filter((user) => user !== null))
            .subscribe(user => {
            this.user = user;
            //this.canEdit = user.canEditProject(user.uid)
            if (this.user && user.isSuperAdmin()) {
                // create new categories
                this.pluginLinks$ = this.pluginLinks$.pipe(
                    map((navMap: Map<string, NavBarLink[]>) => {
                        //add custom navbar elements
                        const data = new Map<string, NavBarLink[]>([
                            ...customLinksStart.entries(),
                            ...navMap.entries(),
                            ...customLinksEnd.entries(),
                            // ...superAdminLinks.entries(),
                        ]);

                        return data;
                    })
                );
                this.adminLinks$ = new Map<string, NavBarLink[]>([
                    // add admin elements
                    ...superAdminLinks.entries()
                ])
            }
            else {
                // create new categories
                this.pluginLinks$ = this.pluginLinks$.pipe(
                    map((navMap: Map<string, NavBarLink[]>) => {
                        //add custom navbar elements
                        return new Map<string, NavBarLink[]>([
                            ...customLinksStart.entries(),
                            ...navMap.entries(),
                            ...customLinksEnd.entries(),
                        ]);
                    })
                );
            }
            // get plugin categories
            this.linkCategories().subscribe((categories) => {
                // get category links
            });
        });

        /* // create new categories
         this.pluginLinks$ = this.pluginLinks$.pipe(
             map((navMap: Map<string, NavBarLink[]>) => {
                 //add custom navbar elements
                 return new Map<string, NavBarLink[]>([
                     ...customLinksStart.entries(),
                     ...navMap.entries(),
                     ...customLinksEnd.entries(),
                 ]);
             })
         );

         // get plugin categories
         this.linkCategories().subscribe((categories) => {
             // get category links
         });*/

        // Check if API is up
        this.monitor.apiUpCheck().subscribe((result) => {
            // console.log("result is " + result)
            this.apiIsUp = result == 1
        })

        // add plugin links to new categories links in a easy to customize way
    }

    linkCategories(): Observable<string[]> {
        const linkCategories = this.pluginLinks$.pipe(map((pls) => [...pls.keys()]));
        //linkCategories.subscribe(data => console.log('data: ', data))
        return linkCategories;
    }

    categoryLinks(category: string): Observable<NavBarLink[]> {
        const categoryLinks = this.pluginLinks$.pipe(map((pls) => pls.get(category)));
        //categoryLinks.subscribe(data => console.log('data 2: ', data))
        return categoryLinks;
    }

    adminLinks(type = null) {

        const links = [...superAdminLinks.entries()];

        if (type === 'key') {
            return links[0][0];
        }
        if (type === 'value') {
            return links[0][1]
        }

        return links[0][1].length > 0;
    }

    isStringURL(testString) {
        return this.checkEmail.isStringURL(testString);
    }

    isToday(date: Date) {
        const today = new Date();
        return (
            date.getFullYear() === today.getFullYear() &&
            date.getMonth() === today.getMonth() &&
            date.getDate() === today.getDate()
        );
    }

    onLogin() {
        this.dialog.open(LoginDialog, { disableClose: true });
    }

    onHealth() {
        this.dialog.open(HealthDialog, { disableClose: true });
    }

    onLogout() {
        this.router.navigate(['.']).then(() => {
            this.userService.logout();
            this.adminLinks$ = null;
            //location.reload()
        });
    }

    onNotificationClicked(notification: ApiUserNotification) {
        const notificationDialog = this.dialog.open(NotificationDialog, {
            minWidth: '25%',
            minHeight: '25%',
            maxWidth: '75%',
            maxHeight: '75%',
            disableClose: true,
            data: notification,
        });

        notificationDialog.afterClosed().subscribe((shouldDelete) => {
            if (shouldDelete) {
                this.userService.deleteNotification(notification.id);
            }
        });
    }

    useLanguage(language: string) {
        this.translate.use(language);
    }

    turnOnEditing() {
        this.editing = true
        this.userService.setIAMEditing()
        //this.translate.use(language);
    }

    turnOffEditing() {
        this.editing = false
        this.userService.setIAMViewing()
        /*
        this.currentUser$.subscribe((user) => {
            user.setIAMViewing()
        })

         */
        //this.translate.use(language);
    }

    clearNotifications() {
        this.userService.deleteAllNotifications();
    }

    get currentLang(): string {
        return this.translate.currentLang;
    }
}
