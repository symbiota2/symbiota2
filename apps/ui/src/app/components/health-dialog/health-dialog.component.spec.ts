import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthDialog } from './health-dialog.component';

describe('HealthDialogComponent', () => {
  let component: HealthDialog;
  let fixture: ComponentFixture<HealthDialog>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HealthDialog ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthDialog);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
