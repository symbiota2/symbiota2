import { ApiUserRoleName } from '@symbiota2/data-access';
export const UserRoleValues = Object.values(ApiUserRoleName);
export const UserRoleKeys = Object.keys(ApiUserRoleName);
export type UserRoleName = typeof ApiUserRoleName;
