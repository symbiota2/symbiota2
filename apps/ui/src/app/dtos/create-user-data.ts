import { CreateUserInputDto, AddRolePermissionsDto } from '@symbiota2/api-auth';
import { UserRoleInputDto } from '@symbiota2/ui-common';

export type AddRolePermissions = Pick<AddRolePermissionsDto, 'role' | 'permissions'>;
export type CreateUserData = Partial<CreateUserInputDto>;