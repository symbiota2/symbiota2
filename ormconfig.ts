import { ConnectionOptions } from 'typeorm';
import { NestFactory } from '@nestjs/core';
import { AppConfigModule, AppConfigService } from './libs/api-config/src';
import { glob } from 'glob';
// @ts-ignore
import path from 'path';
//import { AppConfigModule, AppConfigService } from '@symbiota2/api-config';

const API_DATABASE_PLUGIN_DIR = path.join('libs', 'api-database')
const ENTITIES_DIR = path.join(API_DATABASE_PLUGIN_DIR, 'src', 'entities');
const MIGRATIONS_DIR = path.join(API_DATABASE_PLUGIN_DIR, 'src', 'migrations');

async function bootstrap(): Promise<ConnectionOptions> {
    const app = await NestFactory.createApplicationContext(
        AppConfigModule,
        { logger: ['error','warn', 'log'] }
    );
    const appConfig = app.get(AppConfigService);

    const entitiesFiles = glob.sync(path.join(ENTITIES_DIR, "**", "*.entity.ts"));
    const migrationsFiles = glob.sync(path.join(MIGRATIONS_DIR, "**", "*.ts"));

    return {
        ...appConfig.databaseConfiguration(),
        logging: ['error', 'warn', 'log'],
        entities: entitiesFiles,
        migrations: migrationsFiles,
        cli: {
            entitiesDir: ENTITIES_DIR,
            migrationsDir: MIGRATIONS_DIR
        },
    };
}

const exp = bootstrap();
module.exports = exp;
export default exp;
