esbuild
    .build({
        entryPoints: splitStdout,
        allowOverwrite: true,
        minify: true,
        sourcemap: true,
        outdir: 'dist',
        platform: 'node',
        logLevel: 'silent',
        external: [
            ...Object.keys(dependencies),
            ...Object.keys(devDependencies),
            '@internal/client',
        ],
        target: ['node20.10.0'],
        bundle: true,
        resolveExtensions: ['.ts', '.js', '.json'],
        format: 'esm',
    })
