#!/usr/bin/env node

const fs = require('fs');
const path = require('path');
//require('dotenv').config({path: __dirname + '/.env'}).
require('dotenv').config({path: '.env'})

const ejs = require('ejs');

const environmentFilesDirectory = path.join(__dirname, '../apps/ui/src/environments');
const targetProdEnvironmentTemplateFileName = 'environment.prod.ts.template';
const targetProdEnvironmentFileName = 'environment.prod.ts';
const targetEnvironmentTemplateFileName = 'environment.ts.template';
const targetEnvironmentFileName = 'environment.ts';

// Define default values in case there are no defined ones,
// but you should define only non-crucial values here,
// because build should fail if you don't provide the correct values
// for your production environment
const defaultEnvValues = {
    SYMBIOTA2_UI_IN_PRODUCTION: false,
    SYMBIOTA2_UI_API_URL: "http://localhost:8080/api/v1",
    SYMBIOTA2_UI_DEFAULT_LANGUAGE: "en",
    SYMBIOTA2_UI_TITLE: "Symbiota2 Demo",
    SYMBIOTA2_UI_RECAPTHA_SITE_KEY: "6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI",
    // For mapping functionality
    SYMBIOTA2_UI_TILES_URL: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
    SYMBIOTA2_UI_TILES_TOKEN: "",
    SYMBIOTA2_UI_ENABLE_FOOTER: true,
    SYMBIOTA2_KIBANA_SERVER: 'http://localhost',
    SYMBIOTA2_KIBANA_PORT: '5601',
    SYMBIOTA2_KIBANA_DASHBOARD_SPATIAL_MODULE: '/app/dashboards?auth_provider_hint=anonymous1#/view/3050b510-a931-11ec-8fad-591789a7e073',
    SYMBIOTA2_KIBANA_DASHBOARD_COLLECTION_STATS: '/app/dashboards?auth_provider_hint=anonymous1#/view/a9f7a9a0-bb79-11ec-87e4-7372471162d9',
    SYMBIOTA2_KIBANA_DASHBOARD_IMAGE_SEARCH: '/app/dashboards?auth_provider_hint=anonymous1#/view/fc340bf0-82df-11ec-b9ae-27af917973e5',
};
const defaultProdEnvValues = {
    SYMBIOTA2_UI_IN_PRODUCTION: true,
    SYMBIOTA2_UI_API_URL: "https://api.demo.symbiota2.org/api/v1",
    SYMBIOTA2_UI_DEFAULT_LANGUAGE: "en",
    SYMBIOTA2_UI_TITLE: "Symbiota2 Demo",
    SYMBIOTA2_UI_RECAPTHA_SITE_KEY: "6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI",
    // For mapping functionality
    SYMBIOTA2_UI_TILES_URL: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
    SYMBIOTA2_UI_TILES_TOKEN: "",
    SYMBIOTA2_UI_ENABLE_FOOTER: true,
    SYMBIOTA2_KIBANA_SERVER: 'http://localhost',
    SYMBIOTA2_KIBANA_PORT: '5601',
    SYMBIOTA2_KIBANA_DASHBOARD_SPATIAL_MODULE: '/app/dashboards?auth_provider_hint=anonymous1#/view/3050b510-a931-11ec-8fad-591789a7e073',
    SYMBIOTA2_KIBANA_DASHBOARD_COLLECTION_STATS: '/app/dashboards?auth_provider_hint=anonymous1#/view/a9f7a9a0-bb79-11ec-87e4-7372471162d9',
    SYMBIOTA2_KIBANA_DASHBOARD_IMAGE_SEARCH: '/app/dashboards?auth_provider_hint=anonymous1#/view/fc340bf0-82df-11ec-b9ae-27af917973e5',
};

// Load template file
const environmentTemplate = fs.readFileSync(
    path.join(environmentFilesDirectory, targetEnvironmentTemplateFileName),
    {encoding: 'utf-8'}
);
const environmentProdTemplate = fs.readFileSync(
    path.join(environmentFilesDirectory, targetProdEnvironmentTemplateFileName),
    {encoding: 'utf-8'}
);

// Generate output data
const output = ejs.render(environmentTemplate, Object.assign({}, defaultEnvValues, process.env));
// Write environment file
fs.writeFileSync(path.join(environmentFilesDirectory, targetEnvironmentFileName), output);

const outputProd = ejs.render(environmentProdTemplate, Object.assign({}, defaultProdEnvValues, process.env));
// Write environment file
fs.writeFileSync(path.join(environmentFilesDirectory, targetProdEnvironmentFileName), outputProd);

process.exit(0);
