#!/usr/bin/env node

const fs = require('fs');
const path = require('path');
//require('dotenv').config({path: __dirname + '/.env'}).
require('dotenv').config({path: '.env'})

const ejs = require('ejs');

const distFilesDirectory = path.join(__dirname, '../dist/apps/ui');
const mainFileName = 'main.js';
const targetMainFileName = 'main.js';

// Define default values in case there are no defined ones,
// but you should define only non-crucial values here,
// because build should fail if you don't provide the correct values
// for your production environment
const defaultEnvValues = {
    SYMBIOTA2_UI_IN_PRODUCTION: false,
    SYMBIOTA2_UI_API_URL: "http://localhost:8080/api/v1",
    SYMBIOTA2_UI_DEFAULT_LANGUAGE: "en",
    SYMBIOTA2_UI_TITLE: "Symbiota2 Demo",
    SYMBIOTA2_UI_RECAPTHA_SITE_KEY: "6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI",
    // For mapping functionality
    SYMBIOTA2_UI_TILES_URL: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
    SYMBIOTA2_UI_TILES_TOKEN: "",
    SYMBIOTA2_UI_ENABLE_FOOTER: true,
    SYMBIOTA2_KIBANA_SERVER: 'http://localhost',
    SYMBIOTA2_KIBANA_PORT: '5601',
    SYMBIOTA2_KIBANA_DASHBOARD_SPATIAL_MODULE: '/app/dashboards?auth_provider_hint=anonymous1#/view/3050b510-a931-11ec-8fad-591789a7e073',
    SYMBIOTA2_KIBANA_DASHBOARD_COLLECTION_STATS: '/app/dashboards?auth_provider_hint=anonymous1#/view/a9f7a9a0-bb79-11ec-87e4-7372471162d9',
    SYMBIOTA2_KIBANA_DASHBOARD_IMAGE_SEARCH: '/app/dashboards?auth_provider_hint=anonymous1#/view/fc340bf0-82df-11ec-b9ae-27af917973e5',
};

// console.log("loading main file")
// Load template file
const mainFile = fs.readFileSync(
    path.join(distFilesDirectory, mainFileName),
    {encoding: 'utf-8'}
);

// console.log("generating output file " + distFilesDirectory + targetMainFileName)
// Generate output data
const output = ejs.render(mainFile, Object.assign({}, defaultEnvValues, process.env));
// Write environment file
fs.writeFileSync(path.join(distFilesDirectory, targetMainFileName), output);

process.exit(0);
