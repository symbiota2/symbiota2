import {MigrationInterface, QueryRunner} from "typeorm";

export class makeNameUniqueIdUnsignedInPermisionTb1671279358019 implements MigrationInterface {
    name = 'makeNameUniqueIdUnsignedInPermisionTb1671279358019'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`userpermissions\` CHANGE \`id\` \`userpermissionid\` int UNSIGNED NOT NULL AUTO_INCREMENT`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`userpermissions\` CHANGE \`userpermissionid\` \`id\` int UNSIGNED NOT NULL AUTO_INCREMENT`);
    }

}
