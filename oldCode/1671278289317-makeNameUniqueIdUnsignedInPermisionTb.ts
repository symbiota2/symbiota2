import {MigrationInterface, QueryRunner} from "typeorm";

export class makeNameUniqueIdUnsignedInPermisionTb1671278289317 implements MigrationInterface {
    name = 'makeNameUniqueIdUnsignedInPermisionTb1671278289317'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`role_permissions\` DROP FOREIGN KEY \`FK_17022daf3f885f7d35423e9971e\``);
        await queryRunner.query(`ALTER TABLE \`userpermissions\` CHANGE \`id\` \`id\` int NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`userpermissions\` DROP PRIMARY KEY`);
        await queryRunner.query(`ALTER TABLE \`userpermissions\` DROP COLUMN \`id\``);
        await queryRunner.query(`ALTER TABLE \`userpermissions\` ADD \`id\` mediumint UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT`);
        await queryRunner.query(`ALTER TABLE \`userpermissions\` ADD UNIQUE INDEX \`IDX_17af1ce34de627c54d7c3b6209\` (\`name\`)`);
        await queryRunner.query(`ALTER TABLE \`role_permissions\` DROP PRIMARY KEY`);
        await queryRunner.query(`ALTER TABLE \`role_permissions\` ADD PRIMARY KEY (\`role_id\`)`);
        await queryRunner.query(`DROP INDEX \`IDX_17022daf3f885f7d35423e9971\` ON \`role_permissions\``);
        await queryRunner.query(`ALTER TABLE \`role_permissions\` DROP COLUMN \`permission_id\``);
        await queryRunner.query(`ALTER TABLE \`role_permissions\` ADD \`permission_id\` mediumint UNSIGNED NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`role_permissions\` DROP PRIMARY KEY`);
        await queryRunner.query(`ALTER TABLE \`role_permissions\` ADD PRIMARY KEY (\`role_id\`, \`permission_id\`)`);
        await queryRunner.query(`CREATE INDEX \`IDX_17022daf3f885f7d35423e9971\` ON \`role_permissions\` (\`permission_id\`)`);
        await queryRunner.query(`ALTER TABLE \`role_permissions\` ADD CONSTRAINT \`FK_17022daf3f885f7d35423e9971e\` FOREIGN KEY (\`permission_id\`) REFERENCES \`userpermissions\`(\`id\`) ON DELETE CASCADE ON UPDATE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`role_permissions\` DROP FOREIGN KEY \`FK_17022daf3f885f7d35423e9971e\``);
        await queryRunner.query(`DROP INDEX \`IDX_17022daf3f885f7d35423e9971\` ON \`role_permissions\``);
        await queryRunner.query(`ALTER TABLE \`role_permissions\` DROP PRIMARY KEY`);
        await queryRunner.query(`ALTER TABLE \`role_permissions\` ADD PRIMARY KEY (\`role_id\`)`);
        await queryRunner.query(`ALTER TABLE \`role_permissions\` DROP COLUMN \`permission_id\``);
        await queryRunner.query(`ALTER TABLE \`role_permissions\` ADD \`permission_id\` int NOT NULL`);
        await queryRunner.query(`CREATE INDEX \`IDX_17022daf3f885f7d35423e9971\` ON \`role_permissions\` (\`permission_id\`)`);
        await queryRunner.query(`ALTER TABLE \`role_permissions\` DROP PRIMARY KEY`);
        await queryRunner.query(`ALTER TABLE \`role_permissions\` ADD PRIMARY KEY (\`role_id\`, \`permission_id\`)`);
        await queryRunner.query(`ALTER TABLE \`userpermissions\` DROP INDEX \`IDX_17af1ce34de627c54d7c3b6209\``);
        await queryRunner.query(`ALTER TABLE \`userpermissions\` DROP COLUMN \`id\``);
        await queryRunner.query(`ALTER TABLE \`userpermissions\` ADD \`id\` int NOT NULL AUTO_INCREMENT`);
        await queryRunner.query(`ALTER TABLE \`userpermissions\` ADD PRIMARY KEY (\`id\`)`);
        await queryRunner.query(`ALTER TABLE \`userpermissions\` CHANGE \`id\` \`id\` int NOT NULL AUTO_INCREMENT`);
        await queryRunner.query(`ALTER TABLE \`role_permissions\` ADD CONSTRAINT \`FK_17022daf3f885f7d35423e9971e\` FOREIGN KEY (\`permission_id\`) REFERENCES \`userpermissions\`(\`id\`) ON DELETE CASCADE ON UPDATE CASCADE`);
    }

}
