import {MigrationInterface, QueryRunner} from "typeorm";

export class makeUidAndNameUniqueInUserRole1671214680998 implements MigrationInterface {
    name = 'makeUidAndNameUniqueInUserRole1671214680998'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE UNIQUE INDEX \`IDX_fbd2a779514572be77c39c9a1d\` ON \`userroles\` (\`uid\`, \`role\`)`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX \`IDX_fbd2a779514572be77c39c9a1d\` ON \`userroles\``);
    }

}
