import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateUserPermission1670951213801 implements MigrationInterface {
    name = 'CreateUserPermission1670951213801'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`userpermissions\` (\`id\` int NOT NULL AUTO_INCREMENT, \`name\` varchar(255) NOT NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE \`userpermissions\``);
    }

}
