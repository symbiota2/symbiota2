import {MigrationInterface, QueryRunner} from "typeorm";

export class makingUseRoleUIDNullable1672755290050 implements MigrationInterface {
    name = 'makingUseRoleUIDNullable1672755290050'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`userroles\` DROP FOREIGN KEY \`FK_901b60b94c84906a1ea44fb3737\``);
        await queryRunner.query(`DROP INDEX \`IDX_fbd2a779514572be77c39c9a1d\` ON \`userroles\``);
        await queryRunner.query(`ALTER TABLE \`userroles\` CHANGE \`uid\` \`uid\` int UNSIGNED NULL`);
        await queryRunner.query(`CREATE UNIQUE INDEX \`IDX_fbd2a779514572be77c39c9a1d\` ON \`userroles\` (\`uid\`, \`role\`)`);
        await queryRunner.query(`ALTER TABLE \`userroles\` ADD CONSTRAINT \`FK_901b60b94c84906a1ea44fb3737\` FOREIGN KEY (\`uid\`) REFERENCES \`users\`(\`uid\`) ON DELETE CASCADE ON UPDATE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`userroles\` DROP FOREIGN KEY \`FK_901b60b94c84906a1ea44fb3737\``);
        await queryRunner.query(`DROP INDEX \`IDX_fbd2a779514572be77c39c9a1d\` ON \`userroles\``);
        await queryRunner.query(`ALTER TABLE \`userroles\` CHANGE \`uid\` \`uid\` int UNSIGNED NOT NULL`);
        await queryRunner.query(`CREATE UNIQUE INDEX \`IDX_fbd2a779514572be77c39c9a1d\` ON \`userroles\` (\`uid\`, \`role\`)`);
        await queryRunner.query(`ALTER TABLE \`userroles\` ADD CONSTRAINT \`FK_901b60b94c84906a1ea44fb3737\` FOREIGN KEY (\`uid\`) REFERENCES \`users\`(\`uid\`) ON DELETE CASCADE ON UPDATE CASCADE`);
    }

}
